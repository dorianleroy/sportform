
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <title>Куртки</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv='X-UA-Compatible' content='IE=8'>
    <meta http-equiv='Content-language' content ='ru'>
    <BASE HREF="test1.ru">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="robots" content="INDEX,FOLLOW">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/templates/defaultSchema//style.css">
    <link rel="stylesheet" type="text/css" href="/templates/defaultSchema//jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/templates/defaultSchema//jcarousel/skins/tango/skin.css">
    <link rel="stylesheet" type="text/css" href="/templates/defaultSchema//jcarousel/skins/tango2/skin.css">
    <script type="text/javascript" src="/templates/defaultSchema//jcarousel/lib/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="/templates/defaultSchema//jquery-ui.js"></script>
    <script type="text/javascript" src="/templates/defaultSchema//functions.js"></script>
    <script type="text/javascript" src="/templates/defaultSchema//jcarousel/lib/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" language="javascript" src="/templates/defaultSchema//carouFredSel/jquery.carouFredSel-5.5.0-packed.js"></script>
</head>

<body>
<div id="fb-root"></div>
<script type="text/javascript">
    $(document).ready(function(){
        CheckSearch();
        CheckForHash();
    })
</script>


<div style="margin-top: 8px; margin-right:8px; margin-left: 8px;">
	<div class="page">
		<div class="header_top">
			<!--<div class="header_anons"><h1><a href="#">Сезонное предложение! Сандалии Nike от 1700 р</a></h1></div>-->
			<div class="header_top_left"><a href="/index.php"><img src="/templates/defaultSchema//img/logo.png" /></a></div>
			<div class="header_top_right">
				<div style="text-align: right; padding-right: 9px; padding-top: 2px; font-size: 11px;"><a href="/t/kontakty">Контакты</a> &nbsp;&nbsp;&nbsp; <a href="/t/pomosh">Помощь</a></div>
			</div>
			<div class="header_top_phone">Наш телефон: +7·495·500·6167</div>
			<div class="header_top_basket"><div>
<a href="/b" onmouseover=""><div style="padding: 7px;"><span>Корзина:</span><em>2</em><strong>111 110 р</strong></div></a>
<div id="basket_preview_window"></div>
</div>
<!--ShowBasketInfo();--></div>
			<div class="header_top_search">
				<div class="left_side"><img src="/templates/defaultSchema//img/top_search_left_side.gif" width="26" height="21" border="0" alt=""></div>
				<form id="search_mini_form" method="get" action="/s">
					<input id="search" name="search" type="text" value="название товара, артикул, ...">
					<div class="find_button"><input type=image src="/templates/defaultSchema//img/btn_find.gif" name="sub"></div>
				</form>
				<div class="right_side"><img src="/templates/defaultSchema//img/top_search_right_side.gif" width="9" height="21" border="0" alt=""></div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>

<script type="text/javascript">
    $(document).ready(function() {

    	document.onmousemove = function(e) {
    		var mCur = mousePageXY(e);
    		MouseX = mCur.x;
    		MouseY = mCur.y;
    	};

        // open menu's item what select
        var openItemMenu = 139;
        $(".main_cats_main_title b").each(function(){
            if ($(this).attr('cat') == openItemMenu)
                $(this).next().css("display","block");
        });
        // //open
    });
</script>

<div style="margin-right:8px; margin-left: 8px;">
	<div class="page">
		<div class="main_right_side">
			<div class="main_right_side_in" id="ajax_load_div" style="padding: 0 0 0 227px;">
				<div id="ajax_products_place" style="padding-left: 2px;">
				<div class="products_title">
					<div class="title_h1"><h1>Куртки</h1></div>
				</div>

				<div class="product_cell">
					<div class="product_num">1.</div>
					<div><a href="/p/c.-kurtki/essential" title="essential"><img id="product_main_photo" src="/i/width.-213/height.-213/essential" width="213" height="213" border="0" alt="essential" title="essential"></a></div>
					<span>Adidas</span>
					<strong><a href="/p/c.-kurtki/essential" title="essential">essential</a></strong>
					<div class="art_rating">
						<div class="code">арт. </div>
						<div class="crating"><span class="star"><span style="width:0%"></span></span></div>
					</div>
					<div class="sizes_price">
						<div class="sizes"><div style="line-height: 13px;">В наличии:<br><a href="javascript:void(0);" onClick="ShowAvSizes('essential', '36  - 36.5  - 39.5  - 42  - 44  - 46  - 48  - 44.5 ')">8 размеров</a></div></div>
                        <div class="product_price  nonOldPrice" style="padding: 0px;" ><span class="oldPriceThrow"><b>0р</b></span><span class='priceMain'><em>22 222р</em></span> </div>
                        <!--<div class="product_price  nonOldPrice" style="padding: 0px; min-width:150px; text-align:right;"><span class='priceMain'><em>22 222</em><i>&thinsp;р</i></span><b>0р</b></div>-->
					</div>
					<div class="clear"></div>
					<div class="button_line">
						<div class="button_over" style="">
						<a href="javascript:void(0);" OnClick="ButtonAdd2Basket('essential', '3');">Купить</a>
						</div>
					</div>
				</div>
				<div class="product_cell">
					<div class="product_num">2.</div>
					<div><a href="/p/c.-kurtki/eto_testovyiy_tovar3" title="Это тестовый товар3"><img id="product_main_photo" src="/i/width.-213/height.-213/eto_testovyiy_tovar3" width="213" height="213" border="0" alt="Это тестовый товар3" title="Это тестовый товар3"></a></div>
					<span>Adidas</span>
					<strong><a href="/p/c.-kurtki/eto_testovyiy_tovar3" title="Это тестовый товар3">Это тестовый товар3</a></strong>
					<div class="art_rating">
						<div class="code">арт. </div>
						<div class="crating"><span class="star"><span style="width:0%"></span></span></div>
					</div>
					<div class="sizes_price">
						<div class="sizes"><div style="line-height: 13px;">В наличии:<br><a href="javascript:void(0);" onClick="ShowAvSizes('eto_testovyiy_tovar3', '36.5  - 37  - 39  - 40.5  - 41.5 ')">5 размеров</a></div></div>
                        <div class="product_price  oldPrice" style="padding: 0px;" ><span class="oldPriceThrow"><b>10 000р</b></span><span class='priceMain'><em>1 999р</em></span> </div>
                        <!--<div class="product_price  oldPrice" style="padding: 0px; min-width:150px; text-align:right;"><span class='priceMain'><em>1 999</em><i>&thinsp;р</i></span><b>10 000р</b></div>-->
					</div>
					<div class="clear"></div>
					<div class="button_line">
						<div class="button_over" style="">
						<a href="javascript:void(0);" OnClick="ButtonAdd2Basket('eto_testovyiy_tovar3', '3');">Купить</a>
						</div>
					</div>
				</div>

				<div class="already_set_color" style="display: none;"></div>
				</div>
			</div>
		</div>
		<div class="main_left_side">
			<div class="main_left_side_col">
				<div class="main_cats_drop_button">
	<a href="javascript:void(0);" onClick="ToggleMainCatsDrop();">
		<div class="title">Все разделы магазина<img src="/templates/defaultSchema//img/arrow2.gif" alt="" type="2" width="12" border="0" style="top: 9px; position: relative; float: right; right: 20px;"></div>
	</a>
</div>
<div class="main_cats_drop_window">
	<div class="main_cats_drop_window_page">
		<div style="background-color: #330000; height: 3px;"><img src="/templates/defaultSchema//img/d.gif" alt="" width="1" height="3" border="0"></div>
		<div style="height: 4px;"></div>

		<div class="main_cats_main_title" style="">
			<b onclick="toggleThis(this);" cat="139">Одежда</b>
			<span style="display:none;">

			<a href="/c/kurtki">Куртки</a>,
			<a href="/c/vetrovki">Ветровки</a>,
			<a href="/c/sportivnie_kostymi">Спортивные костюмы</a>,
			<a href="/c/bryki">Брюки</a>,
			<a href="/c/futbolki">Футболки</a>,
			<a href="/c/shorti">Шорты</a>,
			<a href="/c/golovnie_ubori">Головные уборы</a>
			</span>
		</div>
		<div class="main_cats_main_title" style="">
			<b onclick="toggleThis(this);" cat="140">Обувь</b>
			<span style="display:none;">

			<a href="/c/dlya_begaobuv">Для бега</a>,
			<a href="/c/dlya_futbolobuv">Для футбола</a>,
			<a href="/c/dlya_voleybolaobuv">Для волейбола</a>,
			<a href="/c/dlya_backetbolaobuv">Для баскетбола</a>,
			<a href="/c/dlya_tennisaobuv">Для тенниса</a>,
			<a href="/c/dlya_borbiobuv">Для борьбы</a>,
			<a href="/c/dlya_hotybiobuv">Для хотьбы</a>
			</span>
		</div>
		<div class="main_cats_main_title" style="">
			<b onclick="toggleThis(this);" cat="141">Аксессуары</b>
			<span style="display:none;">

			<a href="/c/myachi">Мячи</a>,
			<a href="/c/perchatki">Перчатки</a>,
			<a href="/c/shitki">Щитки</a>,
			<a href="/c/nosk">Носки</a>,
			<a href="/c/sumki">Сумки</a>,
			<a href="/c/raznoe">Разное</a>
			</span>
		</div>
		<div class="main_cats_main_title" style="border: 0px;">
			<b onclick="toggleThis(this);" cat="142">Игровая форма</b>
			<span style="display:none;">

			<a href="/c/dlya_futbolform">Для футбола</a>,
			<a href="/c/dlya_voleybolform">Для волейбола</a>,
			<a href="/c/dlya_backetbol">Для баскетбола</a>,
			<a href="/c/dlya_tennisform">Для тенниса</a>,
			<a href="/c/dlya_legkoform">Для легкой атлектики</a>,
			<a href="/c/dlya_borbform">Для борьбы</a>
			</span>
		</div>
	</div>
	<!--<div class="main_cats_drop_window_shadow1">*</div>-->
</div>

				<ul id="left_side_tree">
				<li>
					<h2></h2>
					<ul>
					<!-- BEGIN cat_level1 -->
					<li>
						<a href="/c/odejda">Одежда</a>
						<ul>

							<li>
							<a href="/c/kurtki" class="open">Куртки</a>
							<ul class="minus">

							</ul>
							</li>
							<li>
							<a href="/c/vetrovki" class="">Ветровки</a>
							<ul class="minus">

							</ul>
							</li>
							<li>
							<a href="/c/sportivnie_kostymi" class="">Спортивные костюмы</a>
							<ul class="minus">

							</ul>
							</li>
							<li>
							<a href="/c/bryki" class="">Брюки</a>
							<ul class="minus">

							</ul>
							</li>
							<li>
							<a href="/c/futbolki" class="">Футболки</a>
							<ul class="minus">

							</ul>
							</li>
							<li>
							<a href="/c/shorti" class="">Шорты</a>
							<ul class="minus">

							</ul>
							</li>
							<li>
							<a href="/c/golovnie_ubori" class="">Головные уборы</a>
							<ul class="minus">

							</ul>
							</li>
						</ul>
					</li>
					<!-- END cat_level1 -->
					</ul>
				</li>
				</ul>


				
			</div>
		</div>
	</div>
</div>

<div id="dialog-sizes" title="Доступные размеры">
</div>
<script type="text/javascript">
$(function() {
	$( "#dialog:ui-dialog" ).dialog( "destroy" );
	$( "#dialog-sizes" ).dialog({
		autoOpen: false,
		height: 126,
		width: 277,
		modal: true,
		resizable: false
	});
	$( "#price-range" ).slider({
			range: true,
			min: 123.00,
			max: 22222.00,
			values: [ 1827, 22222 ],
			slide: function( event, ui ) {
				$( "#filter_price_from" ).html(Number_Format(ui.values[ 0 ]));
				$( "#filter_price_to" ).html(Number_Format(ui.values[ 1 ]));
			},
			change: FiltersSelectPrice
		});
});
var	script_find			=	'kurtki';
var	script_price_min	=	'1827';
var	script_price_max	=	'22222';
var	script_size			=	'';
var	script_color		=	'';
var	script_vendor		=	'';
var script_url			=	'/c';
    if (typeof(size_array) == undefined)    {
    size_array.push('');        }
</script>

<script type="text/javascript">

	$(function() {
		$( "#add_to_basket" ).draggable();
	});
	CheckForHash();
</script>

<div class="clear"></div>
<div>
	<div class="page">
		<div class="footer">
		<div>
			<div><img src="/templates/defaultSchema//img/bottom_left_logo.gif" width="221" height="98" border="0" alt="" title=""></div>
		</div>
		<div>
			<div class="footer_links"><a href="/t/o_magazine">О магазине</a> • <a href="/t/vozvrat_tovara">Возврат товара</a> • <a href="/t/pravovaya_informaciya">Правовая информация</a></div>
			<div class="footer_copyright">© Sportform, 2012.</div>
		</div>
		</div>
	</div>
</div>


<div id="add_to_basket" class="ui-corner-top" style="">
	<div class="add_to_basket_page ui-corner-top">
		<div id="loading">
			<img src="/templates/defaultSchema//img/loading.gif" width="235" height="235" border="0" style="padding-top: 18px;" alt="загрузка">
		</div>
	</div>
	<div class="add_to_basket_page_shadow1">*</div>
	<!--
	<div class="add_to_basket_page_shadow">
		<img src="templates/defaultSchema//img/2basket_shadow_top.gif" width="301" height="17" border="0" style="display: block;">
		<img src="templates/defaultSchema//img/2basket_shadow_bottom.gif" width="301" height="13" border="0" style="display: block; padding-top: 265px;">
	</div>
	-->
</div>
<div class="add_to_basket_page_all_shadow"></div>


<div style="position:absolute;top:-1000px;left:-1000px;" class="add_to_basket_page_ok"></div>

</body>
</html>
