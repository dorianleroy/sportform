if (typeof(_gaq) != 'undefined') {
//check for error class
    $('.error').each(function () {
        if (typeof(this.id) != 'undefined') _gaq.push(['_trackEvent', 'error', 'checkout', this.id]);
    });
    var gaTrackclickMap = {
        'BillingAddressForm_first_name':'/checkout/step/name',
        'creditcard':'/checkout/step/payment_method/cc',
        'invoice':'/checkout/step/payment_method/invoice',
        '2':'/checkout/step/payment_method/master',
        '1':'/checkout/step/payment_method/visa',
        '4':'/checkout/step/payment_method/diners',
        '5':'/checkout/step/payment_method/hipercard',
        '3':'/checkout/step/payment_method/amex'
    };
    $.each(gaTrackclickMap, function (key, value) {
        $('#' + key).click(function () {
            _gaq.push(['_trackPageview', value]);
        });
    });
    var gaTrackfocusMap = {
        'BillingAddressForm_first_name':'/checkout/step/name'
    };
    $.each(gaTrackfocusMap, function (key, value) {
        $('#' + key).focus(function () {
            _gaq.push(['_trackPageview', value]);
        });
    });
    var gaTrackblurMap = {
        'RegisterForm_tax_identification':'/checkout/step/cpf',
        'BillingAddressForm_postcode':'/checkout/step/billing_address',
        'ShippingAddressForm_postcode':'/checkout/step/delivery_address',
        'CreditcardForm_cc_number':'/checkout/step/payment_method/credit_card/number'
    };
    $.each(gaTrackblurMap, function (key, value) {
        $('#' + key).blur(function () {
            _gaq.push(['_trackPageview', value]);
        });
    });
}


var checkout = {
    init:function () {
        if ($('#cardName').val()) {
            $('#' + $('#cardName').val()).trigger('click');
        }
        checkout.initPaymentMethods();
        $(document).on('click', '#coupon-label', function () {
            $(this).hide();
            $('#coupon-body').show();
        });
    },

    initPaymentMethods:function () {
        $('.payment-method input.payment-method-option').click(function () {
            $('.payment-method-form').hide();
            if ($('.payment-method-' + $(this).attr('id')).length) {
                $('.payment-method-' + $(this).attr('id')).show();
            }
        });
        $('.creditcards .ui-listItem').click(function () {
            checkout.selectCreditCard($(this));
        });
        $('#CreditcardForm_cc_number').keydown(function () {
            checkout.detectCard($(this))
        });
    },
    selectCreditCard:function (el) {
        $('.creditcards .ui-listItem').removeClass('selected');
        $('#cardName').val(el.text());
        el.addClass('selected');
    },
    detectCard:function (field) {
        var error = false;
        if (field.val() != "") {
            var v = field.val();
            if (!isNaN(v)) {
                /* visa */
                if (v.substr(0, 1) == 4) {
                    checkout.selectCreditCard($('#visa'));
                    /* master */
                } else if (v.substr(0, 1) == 5) {
                    checkout.selectCreditCard($('#mastercard'));
                    /* diners */
                } else if ((v.substr(0, 3) == 300 || v.substr(0, 3) == 305 || v.substr(0, 2) == 36)) {
                    checkout.selectCreditCard($('#diners'));
                    /* amex */
                } else if ((v.substr(0, 2) == 34 || v.substr(0, 2) == 37)) {
                    checkout.selectCreditCard($('#amex'));
                } else {
                    error = true;
                }
            } else {
                error = true;
            }
        }
    },
    sendCoupon:function () {
        if (!$('#coupon').val().match(/^\s*$/)) {
            $('#coupon').removeClass('error');

            $('#checkoutAjaxLoader').show();
            $('#checkoutBtn').attr('disabled', 'disabled').fadeTo(500, .5);
            $('#coupon').attr('disabled', 'disabled');
            $('#couponSend').attr('disabled', 'disabled');
            $.get('/checkout/finish/cart/couponcode/' + $('#coupon').val(), function (data) {
                if (data) {
                    $('#checkoutGrandTotal').html(data);
                    $.get('/checkout/finish/paymentmethod/', function (data) {
                        if (data) {
                            $('#checkoutAjaxLoader').hide();
                            $('#checkout-payment').html(data);
                            $('#checkoutBtn').attr('disabled', null).fadeTo(500, 1);
                        }
                        ;
                    });
                }
                ;
            });
        } else {
            $('#couponSend').attr('disabled', null);
            $('#coupon').attr('disabled', null);
            $('#checkoutBtn').attr('disabled', null).fadeTo(500, 1);
            $('#checkoutAjaxLoader').hide();
            $('#coupon').addClass('error');
        }
    },
    setPaymentMethod:function (paymentMethod) {
        if (paymentMethod) {
            $('#checkoutAjaxLoader').show();
            $('#checkoutBtn').attr('disabled', 'disabled').fadeTo(500, .5);
            $.get('/checkout/finish/summary/paymentMethod/' + paymentMethod, function (data) {
                if (data) {
                    $('#checkoutAjaxLoader').hide();
                    $('#checkoutGrandTotal').html(data);
                    $('#checkoutBtn').attr('disabled', null).fadeTo(500, 1);
                }
            });
        }
    },
    setShippingMethod:function (shippingMethod) {
        if (shippingMethod) {
            $('#checkoutAjaxLoader').show();
            $('#checkoutBtn').attr('disabled', 'disabled').fadeTo(500, .5);
            $.get('/checkout/finish/summary/shippingMethod/' + shippingMethod, function (data) {
                if (data) {
                    $('#checkoutAjaxLoader').hide();
                    $('#checkoutGrandTotal').html(data);
                    $('#checkoutBtn').attr('disabled', null).fadeTo(500, 1);
                }
            });
        }
    },
    loadDifferentShipping:function (checkbox) {
        $('#checkoutAjaxLoader').show();
        if (checkbox.is(":checked")) {
            $('#checkout-shipping-content').slideUp(500, function () {
                $('#checkoutAjaxLoader').hide();
                $('#checkout-shipping-content').html('');
            });
        } else {
            $.get('/checkout/finish/shipping/', function (data) {
                $('#checkout-shipping-content').hide();
                $('#checkout-shipping-content').html(data);
                checkout.maskShippingAddressFields();
                $('#checkout-shipping-content').slideDown(500, function () {
                    $('#checkoutAjaxLoader').hide();
                });
                $('#new-shipping-address').click(function (event) {
                    checkout.loadDifferentShippingAddress();
                    event.preventDefault();
                });
            });
        }
        ;
    },
    loadDifferentBilling:function () {
        $('#checkoutAjaxLoader').show();
        $.get('/checkout/finish/billing/form/1', function (data) {
            $('#checkout-address').html(data);
            $('#checkoutAjaxLoader').hide();
            checkout.maskBillingAddressFields();
        });
    },
    loadDifferentShippingAddress:function () {
        $('#checkoutAjaxLoader').show();
        $.get('/checkout/finish/shipping/form/1', function (data) {
            $('#checkout-shipping-content').html(data);
            $('#checkoutAjaxLoader').hide();
            checkout.maskShippingAddressFields();
        });
    },
    maskBillingAddressFields:function () {
        //if ($('#checkout-address').length) {
        $('input[id="BillingAddressForm_phone"]').mask('+7 9999999999');
        $.mask.definitions['m'] = '[01]';
        $.mask.definitions['d'] = '[0123]';
        $.mask.definitions['y'] = '[12]';
        $('input[id="BillingAddressForm_birthday_day"]').mask('d9');
        $('input[id="BillingAddressForm_birthday_month"]').mask('m9');
        $('input[id="BillingAddressForm_birthday_year"]').mask('y999');
        $('input[id="BillingAddressForm_postcode"]').mask('999999');
        //}
    },
    maskShippingAddressFields:function () {
        //if ($('#checkout-shipping').length) {
        $('input[id="ShippingAddressForm_phone"]').mask('+7 9999999999');
        $('input[id="ShippingAddressForm_postcode"]').mask('999999');
        //}
    }
};

$(function () {
    checkout.init();
    checkout.maskBillingAddressFields();
    checkout.maskShippingAddressFields();
    $('a#buttonCheckoutSecond').click(function () {
        $('#checkoutBtn').click();
        $('#checkoutForm').submit();
    });
    $('#checkoutBtn').click(function () {
        if (typeof(_gaq) != 'undefined') {
            _gaq.push(['_trackPageview', '/checkout/step/finish']);
        }
        var wHeight = $(window).height();
        var wDialog = $("#dialogProcessing .container").height();
        var marginTop = wHeight / 2 - (156 / 2);
        /* 156 = 100px height of the dialog + 36px padding-top and 20px padding-bottom */
        $("#dialogProcessing .container").css("margin-top", marginTop);
        $('#dialogProcessing').show();
        window.setTimeout('$("#dialogProcessingInfo1").slideUp(300); $("#dialogProcessingInfo2").slideDown(300);', 6500);
    });
    $('#newsletterSignupWrapper').fadeTo(500, .5);
    $('#load-different-billing').click(function (event) {
        event.preventDefault();
        checkout.loadDifferentBilling();
    });
    $('#couponSend').live('click', checkout.sendCoupon);
    $('#shipping-address-different').click(function () {
        checkout.loadDifferentShipping($(this));
    });
    $('#shippingAddressDifferent').click(function () {
        checkout.loadDifferentShipping($(this));
    });
    $('.payment-method-option').click(function () {
        checkout.setPaymentMethod($(this).val())
    });
    $('.shipping-method-option').click(function () {
        checkout.setShippingMethod($(this).val());
    });
});
