<?
$MESSAGES["monthes"] = array("", "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");

######################## site
$MESSAGES["site_title"] 		=	"Sportform.ru";
$MESSAGES["small_name"] 		=	"BO";
$MESSAGES["site_keywords"]		=	"";
$MESSAGES["site_description"]	=	"";

######################## general text

######################## errors messages
$MESSAGES["error_access"]		=	"Нет прав на выполнение этой фукции.";
$MESSAGES["error_logon"]		=	"Не удалось войти. Вы не правильно указали либо логин, либо пароль.";
$MESSAGES["error_password"]		=	"Проверьте правильность ввода пароля.";

$MESSAGES["vendor_noname"]		=	"не указан";

######################### phones
$MESSAGES["phonenumber"]		=	"+7·495·500·6167";
$MESSAGES["phonenumber2"]		=	"";
$MESSAGES["faxnumber"]			=	"";

######################### emails
$MESSAGES["email_general"]		=	"SPORTFORM.RU <xxx@sportform.ru>";

######################### money
$MESSAGES["money_rur"]		=	"р";		//	сокращенное обозначение рублей

######################### urls
$MESSAGES["url_baseurl"]			=	"test1.ru";
$MESSAGES["url_product_image"]		=	"/i";
$MESSAGES["url_product"]			=	"/p";
$MESSAGES["url_aboutvendor"]		=	"/v";
$MESSAGES["url_search"]				=	"/s";
$MESSAGES["url_cat"]				=	"/c";
$MESSAGES["url_basket"]				=	"/b";
$MESSAGES["url_text"]				=	"/t";
?>