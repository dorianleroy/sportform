<?
#################################################################
require ("libs/fo_prepare.php");

$_cache_dir	=	"gzip_cache/";

if (!$_GET["file"]) die();
if (strpos(" ".$_SERVER["HTTP_ACCEPT_ENCODING"], "gzip")) $gzip	=	1; else $gzip	=	0;
if ($_GET["file"]	!=	"jquery-ui-1.css") if (strpos(" ".$_SERVER["HTTP_USER_AGENT"], "Opera")) $gzip	=	0;

$gzip = 0;

function debug($file, $text) {
	if (file_exists($file)) {
		$f = fopen($file, "a");
	} else {
		$f = fopen($file, "w");
	}
	fputs($f, $text);
	fclose($f);
	chmod($file, 0644);
}

function returnMIMEType($filename)
    {
        preg_match("|\.([a-z0-9]{2,4})$|i", $filename, $fileSuffix);

        switch(strtolower($fileSuffix[1]))
        {
            case "js" :
                return "application/x-javascript";

            case "json" :
                return "application/json";

            case "jpg" :
            case "jpeg" :
            case "jpe" :
                return "image/jpg";

            case "png" :
            case "gif" :
            case "bmp" :
            case "tiff" :
                return "image/".strtolower($fileSuffix[1]);

            case "css" :
                return "text/css";

            case "xml" :
                return "application/xml";

            case "doc" :
            case "docx" :
                return "application/msword";

            case "xls" :
            case "xlt" :
            case "xlm" :
            case "xld" :
            case "xla" :
            case "xlc" :
            case "xlw" :
            case "xll" :
                return "application/vnd.ms-excel";

            case "ppt" :
            case "pps" :
                return "application/vnd.ms-powerpoint";

            case "rtf" :
                return "application/rtf";

            case "pdf" :
                return "application/pdf";

            case "html" :
            case "htm" :
            case "php" :
                return "text/html";

            case "txt" :
                return "text/plain";

            case "mpeg" :
            case "mpg" :
            case "mpe" :
                return "video/mpeg";

            case "mp3" :
                return "audio/mpeg3";

            case "wav" :
                return "audio/wav";

            case "aiff" :
            case "aif" :
                return "audio/aiff";

            case "avi" :
                return "video/msvideo";

            case "wmv" :
                return "video/x-ms-wmv";

            case "mov" :
                return "video/quicktime";

            case "zip" :
                return "application/zip";

            case "tar" :
                return "application/x-tar";

            case "swf" :
                return "application/x-shockwave-flash";

            default :
            if(function_exists("mime_content_type"))
            {
                $fileSuffix = mime_content_type($filename);
            }

            return "unknown/" . trim($fileSuffix[0], ".");
        }
    }

$filename	=	"templates/".$_curSchema."/".$_GET["file"];
if (!file_exists($filename)) die();

$path_parts = pathinfo($filename);
//header("Content-Disposition: attachment; filename=" . $path_parts['basename']);
header("Content-Type: ".returnMIMEType($filename));
header("Last-Modified: ".date("r", filectime($filename) )." GMT");

$f = fopen($filename, "r");
$contents = fread($f, filesize($filename));

if (!$gzip) {	##############################################################	����� ��� gzip
	##############################################################	//	����� ��� gzip} else {	##############################################################	����� gzip
	if (filesize($filename) > 2048) {		header("Content-Encoding: gzip");
		##########################################################	��������� ��� - ����� ��� ��� ���� ������ ��� ������
		$original_time	=	filectime($filename);
		if ( (file_exists($_cache_dir.$_GET["file"])) && ($original_time <= filectime($_cache_dir.$_GET["file"])) ) {			######################################################	������� ������ �� ����
			$f_cache	=	fopen($_cache_dir.$_GET["file"], "r");
			$contents	=	fread($f_cache, filesize($_cache_dir.$_GET["file"]));
			fclose($f_cache);
			//debug("./logs/gzip.txt", date("d-m-Y H:i.s")."\t".$_GET["file"]."\t"."����� �� ����\n");		} else {			######################################################	������ ������ � ���������� � ���			$_temp1 = strlen($contents);
			//$contents1	=	$contents;
			$contents	=	gzcompress($contents, 9);
			$contents	=	substr($contents, 0, $_temp1);
			$contents	=	"\x1f\x8b\x08\x00\x00\x00\x00\x00" . $contents;
			@unlink($_cache_dir.$_GET["file"]);
			//debug("./logs/gzip.txt", date("d-m-Y H:i.s")."\t".$_GET["file"]."\t"."������� ���\n");
			$f_cache	=	fopen($_cache_dir.$_GET["file"], "w");
			fwrite($f_cache, $contents);
			fclose($f_cache);
			chmod($_cache_dir.$_GET["file"], 0666);
			touch($_cache_dir.$_GET["file"], $original_time);
			//ob_start('ob_gzhandler');
			//$contents	=	$contents1;
			//debug("./logs/gzip.txt", date("d-m-Y H:i.s")."\t".$_GET["file"]."\t"."������� �����\n");		}
	}
	##############################################################	//	����� gzip}

//header("Content-Length: ".strlen($contents));
fclose($f);
echo $contents;
die();
?>