<?
#################################################################
require ("libs/fo_prepare.php");
/*
$d_t = $data->GET["what"] ? $data->GET["what"] : $data->POST["what"];
$f	=	fopen("./logs/auto.txt", "a");
fputs($f, date("Y-m-d H:i.s")."\t".$d_t."\n");
fclose($f);      */


if ($data->POST["what"]) $data->GET["what"] = $data->POST["what"];

if (!$data->GET["what"]) die();


#################################################################		ПОЛУЧИТЬ ЦВЕТ ТОВАРА
#################################################################		ДЛЯ КОРЗИНЫ
if ($data->POST['what'] == 'getsizefromcolor') {
    $q = $cp->GetSizes_one($data->POST['pid'], $data->POST['color']);
    echo json_encode($q);
    exit;

}

if ($data->POST['what'] == 'addphotos') {

    $imgs = $cp->GetImagesForPID($_default_images_dir_products, $data->POST['pid'], $data->POST["color"]);

    for ($i = 0; $i < count($imgs); $i++) {

        $mask = str_replace("." . $_default_images_dir_products . "/" . $data->POST['pid'] . "_", "", $imgs[$i]);
        $mask = str_replace("-" . $data->GET["color"] . "_.jpg", "", $mask);
        $parts = explode("-", $mask);
        $q[$i] = $parts[0];

    }


    ;
    echo json_encode($q);
    exit;

}


#################################################################		ПРОВЕРКА ЗАКАЗА КЛИЕНТА
if ($data->POST['what'] == 'order') {

    if (count($_REQUEST['array_input']) > 1) {
        $out_array = array();
        $out_array_client = array();
        foreach ($_REQUEST['array_input'] as $k => $v) {
            if ($v['value'] != '') {
                $out_array[$v['id']] = $v['value'];
                $out_array_client[$v['id']] = $v['value'];
            }
            else {
                $out_array[$v['id']] = "нет\n";
                $out_array_client[$v['id']] = "<span class='dont-show'>не заполнено</span>";
            }
        }

        $client_out = "<div class='out_final'>";
        $client_out .=
            "
            <p>Имя  - " . $out_array_client['BillingAddressForm_first_name'] . "</p>
            <p>Фамилия  - " . $out_array_client['BillingAddressForm_last_name'] . "</p>
            <p>Телефон  - " . $out_array_client['phone'] . "</p>
            <p>Емэил  - " . $out_array_client['RegisterForm_email'] . "</p>
            <p>Почтовый индекс - " . $out_array_client['BillingAddressForm_postcode'] . "</p>
            <p>Город - " . $out_array_client['BillingAddressForm_city'] . "\n</p>
            <p>Улица - " . $out_array_client['RegisterForm_street'] . "</p>
            <p>Дом - " . $out_array_client['Billing_home'] . "</p>
            <p>Строение - " . $out_array_client['Billing_stroenie'] . "</p>
            <p>Корпус - " . $out_array_client['Billing_korpus'] . "</p>
            <p>Подъезд - " . $out_array_client['Billing_podezd'] . "</p>
            <p>Код - " . $out_array_client['Billing_code'] . "</p>
            <p>Этаж - " . $out_array_client['Billing_floor'] . "</p>
            <p>Квартира - " . $out_array_client['Billing_flat'] . "</p>
            <p>Метро - " . $out_array_client['metro'] . "</p>
            <p>Примечание - " . $out_array_client['note'] . "</p>
            <p>Способ оплаты - наличными</p>
            ";
        if (count($out_array) > 1) {
            if ($_COOKIE['basket']) {

                $basket_arr = explode("-", $_COOKIE['basket']);
                $products = '';
                for ($i = 0; $i < (count($basket_arr) - 1); $i++) {
                    $z = explode(".", $basket_arr[$i]);
                    $product = $cp->GetProductByID($z[0]);
                    $p_color = $cp->GetProductColor($z[1]);
                    $p_color0 = $cp->GetProductColor($product[color]);
                    $p_size = $cp->GetProductSize($z[2]);
                    $all_sum += ($product[price] * $z[3]);
                    $products .= "<div style='width:300px;border-top:1px solid #999999; padding:10px 3px;'>Название - " . $product['name'] . "<br> Арт. - " . $product['code'] . "<br> Производитель - " . $product['vendor_name'] . "<br> Цвет - " . $p_color['name'] . "<br> Количество - " . $z[3] . "<br> Цена - " . number_format(($product[price] * $z[3]), 0, ',', ' ') . "<br></div>";
                }
                $client_out .= "<p>Выбранный товар - </p>";
                $client_out .= $products;
                $client_out .= "</div>";
                //                $order_number = $cp->InsertCustomerInfo($out_array);
                //                $cp->InsertOrder($basket_arr,$order_number);

            }
        }
    }
    else
    {
        $client_out = 'Произошла ошибка. Администраторы уже ее исправляют.';
        $error = __FILE__ . __DIR__ . "\nREQUEST[INPUT] оказался пуст";
        mail('dorianleroy@gmail.com', "Ошибка с sportform-shop.ru", $error);
    }

    echo $client_out;
    exit;

}

#################################################################		ОКОНЧАТЕЛЬНОЕ ПОДТВЕРЖДЕНИЕ ЗАКАЗА
if ($data->POST["what"] == "deliv_access") {

    if ($data->POST['status'] == 1) {

        $out_array = array();
        $out_array_client = array();
        foreach ($_REQUEST['array_input'] as $k => $v) {
            if ($v['value'] != '') {
                $out_array[$v['id']] = $v['value'];
                $out_array_client[$v['id']] = $v['value'];
            }
            else {
                $out_array[$v['id']] = "нет\n";
                $out_array_client[$v['id']] = "<span class='dont-show'>не заполнено</span>";
            }
        }

        $mail_to_admin =
            "Доброго времени суток!\n
            С сайта sportform.ru поступил заказ\n
            Имя  - " . $out_array['BillingAddressForm_first_name'] . "\n
            Фамилия  - " . $out_array['BillingAddressForm_last_name'] . "\n
            Телефон  - " . $out_array['phone'] . "\n
            Емэил  - " . $out_array['RegisterForm_email'] . "\n
            Почтовый индекс - " . $out_array['BillingAddressForm_postcode'] . "\n
            Город - " . $out_array['BillingAddressForm_city'] . "\n
            Улица - " . $out_array['RegisterForm_street'] . "\n
            Дом - " . $out_array['Billing_home'] . "\n
            Строение - " . $out_array['Billing_stroenie'] . "\n
            Корпус - " . $out_array['Billing_korpus'] . "\n
            Подъезд - " . $out_array['Billing_podezd'] . "\n
            Код - " . $out_array['Billing_code'] . "\n
            Этаж - " . $out_array['Billing_floor'] . "\n
            Квартира - " . $out_array['Billing_flat'] . "\n
            Метро - " . $out_array['metro'] . "\n
            Примечание - " . $out_array['note'] . "\n
            ";
        $client_out = "<div class='out_final'>";
        $client_out .=
            "
            <p>Имя  - " . $out_array_client['BillingAddressForm_first_name'] . "</p>
            <p>Фамилия  - " . $out_array_client['BillingAddressForm_last_name'] . "</p>
            <p>Телефон  - " . $out_array_client['phone'] . "</p>
            <p>Емэил  - " . $out_array_client['RegisterForm_email'] . "</p>
            <p>Почтовый индекс - " . $out_array_client['BillingAddressForm_postcode'] . "</p>
            <p>Город - " . $out_array_client['BillingAddressForm_city'] . "\n</p>
            <p>Улица - " . $out_array_client['RegisterForm_street'] . "</p>
            <p>Дом - " . $out_array_client['Billing_home'] . "</p>
            <p>Строение - " . $out_array_client['Billing_stroenie'] . "</p>
            <p>Корпус - " . $out_array_client['Billing_korpus'] . "</p>
            <p>Подъезд - " . $out_array_client['Billing_podezd'] . "</p>
            <p>Код - " . $out_array_client['Billing_code'] . "</p>
            <p>Этаж - " . $out_array_client['Billing_floor'] . "</p>
            <p>Квартира - " . $out_array_client['Billing_flat'] . "</p>
            <p>Метро - " . $out_array_client['metro'] . "</p>
            <p>Примечание - " . $out_array_client['note'] . "</p>
            <p>Способ оплаты - наличными</p>
            ";
        if (count($out_array) > 1) {
            if ($_COOKIE['basket']) {
                $admin_email = 'sportform@yandex.ru';

                $basket_arr = explode("-", $_COOKIE['basket']);
                $products = '';

                for ($i = 0; $i < (count($basket_arr) - 1); $i++) {
                    $z = explode(".", $basket_arr[$i]);
                    $product = $cp->GetProductByID($z[0]);
                    $p_color = $cp->GetProductColor($z[1]);
                    $p_color0 = $cp->GetProductColor($product[color]);
                    $p_size = $cp->GetProductSize($z[2]);
                    $all_sum += ($product[price] * $z[3]);
                    $products .= "<div style='width:300px;border-top:1px solid #999999; padding:10px 3px;'>Название - " . $product['name'] . "<br> Арт. - " . $product['code'] . "<br> Производитель - " . $product['vendor_name'] . "<br> Цвет - " . $p_color['name'] . "<br> Количество - " . $z[3] . "<br> Цена - " . number_format(($product[price] * $z[3]), 0, ',', ' ') . "<br></div>";
                }
                $client_out .= "<p>Выбранный товар - </p>";
                $client_out .= $products;
                $client_out .= "</div>";
                $order_number = $cp->InsertCustomerInfo($out_array);
                if ($order_number) {
                    $cp->InsertOrder($basket_arr, $order_number);
                }
                else
                {
                    $user_exists = true;
                    exit;
                }

            }
        }


        $admin_email = 'sportform@yandex.ru';
        $from = "Sportform-shop.ru";
        $subj = "Заказ с Sportform-shop.ru";
        $head = "From: $from\n";
        $head .= "Subject: $subj\n";
        $head .= "X-Mailer: PHPMail Tool\n";
        $head .= "Reply-To: $from\n";
        $head .= "Mime-Version: 1.0\n";
        $head .= "Content-Type:text/html";

        $cp->LastDeliveryEnable();
        $lo = $cp->LastOrder();
        $lastorder = str_replace('-', '_', substr($lo['date'], 0, 10)) . '_' . $lo['id'];
        //setcookie("basket","",time()-3600);

        if (mail($admin_email, 'Заказ с sportform-shop.ru', 'Номер Заказа - ' . $lastorder . "<br>" . $client_out, $head)) {
            mail($out_array['RegisterForm_email'], 'Заказ с sportform-shop.ru', 'Здравствуйте! Вы сделали заказ на сайте - sportform-shop.ru. <br>Номер Вашего заказа - ' . $lastorder . "<br>" . $products . "<br> По интересующим Вас вопросам звоните по телефону - +7·495·500·6167 ", $head);
        }
        else
        {
            $error = 'Ошибка отправки письма';
            $client_out = 'Произошла ошибка. Администраторы уже ее исправляют.';
        }

        $error = false;
    }
    else
    {
        $error = true;
        $error_admin = __FILE__ . __DIR__ . "\nПодтверждение заказа не произошло";
        mail($out_array['RegisterForm_email'], 'Error.sportform-shop.ru', "Error. $desc_er");
    }

    if (!$error) {
        echo $lastorder;
    }
    else {
        return false;
    }
}

#################################################################		АВТОКОМПЛИТ ДЛЯ ПОИСКА
if ($data->GET["what"] == "search_auto") {
    if (strlen($data->GET["q"]) < 2) die();

    //$ff	=	fopen("./logs/auto_".date("Ymd-Hms").".txt", "w");

    $where_find = "";
    $find = explode(" ", $data->GET["q"]);
    for ($i = 0; $i < count($find); $i++) {
        $where_find .= "AND (name LIKE '%" . addslashes($find[$i]) . "%') ";
    }

    $found = array();

    $sql = new SQLClass();
    $res = $sql->query("SELECT name FROM " . $tableCollab["products"] . " WHERE 1 " . $where_find . " ORDER BY views DESC, name LIMIT 15");
    //fputs($ff, "SELECT name FROM ".$tableCollab["products"]." WHERE 1 ".$where_find." ORDER BY name LIMIT 100"."\n");
    //fputs($ff, mysql_num_rows($res)."\n");
    for ($i = 0; $i < mysql_num_rows($res); $i++) {
        $sql->fetch();
        $z = $sql->Record;
        if ((!in_array('"' . trim($z[0]) . '"', $found)) && (strlen($z[0]))) $found[count($found)] = '"' . trim($z[0]) . '"';
    }
    $sql->close();

    $f = implode(", ", $found);
    $f = str_replace("&quot;", "", $f);
    $f = str_replace("&#x00bb;", "", $f);
    $f = str_replace("&#x00ab;", "", $f);

    echo "[" . $f . "]";
    //fclose($ff);
    die();
}

#################################################################       ВЫПАДАЮЩЕЕ ОКНО ВЫБОРА ОПЛАТЫ ЗАКАЗА
if ($data->GET["what"] == "addhowtopay"){
    $longf = $t->set_file(array(
        "index" => "choice_how_to_buy.tpl.htm"
    ));
    $t->parse("OUT2", "index");
    $t->p("OUT2");
    die();
}
#################################################################       // ВЫПАДАЮЩЕЕ ОКНО ВЫБОРА ОПЛАТЫ ЗАКАЗА


#################################################################		ДОБАВИТЬ В КОРЗИНУ СРАЗУ ПОСЛЕ НАЖАТИЮ НА КНОПКУ
if ($data->GET["what"] == "add2basketproduct") {

}
#################################################################		//ДОБАВИТЬ В КОРЗИНУ СРАЗУ ПОСЛЕ НАЖАТИЮ НА КНОПКУ

#################################################################		ВЫПАДАЮЩЕЕ ОКНО С "ДОБАВИТЬ В КОРЗИНУ"
if ($data->GET["what"] == "add2basketwindow") {
    $url_encode = $_REQUEST[pid];
    $pid = $cp->GetProductIDByURL($url_encode);
    if (!$pid) {
        header("Location: /");
        die();
    }
    $product = $cp->GetProductByID($pid);

    $long = $t->set_file(array(
        "index" => "ajax_add2basket_window.tpl.htm"
    ));

    $t->set_var(array(
        "PRODUCT_ID" => $product[id],
        "PRODUCT_URL" => $product[url],
        "PRODUCT_NAME" => $product[name],
        "PRODUCT_CODE" => $product[code],
        "PRODUCT_VENDOR_NAME" => $product[vendor_name],
        "PRODUCT_VENDOR_COUNTRY" => $product[vendor_name_country],
        "PRODUCT_PRICE" => number_format($product[price], 0, ',', ' '),
        "PRODUCT_FORUNIT" => $product[for_unit],
        "PRODUCT_PRICE2" => number_format($product[price2], 0, ',', ' '),
        "PRODUCT_FORUNIT2" => $product[for_unit2],
        "PRODUCT_WAREHOUSE" => $product[warehouse],
        "PRODUCT_UNIT" => $cp->GetUnitByID($product[units]),
        "PRODUCT_SHORT_DESCRIPTION" => $_descr,
        "PRODUCT_FULL_DESCRIPTION" => $_full_descr,
        "PRODUCT_FULL_DESCRIPTION_PART1" => $_full_descr_parts[0],
        "PRODUCT_FULL_DESCRIPTION_PART2" => $_full_descr_parts[1],
        "PRODUCT_RATING" => $product_rating,
        "PRODUCT_COLOR" => $data->GET["color"],
        "COLOR_ID" => $data->GET["color"]
    ));

    ###############################################	Цвета товара
    $t->set_block("index", "basket-available-colors", "_basket-available-colors");
    $colors = $cp->GetProductColorList($pid);
    $col_from_ware = $cp->GetColorsWare($pid);
    $colors = $col_from_ware; //****** - заплатка
    for ($i = 0; $i < count($colors); $i++) {
        $color = $cp->GetProductColor($colors[$i]);
        if ($color["id"] == $_REQUEST["color"]) $color_selected_class = "ui-corner-all selected_color"; else $color_selected_class = "";
        $t->set_var(array(
            "COLOR_ID" => $color["id"],
            "COLOR_NAME" => $color["name"],
            "COLOR_COLOR" => $color["color"],
            "COLOR_ID_SELECTED" => $_REQUEST["color"],
            "COLOR_SELECTED_CLASS" => $color_selected_class
        ));
        $t->parse("_basket-available-colors", "basket-available-colors", true);
    }
    ###############################################	//	Цвета товара

    ###############################################	Дополнительные цвета товара
    $t->set_block("index", "available-colors_ad", "_available-colors_ad");
    $colors_ad = $cp->GetProductColorList($pid);
    $col_from_ware_ad = $cp->GetAdtnlColors($pid);
    $colors_ad = $col_from_ware_ad; //****** - заплатка
    if (count($colors_ad) < 1) {
        $t->set_block("index", "av-col-ad", "_av-col-ad");
        $t->set_var("av-col-ad", "");
        $t->parse("_av-col-ad", "av-col-ad");
    }

    for ($i = 0; $i < count($colors_ad); $i++) {
        $color_ad = $cp->GetProductColor($colors_ad[$i]);
        $t->set_var(array(
            "COLOR_ID_AD" => $color_ad["id"],
            "COLOR_NAME_AD" => $color_ad["name"],
            "COLOR_COLOR_AD" => $color_ad["color"]
        ));
        $t->parse("_available-colors_ad", "available-colors_ad", true);
    }

    ###############################################	//	Дополнительные цвета товара

    /////////////	Размеры по выбранному цвету
    $t->set_block("index", "available-sizes", "_available-sizes");
    //	$sizes	=	$cp->GetProductWarehouseSizes($product["id"], $_REQUEST["color"]); !-!old
    if ($_REQUEST['color']) {
        $cur_color = $_REQUEST['color'];
    }
    else
        $cur_color = '2';
    $sizes = $cp->GetSizesArray($product["id"], $_REQUEST["color"]);
    for ($i = 0; $i < count($sizes); $i++) {
        if ($sizes[$i]["id"] == $_REQUEST["size"]) $size_selected = "selected"; else $size_selected = "";
        $t->set_var(array(
            "SIZE_ID" => $sizes[$i]["id"],
            "SIZE_NAME" => $sizes[$i]["name"],
            "SIZE_SELECTED" => $size_selected
        ));
        $t->parse("_available-sizes", "available-sizes", true);
    }
    //	//	Размеры по выбранному цвету


    $t->parse("OUT2", "index");
    $t->p("OUT2");
    die();
}


#################################################################		ИНФОРМАЦИЯ О КОРЗИНЕ В ШАПКЕ САЙТА
if ($data->POST["what"] == "update_qty") {
    if (!$_COOKIE["basket"]) die();
    $b = $blocks->ShowHeaderBasketInfo();
    $price = number_format($b[1], 0, ',', ' ');
    echo $price;
    die();
}

#################################################################		ИНФОРМАЦИЯ О КОРЗИНЕ В ШАПКЕ САЙТА
if ($data->GET["what"] == "update_basket") {
    if (!$_COOKIE["basket"]) die();

    $t->set_file(array(
        "index" => "ajax_header_basket.tpl.htm"
    ));

    $b = $blocks->ShowHeaderBasketInfo();
    $price = number_format($b[1], 0, ',', ' ');
    $t->set_var(array(
        "BASKET_ITEMS" => $b[0],
        "BASKET_SUM" => $price
    ));

    $t->parse("OUT", "index");
    $t->p("OUT");
    die();
}


#################################################################		КРАТКАЯ ИНФОРМАЦИЯ О КОРЗИНЕ В ВЫПАДАЮЩЕМ ОКНЕ
if ($data->GET["what"] == "show_basket_preview") {
    if (!$_COOKIE["basket"]) die();

    $t->set_file(array(
        "index" => "ajax_header_basket_preview.tpl.htm"
    ));

    $z = explode("-", $_COOKIE["basket"]);
    $t->set_block("index", "basket_preview_products", "_basket_preview_products");
    for ($i = 0; $i < count($z); $i++) {
        $zz = explode(".", $z[$i]);
        $product = $cp->GetProductByID($zz[0]);
        $t->set_var(array(
            "BP_NAME" => $product[name],
            "BP_QTY" => $zz[3],
            "BP_UNIT" => $cp->GetUnitShortNameByID($product[units])
        ));
        unset($product);
        $t->parse("_basket_preview_products", "basket_preview_products", true);
    }


    $t->parse("OUT", "index");
    $t->p("OUT");
    die();
}
?>