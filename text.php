<?
#################################################################
require ("libs/fo_prepare.php");

$news = new NewsClass();

$what = "general";

$z	=	explode("/", $data->GET["url"]);
for ($i=0; $i<count($z); $i++) {
	$k	=	explode(".-", $z[$i]);
	$data->GET[$k[0]]	=	$k[1];
}
$data->GET["url"]	=	$z[(count($z)-1)];

$article	=	$news->GetNewsByURL($data->GET["url"]);
if (!$article["id"]) {
	header("Location: /");
	die();
}

$t->set_file(array(
    "index"		=>	"text.tpl.htm"
));

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");

if ($what == "general") {
	$blocks->ShowHeaderBasketInfo();		//	���������� ���������� � ������� � ����� �����
	$blocks->MainCatsDrop();				//	���������� ���� �� ������� ������� ��������

 	$t->set_var(array(
 		"TEXT_TEXT"			=>	$article["details"]
 	));



 	########################################	������ ��������
 	$t->set_block("index", "news-list", "_news-list");
 	$texts					=	$news->GetNewsByCat(9);		//	�������� ������
	for ($i=0; $i<count($texts); $i++) {
		$t->set_var(array(
			"NEWS_NAME"			=>	$texts[$i][headline],
			"NEWS_URL"			=>	$texts[$i][url]
	    ));
		$t->parse("_news-list", "news-list", true);
	}
 	########################################	//	������ ��������


	$t->set_block("index", "index_general", "_index_general");
	$t->parse("_index_general", "index_general", true);
}

$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>