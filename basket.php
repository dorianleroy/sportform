<?
#################################################################
require ("libs/fo_prepare.php");

$what = "general";

$z	=	explode("/", $data->GET["url"]);
for ($i=0; $i<count($z); $i++) {
	$k	=	explode(".-", $z[$i]);
	$data->GET[$k[0]]	=	$k[1];
}
$data->GET["url"]	=	$z[(count($z)-1)];

$t->set_file(array(
    "index"		=>	"basket.tpl.htm"
));

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");

if ($what == "general") {	$blocks->ShowHeaderBasketInfo();		//	отображаем информацию о корзине в шапке сайта
	$blocks->MainCatsDrop();				//	Выпадающее окно со списком главных разделов

	$basket_info	=	explode("-", $_COOKIE["basket"]);
	if (!strlen($basket_info[0])) {		$blocks->HideBlock("index", "not_empty_basket");	} else {		$blocks->HideBlock("index", "empty_basket");
		$t->set_block("index", "basket_items", "_basket_items");
		$all_sum	=	0;
		for ($i=0; $i<(count($basket_info)-1); $i++) {			$z			=	explode(".", $basket_info[$i]);
			$product	=	$cp->GetProductByID($z[0]);
			$p_color	=	$cp->GetProductColor($z[1]);
			$p_color0	=	$cp->GetProductColor($product[color]);
			$p_size		=	$cp->GetProductSize($z[2]);
			$all_sum	+=	($product[price] * $z[3]);			$t->set_var(array(
				"PRODUCT_I"						=>	($i+1),
				"PRODUCT_VENDOR"				=>	$product[vendor_name],
				"PRODUCT_ID"					=>	$product[id],
				"PRODUCT_URL"					=>	$product[url],
				"PRODUCT_NAME"					=>	$product[name],
				"PRODUCT_CODE"					=>	$product[code],
				"PRODUCT_COLOR"					=>	$z[1],
				"PRODUCT_COLOR_NAME"			=>	$p_color[name],
				"PRODUCT_COLOR_RGB"				=>	$p_color[color],
				"PRODUCT_COLOR0_NAME"			=>	$p_color0[name],
				"PRODUCT_COLOR0_RGB"			=>	$p_color0[color],
				"PRODUCT_QTY"					=>	$z[3],
				"PRODUCT_SUM"					=>	number_format(($product[price] * $z[3]), 0, ',', ' '),
				"PRODUCT_SIZE"					=>	$p_size[name],
				"PRODUCT_SIZE_ID"				=>	$z[2]
			));			$t->parse("_basket_items", "basket_items", true);
			unset($product);		}

		$t->set_var(array(
			"PRODUCTS_SUM"						=>	number_format($all_sum, 0, ',', ' ')
		));	}

	$t->set_block("index", "index_general", "_index_general");
	$t->parse("_index_general", "index_general", true);}

$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>