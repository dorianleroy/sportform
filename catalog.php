<?
#################################################################
require ("libs/fo_prepare.php");


$what = "general";

$z	=	explode("/", $data->GET["url"]);

for ($i=0; $i<count($z); $i++) {
	$k	=	explode(".-", $z[$i]);
	$data->GET[$k[0]]	=	$k[1];
}
$data->GET["cid_url"]	=	$z[(count($z)-1)];

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

$cid	        =	$cp->GetCategoryIDByURL($data->GET["cid_url"]);
$parentCid      =   $cp->GetParentCat($cid);

if (!$cid) {
    $vendor_array = array(
        'puma',
        'adidas',
        'reebok',
        'umbro',
        'asics',
        '8848altitude',
        'mizuno'
    );
    if (in_array($what,$vendor_array)){
        $from_vendor = true;
        $data->GET['cid_url'] = $what;
    }
    else
    {
        header("Location: /");
        die();
    }
}


if ($data->GET['reload'] == 'none'){
    $t->set_file(array(
        "index"		=>	"catalog_filter_ajax.tpl.htm"
    ));
}
else {
    $t->set_file(array(
        "index"		=>	"catalog.tpl.htm"
    ));
}


//$actions = array("general");

###
//if (!in_array($what, $actions)) $what = "general";
###

$blocks->HideBlocks($what, $actions, "index");

	$cat	=	$cp->GetCatByID($cid);

	$MESSAGES["site_title"]		=	$cat["name"];
    $p_cid = (!$parentCid['pid']) ? "'1'" : $parentCid['pid'];
	$t->set_var(array(
		"SCRIPT_FIND"		=>	$data->GET["cid_url"],
		"SCRIPT_PRICE_MIN"	=>	$data->GET["min"],
		"SCRIPT_PRICE_MAX"	=>	$data->GET["max"],
		"SCRIPT_SIZE"		=>	$data->GET["size"],
		"SCRIPT_COLOR"		=>	$data->GET["color"],
		"SCRIPT_VENDOR"		=>	$data->GET["vendor"],
		"SCRIPT_URL"		=>	"/c",
		"CATALOG_TITLE"		=>	$cat["name"],
		"CATALOG_URL"		=>	$data->GET["cid_url"],
        "MAIN_CAT_ID_MENU"  => $p_cid
	));

	$t->set_block("index", "found_products", "_found_products");
	$t->set_block("found_products", "av_sizes", "_av_sizes");

	$cats_list 	= $cp->GetSubCatsInCat($cid, array());
	$cats_list[count($cats_list)] = $cid;

    if ($from_vendor == false){

//        $found	=	$cp->GetProductsByCatsList($cats_list, 0, 10000);

        $found	=	$cp->GetProdByIdCatNew($cid);
        $found_all	=	$found;
        $found	=	$cp->ProductsDoFilters($found_all, array($data->GET["min"], $data->GET["max"]), $data->GET["size"], $data->GET["color"], $data->GET["vendor"]);


    }else{

        $found = $cp->GetProductByVendor($what);

        $found_all = $found;
    }


//echo "<pre>",var_dump($found2),"</pre>";
//    echo "<hr>";
//    echo "<pre>",var_dump($found),"</pre>";
//exit;

	if (count($found)) {
		$blocks->HideBlock("index", "notfound_products");
	} else {
		$t->set_var("_found_products", "");
	}

	for ($i=0; $i<count($found); $i++) {

		$product					=	$cp->GetProductByID($found[$i]["id"]);

        if($product){

		if (!($product[rating_count]) || ($product[rating_count] == 0)) $product[rating_count]	=	0.0001;

		$product_rating	=	round(($product[rating_sum] / $product[rating_count]) * 20);
		$sizes	=	$cp->GetProductWarehouseSizes_2($product["id"], $data->GET["color"]);

		if (!count($sizes)) {
			$hide_buy_button	=	"display: none;";
			$t->set_var("_av_sizes", "нет в наличии");
		} else {
			$hide_buy_button	=	"";
			for ($is=0; $is<count($sizes); $is++) {
				if ($is < (count($sizes)-1)) $av_sizes_point = " - "; else $av_sizes_point = "";
				$t->set_var(array(
					"AV_SIZE_NAME"		=>	$sizes[$is]["name"],
					"AV_SIZE_POINT"		=>	$av_sizes_point
				));
   				$t->parse("_av_sizes", "av_sizes", true);
		 	}
		}

//		$colors		=	$cp->GetProductColorList($found[$i]["id"]);
        $colors		=	$cp->GetColorsWare($found[$i]["id"]);
		$color		=	$colors[0];

        if ($product['old_price'] == 0.00)
             $style_o = "nonOldPrice";
        else
             $style_o = "oldPrice";

		$t->set_var(array(
			"ITEM_NUM"						=>	($i+1),
			"PRODUCT_ID"					=>	$product[id],
			"PRODUCT_URL"					=>	$product[url],
			"PRODUCT_NAME"					=>	$product[name],
			"PRODUCT_CODE"					=>	$product[code],
			"PRODUCT_VENDOR_NAME"			=>	$product[vendor_name],
			"PRODUCT_VENDOR_COUNTRY"		=>	$product[vendor_name_country],
			"PRODUCT_PRICE"					=>	number_format($product[price], 0, ',', ' '),
			"PRODUCT_FORUNIT"				=>	$product[for_unit],
			"PRODUCT_PRICE2"				=>	number_format($product[price2], 0, ',', ' '),
			"PRODUCT_FORUNIT2"				=>	$product[for_unit2],
			"PRODUCT_WAREHOUSE"				=>	$product[warehouse],
			"PRODUCT_UNIT"					=>	$cp->GetUnitByID($product[units]),
			"PRODUCT_SHORT_DESCRIPTION"		=>	$_descr,
			"PRODUCT_FULL_DESCRIPTION"		=>	$_full_descr,
			"PRODUCT_FULL_DESCRIPTION_PART1"=>	$_full_descr_parts[0],
			"PRODUCT_FULL_DESCRIPTION_PART2"=>	$_full_descr_parts[1],
			"PRODUCT_RATING"				=>	$product_rating,
			"PRODUCT_COLOR"					=>	$color,
			"PRODUCT_SIZES"					=>	count($sizes),
			"PRODUCT_BUY_HIDDEN"			=>	$hide_buy_button,
            "PRODUCT_OLD_PRICE"             =>  number_format($product[old_price], 0, ',', ' '),
            "PRICE_OLD_STYLE"               =>  $style_o
		));
 		$t->parse("_found_products", "found_products", true);
 		$t->set_var("_av_sizes", "");
 		unset($product);
	}
    }

    // 3 обязательных фильтра для каждой страницы

	#############################################################################	фильтр по цене
	$filter_price	=	$cp->GetProductsPriceRange($found_all);
	if ($data->GET["min"]) $_min	=	$data->GET["min"]; else $_min	=	$filter_price["min"];
	if ($data->GET["max"]) $_max	=	$data->GET["max"]; else $_max	=	$filter_price["max"];
	$t->set_var(array(
		"FILTER_PRICE_MIN"			=>	number_format($_min, 0, ',', ' '),
		"FILTER_PRICE_MAX"			=>	number_format($_max, 0, ',', ' '),
		"FILTER_PRICE_MIN_VALUE"	=>	$filter_price["min"],
		"FILTER_PRICE_MAX_VALUE"	=>	$filter_price["max"],
		"FILTER_PRICE_MIN_VALUE0"	=>	$_min,
		"FILTER_PRICE_MAX_VALUE0"	=>	$_max
	));

   #############################################################################	фильтр по производителям
//echo "<pre>",var_dump($found_all), "</pre>"; exit;
   $vendors = $cp->GetProductsVendors($found_all);

   //simple filter for old code
   foreach($vendors as $k=>$v){
       $vendorsTemp[$v['id']] = $v['name'];
   }

   $vendors = $vendorsTemp;
   // //filter

	$blocks->ShowHeaderBasketInfo();		//	отображаем информацию о корзине в шапке сайта
	$blocks->MainCatsDrop();				//	Выпадающее окно со списком главных разделов

	////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////	Рисуем фильтры
	////////////////////////////////////////////////////////////////////////////////////////////////

    if ($what=='general'){
//	if (count($found_all)) {
		$blocks->ShowFiltersRow();				//	Блок с фильтрами
//	} else {
//		$t->set_var("BLOCK_FILTERS_ROW", "");
//	}
    }
    else{
        $t->set_var("BLOCK_FILTERS_ROW","");
    }


	////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////	Рисуем категории
	////////////////////////////////////////////////////////////////////////////////////////////////
	$cid_is_parent	=	$cp->IsThisParentCat($cid);
	$path = $cp->GetPathFromCID($cid);

	if (!$cid_is_parent) {
		if (count($path) < 5) $cat_level0	=	count($path)-1; else $cat_level0	=	3;
	} else {
		if (count($path) < 5) $cat_level0	=	count($path)-1; else $cat_level0	=	2;
	}

	$t->set_var(array(
		"CAT_LEVEL0_NAME"		=>	$path[$cat_level0]["short_name"],
		"CAT_LEVEL0_URL"		=>	$path[$cat_level0]["url"],
		"CAT_LEVEL1_NAME"		=>	$path[($cat_level0-1)]["short_name"],
		"CAT_LEVEL1_URL"		=>	$path[($cat_level0-1)]["url"],
	));

	$t->set_block("index", "cat_level2", "_cat_level2");
	$t->set_block("cat_level2", "cat_level3", "_cat_level3");
	$cat_level2	=	$cp->GetSimpleCatsList($path[($cat_level0-1)]["id"]);
	if (!count($cat_level2)) $t->set_var("_cat_level2", "");
	for ($i=0; $i<count($cat_level2); $i++) {
		if ($cat_level2[$i]["id"] == $path[($cat_level0-2)]["id"]) $cat_level2_open = "open"; else $cat_level2_open = "";

		$t->set_var(array(
			"CAT_LEVEL2_URL"		=>	$cat_level2[$i]["url"],
			"CAT_LEVEL2_NAME"		=>	$cat_level2[$i]["short_name"],
			"CAT_LEVEL2_OPEN"		=>	$cat_level2_open
		));

		if ($cat_level2[$i]["id"] == $path[($cat_level0-2)]["id"]) {
			$cat_level3	=	$cp->GetSimpleCatsList($path[($cat_level0-2)]["id"]);
			if (!count($cat_level3)) $t->set_var("_cat_level3", "");
			for ($i3=0; $i3<count($cat_level3); $i3++) {
				if ($cat_level3[$i3]["id"] == $path[($cat_level0-3)]["id"]) $cat_level3_open = "open"; else $cat_level3_open = "";
				$t->set_var(array(
					"CAT_LEVEL3_URL"		=>	$cat_level3[$i3]["url"],
					"CAT_LEVEL3_NAME"		=>	$cat_level3[$i3]["short_name"],
					"CAT_LEVEL3_OPEN"		=>	$cat_level3_open
				));
				$t->parse("_cat_level3", "cat_level3", true);
			}
		}

		$t->parse("_cat_level2", "cat_level2", true);
		$t->set_var("_cat_level3", "");
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////	//	Рисуем категории
	////////////////////////////////////////////////////////////////////////////////////////////////


	$t->set_block("index", "index_general", "_index_general");
	$t->parse("_index_general", "index_general", true);



$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

/*
$ff	=	fopen("./logs/auto_".date("Ymd-Hms")."-".microtime(true).".txt", "w");
fputs($ff, $t->finish($t->get_var("OUT"))."\n");
fclose($ff);
*/

$t->p("OUT");
?>