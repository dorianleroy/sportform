<?
#################################################################
//require ("libs/fo_prepare.php");
require ("libs/library.php");
$cp = new CATS_AND_PRODUCTSClass();
$_img_product_dir 		= "/uploads/images/products/";

//phpinfo();
//die();
//http://sf.i-see-it.ru/product-image/width.-360/height.-360/krossovki_Asics_Kayano_17

$z	=	explode("/", $data->GET["url"]);

for ($i=0; $i<count($z); $i++) {
	$k	=	explode(".-", $z[$i]);
	$data->GET[$k[0]]	=	$k[1];
}

$data->GET["pid"]	=	$cp->GetProductIDByURL($z[(count($z)-1)]);

$width	=	110;
$height	=	110;

$pid	=	$data->GET["pid"];

if (!$data->GET["mask"])	$mask		=	"main"; else $mask	=	$data->GET["mask"];
if ($data->GET["width"]) 	$width		=	$data->GET["width"];
if ($data->GET["height"]) 	$height		=	$data->GET["height"];
if ($data->GET["decrease"]) $decrease	=	$data->GET["decrease"];
if (!$data->GET["color"]) 	{
	$colors		=	$cp->GetProductColorList($pid);
	$color		=	$colors[0];
} else $color		=	$data->GET["color"];

//$_img_product_dir	= $_default_images_dir_products;

if (!$pid) {
	$img	=	$cp->GetImageForPID($_img_product_dir, $mask, -1);
	header("Location: ".$img);
	die();
}	else {
	$img	=	$cp->GetImageForPID($_img_product_dir, $mask, $pid, $color);
}

if ($data->GET["size"]	==	"original") {
	$photo_product = ImageCreateFromJPEG(".".$img);
	$width = ImageSX($photo_product);
	$height = ImageSY($photo_product);
	ImageDestroy($photo_product);
}

header("Content-type: image/jpeg");

if ($decrease) {
	$photo_product = ImageCreateFromJPEG($img);
	$photoW = ImageSX($photo_product);
	$photoH = ImageSY($photo_product);
	$width	=	($decrease * $photoW) / 100;
	$new_width	=	$width;
	$height	=	($decrease * $photoH) / 100;
	$new_height	=	$height;
	ImageDestroy($photo_product);
}
$filename	=	$data->GET["pid"]."_".$mask."-".$color."x".$width."x".$height.".jpg";

if ( (!file_exists(".".$_img_product_dir.$filename)) || ( filectime(".".$_img_product_dir.$filename) < filectime(".".$img) ) ) {

	$photo_product = ImageCreateFromJPEG(".".$img);
	ImageAlphaBlending($photo_product, true);

	$photoW = ImageSX($photo_product);
	$photoH = ImageSY($photo_product);

	if (!$decrease) {
		if (($photoW/$photoH) > 1) {
			$new_width	=	$width;
			$new_height	=	($photoH * $width) / $photoW;
			$new_x		=	0;
			$new_y		=	($height - $new_height)/2;
		}

		if (($photoW/$photoH) == 1) {
			$new_width	=	($photoW * $height) / $photoH;
			$new_height	=	($photoH * $width) / $photoW;
			$new_x		=	($width - $new_width)/2;
			$new_y		=	($height - $new_height)/2;
		}

		if (($photoW/$photoH) < 1) {
			$new_width	=	($photoW * $height) / $photoH;
			$new_height	=	$height;
			$new_x		=	($width - $new_width)/2;
			$new_y		=	0;
		}
	} else {
		$width	=	($decrease * $photoW) / 100;
		$new_width	=	$width;
		$height	=	($decrease * $photoH) / 100;
		$new_height	=	$height;
	}

	$FinishImage	=	imagecreatetruecolor($width, $height);
	$white 			=	imagecolorallocate($FinishImage, 255, 255, 255);
	imagefill($FinishImage, 0, 0, $white);
	//imagecopyresized($FinishImage, $photo_product, $new_x, $new_y, 0, 0, $new_width, $new_height, $photoW, $photoH);
	imagecopyresampled($FinishImage, $photo_product, $new_x, $new_y, 0, 0, $new_width, $new_height, $photoW, $photoH);

	ImageDestroy($photo_product);

	/*
	if ($height > 300) {
		if ($width <= 200) $n	=	"only_logo.png";
		if ($width > 200) $n	=	"logo_text.png";
		if ($width > 350) $n	=	"logo_text_350.png";
		if ($width > 500) $n	=	"logo_text_500.png";
		$wfile_name	=	$_default_images_dir.$_default_images_dir_watermarks.$n;
		$watermark = ImageCreateFromPNG($wfile_name);
		ImageAlphaBlending($watermark, true);
		imagesavealpha($watermark, true);
		$water_width	=	ImageSX($watermark);
		$water_height	=	ImageSY($watermark);
		$water_x		=	round( ($width-$water_width) / 2);
		$water_y		=	round( ($height-$water_height) / 2);
		imagecopymerge($FinishImage, $watermark, $water_x, $water_y, 0, 0, $water_width, $water_height, 30);
	}
    */

	ImageJPEG($FinishImage, ".".$_img_product_dir.$filename, 90);
	chmod(".".$_img_product_dir.$filename, 0666);


	ImageJPEG($FinishImage, '', 90);

} else {
	$f	=	fopen(".".$_img_product_dir.$filename, "r");
	echo fread($f, filesize(".".$_img_product_dir.$filename));
	fclose($f);
}

?>