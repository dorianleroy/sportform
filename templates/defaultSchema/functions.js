var MouseX, MouseY;
var loading_div = "";

// уcтанавливает cookie
function setCookie(name, value, props) {
	props = props || {}
	var exp = props.expires
	if (typeof exp == "number" && exp) {
		var d = new Date()
		d.setTime(d.getTime() + exp*1000)
		exp = props.expires = d
	}
	if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

	value = encodeURIComponent(value)
	var updatedCookie = name + "=" + value
	for(var propName in props){
		updatedCookie += "; " + propName
		var propValue = props[propName]
		if(propValue !== true){ updatedCookie += "=" + propValue }
	}
	document.cookie = updatedCookie

}

// возвращает cookie если есть или undefined
function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
	  "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	))
	return matches ? decodeURIComponent(matches[1]) : undefined
}


function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

function mousePageXY(e) {
  var x = 0, y = 0;

  if (!e) e = window.event;

  if (e.pageX || e.pageY)
  {
    x = e.pageX;
    y = e.pageY;
  }
  else if (e.clientX || e.clientY)
  {
    x = e.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft) - document.documentElement.clientLeft;
    y = e.clientY + (document.documentElement.scrollTop || document.body.scrollTop) - document.documentElement.clientTop;
  }

  return {"x":x, "y":y};
}

//	проверяем поле с поиском
function CheckSearch() {
	var	def_q	=	"Название товара, артикул или производитель";
	var q	=	$("#search").val();
	if (q	==	"") {
		$("#search").val(def_q);
		$("#search").css("color", "#666666");
	}

	$('#search').focus(function() {
		q	=	$("#search").val();
		if (q	==	def_q) {
			$("#search").css("color", "#000000");
			$("#search").val("");
		}
	});

	$('#search').focusout(function() {
		q	=	$("#search").val();
		if (q	==	"") {
			$("#search").css("color", "#666666");
			$("#search").val(def_q);
		}
	});

	$('#search_mini_form').submit(function() {
		q	=	$("#search").val();
		if (q != def_q) document.location.href	=	"/s/"+$("#search").val();
		return false;
	});

	var search_cache = {};
	$( "#search" ).autocomplete({
		minLength: 2,
		source: function(request, response) {

			if ( request.term in search_cache ) {
				response( search_cache[ request.term ] );
                alert("asdf");
				return;
			}


            //console.log(test);

			$.ajax({
				url: "/autocomplete.php",
				dataType: "json",
				data: "what=search_auto&q="+encodeURIComponent($("#search").val()),
				success: function( data ) {
					search_cache[ request.term ] = data;
					response( data );
				},
                error: function (data){
                    alert("error - " + data);
                }
			});
		}
	});
}

///	координаты элемента
function getOffsetRect(elem) {
    // (1)
    var box = elem.getBoundingClientRect()

    // (2)
    var body = document.body
    var docElem = document.documentElement

    // (3)
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft

    // (4)
    var clientTop = docElem.clientTop || body.clientTop || 0
    var clientLeft = docElem.clientLeft || body.clientLeft || 0

    // (5)
    var top  = box.top +  scrollTop - clientTop
    var left = box.left + scrollLeft - clientLeft

    return { top: Math.round(top), left: Math.round(left) }
}

///	в описании товара нажали на выбор цвета
function SelectColor(radio, color, url, pid) {
	var old = $('input[name='+radio+']').val();

    $.post("/autocomplete.php", {color:color, what:'getsizefromcolor', pid:pid}, function(data_json){
        //alert(data);
        var data = JSON.parse(data_json);

        $(".avail_sizes").remove();
        if ( data != null)
        for(var i=0, j=data.length; i<j; i++){
            $("#selected_size,#basket-selected_size").append("<option class='avail_sizes' value=" + data[i].id + ">" + data[i].name + "</option>");
        }
    });

    if (old != color) {
        $('input[name='+radio+']').val(color);
//        $.post("/autocomplete.php", {color:color, what:'addphotos', pid:pid},
//            function(data_json){
//
//                var data = JSON.parse(data_json)
//                if(typeof(data) == 'object'){
//                    var n = $('input[name=selected_color]').val();
//                    $('.additional_photos').empty();
//                    for(var i=0; i<data.length-1; i++){
//                        $('.additional_photos').append('<a class="pirobox_gal1" title="essential - фото 2" href="/i/size.-original/color.-'+n+'/mask.-'+data[i]+'/essential" rel="2&nbsp;из&nbsp;4" rev="1"><img width="50" height="50" border="0" style="border: 1px solid #aaaaaa;" title="essential - фото 4" alt="essential - фото 4" src="/i/width.-360/height.-360/color.-'+n+'/mask.-'+data[i]+'/essential"></a>');
//                    }
//                    $('.piro_overlay').remove();
//                    $('.pirobox_content').remove();
//                    $().piroBox({
//                        my_speed: 400, //animation speed
//                        bg_alpha: 0.5, //background opacity
//                        slideShow : false, // true == slideshow on, false == slideshow off
//                        slideSpeed : 4, //slideshow duration in seconds(3 to 6 Recommended)
//                        close_all : '.piro_close,.piro_overlay'// add class .piro_overlay(with comma)if you want overlay click close piroBox
//                    });
//                }
//        });

        OnColorChange(url);

    }

}

///	если изменился выбор цвета на странице с описанием товара
function OnColorChange(url) {
	$('.product_color .label i').removeClass('ui-corner-all selected_color');
	$('.basket-product_color .label i').removeClass('ui-corner-all selected_color');
	var n = $('input[name=selected_color]').val();
	$('#i_selected_color_'+n).addClass('ui-corner-all selected_color');
	$('#bi_selected_color_'+n).addClass('ui-corner-all selected_color');
	$('.already_set_color').show();
	$('.unselect_color').hide();
    //change image
	$('.product_image').fadeTo('slow', 0.5);
	$('.additional_photos').fadeTo('slow', 0.5);
//	$('.product_image').load('/p/what.-product_images/color.-'+n+'/'+url);
//    $('.additional_photos img').attr('src','/i/color.-'+n+'/'+url);
    $('.product_image img').attr('src','/i/width.-360/height.-360/color.-'+n+'/'+url);
    $('.product_image a').attr('href','/i/size.-original/color.-'+n+'/'+url);

//    $('.additiional_photos a').attr('href','/i/size.-original/color.-'+n+'/'+url);
//    $('.additional_photos img').attr('src','/i/width.-360/height.-360/color.-'+n+'/'+url);
	$('.additional_photos').load('/p/what.-add_product_images/color.-'+n+'/'+url, function(data){

        $('.additional_photos').html(data);
        $('.piro_overlay').remove();
        $('.pirobox_content').remove();
        $().piroBox({
            my_speed: 400, //animation speed
            bg_alpha: 0.5, //background opacity
            slideShow : false, // true == slideshow on, false == slideshow off
            slideSpeed : 4, //slideshow duration in seconds(3 to 6 Recommended)
            close_all : '.piro_close,.piro_overlay'// add class .piro_overlay(with comma)if you want overlay click close piroBox
        });
    });
	/*$('.already_set_color').load('/p/what.-sizes_for_selected_color/color.-'+n+'/'+url, function() {
		  $('#basket-selected_size').html(
		  	$('#selected_size').html()
		  );
	});  */
	$('.product_image').fadeTo('slow', 1);
	$('.additional_photos').fadeTo('slow', 1);
}

///	Открыть список основных категорий в левом столбце
function ToggleMainCatsDrop() {
	var p_n;
	var p	=	$('.main_cats_drop_button .title img').attr('type');
	if (p == 1) p_n = 2; else p_n = 1;
	var src = $('.main_cats_drop_button .title img').attr('src');
	src = src.replace(new RegExp("arrow"+p,'g'), "arrow"+p_n);
	$('.main_cats_drop_button .title img').attr('type', p_n);
	$('.main_cats_drop_button .title img').attr('src', src);

	$('.main_cats_drop_window').toggle();
	var h = $('.main_cats_drop_window_page').height();
	//$('.main_cats_drop_window_shadow').height((h+17));
	//$('.main_cats_drop_window_shadow img').css('top', (h+9)+"px");

	//MakeShadow('.main_cats_drop_window .main_cats_drop_window_page', '.main_cats_drop_window .main_cats_drop_window_shadow1', '*', 5, '#999999', 4, 4, 4, 4, 5, 5, 5, 5);
}

///	Нажали кнопку "купить"
function ButtonHowToPay() {

	var bw	=	197;
	var bh	=	191;
	var bw_off	=	268;
	var bh_off	=	36;

	$("#add_to_basket2").css('left', (MouseX - bw + bw_off)+'px');
	$("#add_to_basket2").css('top', (MouseY - (bh - bh_off))+'px');

	var w = $(window);
	$(".add_to_basket_page_all_shadow").css("width", (w.width()-1)+"px");
	$(".add_to_basket_page_all_shadow").css("height", getDocHeight()+"px");
	$("#add_to_basket2 .add_to_basket_page_all_shadow").css("width", "1px");
	$("#add_to_basket2 .add_to_basket_page_all_shadow").css("height", "1px");

	$('.add_to_basket_page_all_shadow').toggle();
	$('#add_to_basket2').toggle();

	$('#add_to_basket2 .add_to_basket_page').load('/autocomplete.php?what=addhowtopay', function(data) {
		MakeShadow('#add_to_basket2 .add_to_basket_page', '#add_to_basket2 .add_to_basket_page_shadow1', '*', 5, '#999999', 4, 4, 4, 4, 5, 5, 5, 5);

	});
}

///	Нажали кнопку "купить"
function ButtonAdd2Basket(purl, def_color) {
	if (loading_div == '') loading_div = $('#add_to_basket .add_to_basket_page').html();
    // purl =  escape(purl);
    //alert(purl);

	$('#add_to_basket .add_to_basket_page').html(loading_div);

	var bw	=	277;
	var bh	=	271;
	var bw_off	=	68;
	var bh_off	=	36;

	$("#add_to_basket").css('left', (MouseX - bw + bw_off)+'px');
	$("#add_to_basket").css('top', (MouseY - (bh - bh_off))+'px');

	var w = $(window);
	$(".add_to_basket_page_all_shadow").css("width", (w.width()-1)+"px");
	$(".add_to_basket_page_all_shadow").css("height", getDocHeight()+"px");
	$("#add_to_basket .add_to_basket_page_all_shadow").css("width", "1px");
	$("#add_to_basket .add_to_basket_page_all_shadow").css("height", "1px");

	/*
	$("#add_to_basket .add_to_basket_page .add_to_basket_page_main .basket-product_color").html(
		$(".product_color").html()
	);
	*/
	var color 	=	$('#selected_color').val();
	if (!color) color	=	def_color;
	var size	=	$('#selected_size').val();

	$('.add_to_basket_page_all_shadow').toggle();
	$('#add_to_basket').toggle();

	$('#add_to_basket .add_to_basket_page').load('/autocomplete.php?what=add2basketwindow&pid='+purl+"&color="+color+"&size="+size, function(data) {
		MakeShadow('#add_to_basket .add_to_basket_page', '#add_to_basket .add_to_basket_page_shadow1', '*', 5, '#999999', 4, 4, 4, 4, 5, 5, 5, 5);

	});
}

///	Нажали кнопку "купить"
function ButtonAdd2BasketProduct(purl, pid) {

    var size	=	$('#selected_size').val();

    if (size != 0) {
//        alert('asdf');return false;
        var pid 	= pid;
        var color 	= $("#selected_color").val();
        var size 	= $("#selected_size").val();
        var qty 	=  1;
        var nb		=	"";
        var	n		=	1;
        var b		=	getCookie('basket');
        //alert(typeof b);
        if (typeof b != 'undefined') {
            var	bs		=	b.split('-');
//           alert(bs);return false;
            for (var i=0; i<(bs.length-1); i++) {
                var z	=	bs[i].split('.');
                if ( (z[0] == pid) && (z[1] == color) && (z[2] == size) ) {
                    z[3] = parseInt(z[3]) + parseInt(qty);
                    n	=	0;
                }
                nb +=	z[0] + '.' + z[1] + '.' + z[2] + '.' + z[3] + '-';
            }
        }
        if (n) nb +=	pid + '.' + color + '.' + size + '.' + qty + '-';
        //alert(nb);
        setCookie("basket", nb, {expires: 630720000, path: "/"});
        //.delay(800)
        $('.add_to_basket_page_main').html('<div class="add_to_basket_page_ok"></div>');
        $('#add_to_basket').delay(800).fadeOut('slow');
        $('.add_to_basket_page_all_shadow').delay(800).fadeOut('slow');
        $('.header_top .header_top_basket').load('/autocomplete.php?what=update_basket');
        window.location.href = "/b";
    } else {
//        alert("fdsa"); return false;
        $('#selected_size').css('background-color', '#D95F5F');
        $('#selected_size').focus();
    }
}

///	Нажали кнопку "купить"
function ButtonAdd2BasketClose() {
	$('.add_to_basket_page_all_shadow').toggle();
	$('#add_to_basket').toggle();
}

///	Нажали кнопку "купить"
function HowToBuyClose() {
	$('.add_to_basket_page_all_shadow').toggle();
	$('#add_to_basket2').toggle();
}

///	Сделать тень из div'а
function MakeShadow(parent, shadow, def_html, len, color, tlr, trr, blr, brr, tp, bp, lp, rp) {
	$(shadow).css('position', 'relative');
	var ph = $(parent).height();
	var hh = "";
	var opacity = Math.round(100 / len);
	var op	=	Math.round(100 / len);

	//console.log($(parent).width());
	var width = $(parent).width();
	var height = $(parent).height();

	$(shadow).width( $(parent).width()+lp+rp );
	$(shadow).height( $(parent).height()+tp+bp );

	//var left = $(parent).offset().left - lp;
	$(shadow).css('left', '-'+lp+'px');
	$(shadow).css('top', '-'+(tp+$(parent).height())+'px');
	//console.log(left);

	var htm = $(shadow).html();
	if (htm == def_html) {
		htm = '';

		for (var i=0; i<len; i++) {
			var st	=	'top:'+(tp-(1+i))+'px;';
			var sl	=	'left:'+(lp-(1+i))+'px;';

			tlrh	=	"-moz-border-radius-topleft: "+(tlr+i)+"px; -webkit-border-top-left-radius: "+(tlr+i)+"px; -khtml-border-top-left-radius: "+(tlr+i)+"px; border-top-left-radius: "+(tlr+i)+"px;";
			trrh	=	"-moz-border-radius-topright: "+(trr+i)+"px; -webkit-border-top-right-radius: "+(trr+i)+"px; -khtml-border-top-right-radius: "+(trr+i)+"px; border-top-right-radius: "+(trr+i)+"px;";
			blrh	=	"-moz-border-radius-bottomleft: "+(blr+i)+"px; -webkit-border-bottom-left-radius: "+(blr+i)+"px; -khtml-border-bottom-left-radius: "+(blr+i)+"px; border-bottom-left-radius: "+(blr+i)+"px;";
			brrh	=	"-moz-border-radius-bottomright: "+(brr+i)+"px; -webkit-border-bottom-right-radius: "+(brr+i)+"px; -khtml-border-bottom-right-radius: "+(brr+i)+"px; border-bottom-right-radius: "+(brr+i)+"px;";

			if (tp) tph = "padding-top:"+(i+1)+"px;"; else tph = "";
			if (bp) bph = "padding-bottom:"+(i+1)+"px;"; else bph = "";
			if (lp) lph = "padding-left:"+(i+1)+"px;"; else lph = "";
			if (rp) rph = "padding-right:"+(i+1)+"px;"; else rph = "";

			htm = htm+'<div style="position:absolute;'+sl+st+'width:'+width+'px;height:'+height+'px;background-color:'+color+';opacity: 0.'+opacity+';filter:progid:DXImageTransform.Microsoft.Alpha(opacity='+opacity+');'+tph+bph+lph+rph+tlrh+trrh+blrh+brrh+'"></div>\n';
			//opacity -= op;
		}
		$(shadow).html(htm);
	}
}

///	Нажали "ОК" для выпавшего окна "добавить в корзину"
function ButtonAdd2BasketOK() {
	if ($("#basket-selected_size").val() != 0) {
		var pid 	= $("#basket-pid").val();
		var color 	= $("#basket-selected_color").val();
		var size 	= $("#basket-selected_size").val();
		var qty 	= $("#basket-selected_qty").val();
		var nb		=	"";
		var	n		=	1;
		var b		=	getCookie('basket');
//        alert(size); return false;
//        if (size.indexOf(".") == -1){
//
//        }
//        else {
//            size = size.split(".").join(",");
//
//        }
		if (typeof b != 'undefined') {
			var	bs		=	b.split('-');
//           alert(bs);return false;
			for (var i=0; i<(bs.length-1); i++) {
				var z	=	bs[i].split('.');
				if ( (z[0] == pid) && (z[1] == color) && (z[2] == size) ) {
					z[3] = parseInt(z[3]) + parseInt(qty);
					n	=	0;
				}
				nb +=	z[0] + '.' + z[1] + '.' + z[2] + '.' + z[3] + '-';
			}
		}
		if (n) nb +=	pid + '.' + color + '.' + size + '.' + qty + '-';
		//alert(nb);
		setCookie("basket", nb, {expires: 630720000, path: "/"});
		//.delay(800)
		$('.add_to_basket_page_main').html('<div class="add_to_basket_page_ok"></div>');
		$('#add_to_basket').delay(800).fadeOut('slow');
		$('.add_to_basket_page_all_shadow').delay(800).fadeOut('slow');
		$('.header_top .header_top_basket').load('/autocomplete.php?what=update_basket');
        window.location.href = "/b";
	} else {
		$('#basket-selected_size').css('background-color', '#D95F5F');
		$('#basket-selected_size').focus();
	}
}

function howtopay(){
    var w = $(window);

    /*
    $(".add_to_basket_page_all_shadow").css("width", (w.width()-1)+"px");
    $(".add_to_basket_page_all_shadow").css("height", getDocHeight()+"px");
    $("#add_to_basket .add_to_basket_page_all_shadow").css("width", "1px");
    $("#add_to_basket .add_to_basket_page_all_shadow").css("height", "1px");*/

}

/// Analog of implode from php, special for size's
function implode( glue, pieces ) {	// Join array elements with a string
	//
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: _argos

	return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
}


///	Проверяем выбранный размер
function CheckSelectedSize() {
	if ($("#basket-selected_size").val() != 0) {
		$('#basket-selected_size').css('background-color', '#FFFFFF');
	}
}

///	Показать окошко со всеми доступными размерами
function ShowAvSizes(pid, html) {
	var Xpos	=	MouseX - 138, ScrollY	=	(document.documentElement.scrollTop || document.body.scrollTop) - document.documentElement.clientTop, Ypos	=	MouseY - (150 + ScrollY);

	$("#dialog-sizes").dialog("option", "position", [Xpos,Ypos]);

	$( "#dialog-sizes" ).html(html);

	$( "#dialog-sizes" ).dialog( "open" );
}

///	В фильтрах выбрали какой-то размер
function FiltersSelectSize(size_id, div) {
//
//	$(".filters .filter_size .filter_sizes .filter_size_item").removeClass('filters_selected_size');
//    //alert(script_size);
//    //alert(script_size.length);
//
//    $(".filters .filter_size .filter_sizes .filter_size_item").removeClass('filters_selected_size');



    if ($("#" + div).is('.filters_selected_size'))
    {
        $("#" + div).removeClass('filters_selected_size');

        //удалить значение из массива
        for(var key in size_array){
            var val = size_array[key];
            if(val == size_id){
                size_array.splice(key,1);
            }
        }
        size_id=implode('&',size_array);
    }
    else
    {
        $("#" + div).addClass('filters_selected_size');
        size_array.push(size_id);
        size_id=implode('&',size_array);
    }

//    console.log(size_id);

//        $("#" + div).removeClass('filters_selected_size');

//	if (size_id != script_size) {
//	 	$("#" + div).addClass('filters_selected_size');
//	} else {
//        //если клик по размеру который был ранее выбран
//		size_id = 0;
//	}

//     return false;

//    if(typeof(size_array) == 'undefined'){
//
//       // $("#" + div).addClass('filters_selected_size');
//        //alert('wht');
//    }
//    else{
//    //alert(size_array);
//    if(size_array.length > 1){
//        for(var key in size_array){
//            var val = size_array[key];
//            if(val != size_id){
//                $("#" + div).addClass('filters_selected_size');
//            }
//        }
//
//    }
//    else
//    {
//         alert("qwe");
//        if (size_id != size_array[0]) {
//            $("#" + div).addClass('filters_selected_size');
//        } else {
//            size_id = 0;
//        }
//    }
//    }

 	LoadSearchPage(script_find, script_price_min, script_price_max, size_id, script_color, script_vendor);
}

///	В фильтрах выбрали какого-то производителя
function FiltersSelectVendor(vendor_id, div) {
	$(".filters .filter_size .filter_vendors .filter_vendor_item").css('background-position', '0% 0%');
	if (vendor_id != script_vendor) {
 		$("#" + div).css('background-position', '0% 100%');
 	} else {
 		vendor_id	=	0;
 	}

 	LoadSearchPage(script_find, script_price_min, script_price_max, script_size, script_color, vendor_id);
}

///	В фильтрах выбрали цвет
function FiltersSelectColor(color_id, div) {
	$(".filters .filter_size .filter_colors .filter_color_item").removeClass('filter_selected_color');
	if (color_id != script_color) {
	 	$("#" + div).addClass('filter_selected_color');
	} else {
		color_id	=	0;
	}

 	LoadSearchPage(script_find, script_price_min, script_price_max, script_size, color_id, script_vendor);
}

///	В фильтрах выбрали цену
function FiltersSelectPrice() {
	var	price_min	=	$( "#price-range" ).slider( "values", 0 );
	var	price_max	=	$( "#price-range" ).slider( "values", 1 );


	LoadSearchPage(script_find, price_min, price_max, script_size, script_color, script_vendor);
}

///	Загрузить страницу поиска с фильтрами
function LoadSearchPage(find, min, max, size, color, vendor) {
	var w = $(window);
    //console.log(w);
	$(".add_to_basket_page_all_shadow").css("width", (w.width()-1)+"px");
	$(".add_to_basket_page_all_shadow").css("height", getDocHeight()+"px");
	$('.add_to_basket_page_all_shadow').toggle();
    
    //size_array.push(size);

   // alert(size_array[0]);

	script_find			=	find;
	script_price_min	=	min;
	script_price_max	=	max;
	script_size			=	size;
	script_color		=	color;
	script_vendor		=	vendor;

    //console.log(script_size);
	var url	=	script_url+'/min.-'+min+'/max.-'+max+'/size.-'+size+'/color.-'+color+'/vendor.-'+vendor+'/'+find;
	window.location.hash	=	url;

	url	=	script_url+'/reload.-none/min.-'+min+'/max.-'+max+'/size.-'+size+'/color.-'+color+'/vendor.-'+vendor+'/'+encodeURIComponent(find);

	$('#ajax_load_div').load(url
//        +' #ajax_products_place'
        , function(data){
		//alert(data);
		$("#ajax_load_div").html(data);
		$('.add_to_basket_page_all_shadow').toggle();
	});

}

///	Проверка на наличие хэша (если так, то переходим на эту страницу)
function CheckForHash() {
	if (window.location.hash != "")	{
		window.location	=	window.location.hash.replace("#", "");
	}
}

///	Преобразование числа в денежный формат
function Number_Format(num) {
	var decimal	=	0;

	var x 			= 	Math.pow(10,decimal);
    var m 			= 	Math.abs(num) * x;
	var n 			= 	Math.floor(Math.round(m)) / x;
    var num 		= 	n.toFixed(decimal);

	var parts		=	num.split(' ');
    var frontPart	=	parts[0];
	var length		=	frontPart.length;
	var s			=	'';
	for (i=0; i<length; i++) {
		if ( (Math.round(i/3) == (i/3)) && (i != 0) ) s = ' ' + s;
		s = frontPart.charAt((length-1)-i) + s;
	}

	//s = s + ' ' + parts[1];

    return s;
}

///	Показать краткую информацию о корзине
function ShowBasketInfo() {
	if (loading_div == '') loading_div = $('#add_to_basket .add_to_basket_page').html();

	if ($('#basket_preview_window').css('display') == 'none') {
		$('#basket_preview_window').html(loading_div);
		$('#basket_preview_window').fadeIn();
		$.get("./autocomplete.php", { what: "show_basket_preview" },
	   		function(data){
	     		$('#basket_preview_window').html(data);
	   	});
   	}
}

///	Скрыть краткую информацию о корзине
function HideBasketInfo() {
	$('#basket_preview_window').fadeOut();
	$('#basket_preview_window').html('');
}

function toggleThis(th){
   $(th).next().toggle();
}