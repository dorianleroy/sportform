(function (a, b) {
    function cA(a) {
        return f.isWindow(a) ? a : a.nodeType === 9 ? a.defaultView || a.parentWindow : !1
    }

    function cx(a) {
        if (!cm[a]) {
            var b = c.body, d = f("<" + a + ">").appendTo(b), e = d.css("display");
            d.remove();
            if (e === "none" || e === "") {
                cn || (cn = c.createElement("iframe"), cn.frameBorder = cn.width = cn.height = 0), b.appendChild(cn);
                if (!co || !cn.createElement) {
                    co = (cn.contentWindow || cn.contentDocument).document, co.write((c.compatMode === "CSS1Compat" ? "<!doctype html>" : "") + "<html><body>"), co.close()
                }
                d = co.createElement(a), co.body.appendChild(d), e = f.css(d, "display"), b.removeChild(cn)
            }
            cm[a] = e
        }
        return cm[a]
    }

    function cw(a, b) {
        var c = {};
        f.each(cs.concat.apply([], cs.slice(0, b)), function () {
            c[this] = a
        });
        return c
    }

    function cv() {
        ct = b
    }

    function cu() {
        setTimeout(cv, 0);
        return ct = f.now()
    }

    function cl() {
        try {
            return new a.ActiveXObject("Microsoft.XMLHTTP")
        } catch (b) {
        }
    }

    function ck() {
        try {
            return new a.XMLHttpRequest
        } catch (b) {
        }
    }

    function ce(a, c) {
        a.dataFilter && (c = a.dataFilter(c, a.dataType));
        var d = a.dataTypes, e = {}, g, h, i = d.length, j, k = d[0], l, m, n, o, p;
        for (g = 1; g < i; g++) {
            if (g === 1) {
                for (h in a.converters) {
                    typeof h == "string" && (e[h.toLowerCase()] = a.converters[h])
                }
            }
            l = k, k = d[g];
            if (k === "*") {
                k = l
            } else {
                if (l !== "*" && l !== k) {
                    m = l + " " + k, n = e[m] || e["* " + k];
                    if (!n) {
                        p = b;
                        for (o in e) {
                            j = o.split(" ");
                            if (j[0] === l || j[0] === "*") {
                                p = e[j[1] + " " + k];
                                if (p) {
                                    o = e[o], o === !0 ? n = p : p === !0 && (n = o);
                                    break
                                }
                            }
                        }
                    }
                    !n && !p && f.error("No conversion from " + m.replace(" ", " to ")), n !== !0 && (c = n ? n(c) : p(o(c)))
                }
            }
        }
        return c
    }

    function cd(a, c, d) {
        var e = a.contents, f = a.dataTypes, g = a.responseFields, h, i, j, k;
        for (i in g) {
            i in d && (c[g[i]] = d[i])
        }
        while (f[0] === "*") {
            f.shift(), h === b && (h = a.mimeType || c.getResponseHeader("content-type"))
        }
        if (h) {
            for (i in e) {
                if (e[i] && e[i].test(h)) {
                    f.unshift(i);
                    break
                }
            }
        }
        if (f[0] in d) {
            j = f[0]
        } else {
            for (i in d) {
                if (!f[0] || a.converters[i + " " + f[0]]) {
                    j = i;
                    break
                }
                k || (k = i)
            }
            j = j || k
        }
        if (j) {
            j !== f[0] && f.unshift(j);
            return d[j]
        }
    }

    function cc(a, b, c, d) {
        if (f.isArray(b)) {
            f.each(b, function (b, e) {
                c || bG.test(a) ? d(a, e) : cc(a + "[" + (typeof e == "object" || f.isArray(e) ? b : "") + "]", e, c, d)
            })
        } else {
            if (!c && b != null && typeof b == "object") {
                for (var e in b) {
                    cc(a + "[" + e + "]", b[e], c, d)
                }
            } else {
                d(a, b)
            }
        }
    }

    function cb(a, c) {
        var d, e, g = f.ajaxSettings.flatOptions || {};
        for (d in c) {
            c[d] !== b && ((g[d] ? a : e || (e = {}))[d] = c[d])
        }
        e && f.extend(!0, a, e)
    }

    function ca(a, c, d, e, f, g) {
        f = f || c.dataTypes[0], g = g || {}, g[f] = !0;
        var h = a[f], i = 0, j = h ? h.length : 0, k = a === bV, l;
        for (; i < j && (k || !l); i++) {
            l = h[i](c, d, e), typeof l == "string" && (!k || g[l] ? l = b : (c.dataTypes.unshift(l), l = ca(a, c, d, e, l, g)))
        }
        (k || !l) && !g["*"] && (l = ca(a, c, d, e, "*", g));
        return l
    }

    function b_(a) {
        return function (b, c) {
            typeof b != "string" && (c = b, b = "*");
            if (f.isFunction(c)) {
                var d = b.toLowerCase().split(bR), e = 0, g = d.length, h, i, j;
                for (; e < g; e++) {
                    h = d[e], j = /^\+/.test(h), j && (h = h.substr(1) || "*"), i = a[h] = a[h] || [], i[j ? "unshift" : "push"](c)
                }
            }
        }
    }

    function bE(a, b, c) {
        var d = b === "width" ? a.offsetWidth : a.offsetHeight, e = b === "width" ? bz : bA;
        if (d > 0) {
            c !== "border" && f.each(e, function () {
                c || (d -= parseFloat(f.css(a, "padding" + this)) || 0), c === "margin" ? d += parseFloat(f.css(a, c + this)) || 0 : d -= parseFloat(f.css(a, "border" + this + "Width")) || 0
            });
            return d + "px"
        }
        d = bB(a, b, b);
        if (d < 0 || d == null) {
            d = a.style[b] || 0
        }
        d = parseFloat(d) || 0, c && f.each(e, function () {
            d += parseFloat(f.css(a, "padding" + this)) || 0, c !== "padding" && (d += parseFloat(f.css(a, "border" + this + "Width")) || 0), c === "margin" && (d += parseFloat(f.css(a, c + this)) || 0)
        });
        return d + "px"
    }

    function br(a, b) {
        b.src ? f.ajax({url:b.src, async:!1, dataType:"script"}) : f.globalEval((b.text || b.textContent || b.innerHTML || "").replace(bi, "")), b.parentNode && b.parentNode.removeChild(b)
    }

    function bq(a) {
        var b = (a.nodeName || "").toLowerCase();
        b === "input" ? bp(a) : b !== "script" && typeof a.getElementsByTagName != "undefined" && f.grep(a.getElementsByTagName("input"), bp)
    }

    function bp(a) {
        if (a.type === "checkbox" || a.type === "radio") {
            a.defaultChecked = a.checked
        }
    }

    function bo(a) {
        return typeof a.getElementsByTagName != "undefined" ? a.getElementsByTagName("*") : typeof a.querySelectorAll != "undefined" ? a.querySelectorAll("*") : []
    }

    function bn(a, b) {
        var c;
        if (b.nodeType === 1) {
            b.clearAttributes && b.clearAttributes(), b.mergeAttributes && b.mergeAttributes(a), c = b.nodeName.toLowerCase();
            if (c === "object") {
                b.outerHTML = a.outerHTML
            } else {
                if (c !== "input" || a.type !== "checkbox" && a.type !== "radio") {
                    if (c === "option") {
                        b.selected = a.defaultSelected
                    } else {
                        if (c === "input" || c === "textarea") {
                            b.defaultValue = a.defaultValue
                        }
                    }
                } else {
                    a.checked && (b.defaultChecked = b.checked = a.checked), b.value !== a.value && (b.value = a.value)
                }
            }
            b.removeAttribute(f.expando)
        }
    }

    function bm(a, b) {
        if (b.nodeType === 1 && !!f.hasData(a)) {
            var c, d, e, g = f._data(a), h = f._data(b, g), i = g.events;
            if (i) {
                delete h.handle, h.events = {};
                for (c in i) {
                    for (d = 0, e = i[c].length; d < e; d++) {
                        f.event.add(b, c + (i[c][d].namespace ? "." : "") + i[c][d].namespace, i[c][d], i[c][d].data)
                    }
                }
            }
            h.data && (h.data = f.extend({}, h.data))
        }
    }

    function bl(a, b) {
        return f.nodeName(a, "table") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a
    }

    function X(a) {
        var b = Y.split(" "), c = a.createDocumentFragment();
        if (c.createElement) {
            while (b.length) {
                c.createElement(b.pop())
            }
        }
        return c
    }

    function W(a, b, c) {
        b = b || 0;
        if (f.isFunction(b)) {
            return f.grep(a, function (a, d) {
                var e = !!b.call(a, d, a);
                //r...(E);
                var H = F.data("selectBox-control"), G = H.data("selectBox-options");
                G.find(".selectBox-hover").removeClass("selectBox-hover")

            var e = function (G, F, E) {
                if (!F || F.length === 0) {
                    return
                }
                G = a(G);
                var L = G.data("selectBox-control"), I = L.data("selectBox-options"), J = L.hasClass("selectBox-dropdown") ? I : I.parent(), K = parseInt(F.offset().top - J.position().top), H = parseInt(K + F.outerHeight());
                if (E) {
                    J.scrollTop(F.offset().top - J.offset().top + J.scrollTop() - (J.height() / 2))
                } else {
                    if (K < 0) {
                        J.scrollTop(F.offset().top - J.offset().top + J.scrollTop())
                    }
                    if (H > J.height()) {
                        J.scrollTop((F.offset().top + F.outerHeight()) - J.offset().top + J.scrollTop() - J.height())
                    }
                }
            };
            var q = function (L, E) {
                L = a(L);
                var I = L.data("selectBox-control"), M = I.data("selectBox-options"), G = L.data("selectBox-settings"), H = 0, J = 0;
                if (I.hasClass("selectBox-disabled")) {
                    return
                }
                switch (E.keyCode) {
                    case 8:
                        E.preventDefault();
                        B = "";
                        break;
                    case 9:
                    case 27:
                        h();
                        C(L);
                        break;
                    case 13:
                        if (I.hasClass("selectBox-menuShowing")) {
                            y(L, M.find("LI.selectBox-hover:first"), E);
                            if (I.hasClass("selectBox-dropdown")) {
                                h()
                            }
                        } else {
                            u(L)
                        }
                        break;
                    case 38:
                    case 37:
                        E.preventDefault();
                        if (I.hasClass("selectBox-menuShowing")) {
                            var F = M.find(".selectBox-hover").prev("LI");
                            H = M.find("LI:not(.selectBox-optgroup)").length;
                            J = 0;
                            while (F.length === 0 || F.hasClass("selectBox-disabled") || F.hasClass("selectBox-optgroup")) {
                                F = F.prev("LI");
                                if (F.length === 0) {
                                    if (G.loopOptions) {
                                        F = M.find("LI:last")
                                    } else {
                                        F = M.find("LI:first")
                                    }
                                }
                                if (++J >= H) {
                                    break
                                }
                            }
                            z(L, F);
                            y(L, F, E);
                            e(L, F)
                        } else {
                            u(L)
                        }
                        break;
                    case 40:
                    case 39:
                        E.preventDefault();
                        if (I.hasClass("selectBox-menuShowing")) {
                            var K = M.find(".selectBox-hover").next("LI");
                            H = M.find("LI:not(.selectBox-optgroup)").length;
                            J = 0;
                            while (K.length === 0 || K.hasClass("selectBox-disabled") || K.hasClass("selectBox-optgroup")) {
                                K = K.next("LI");
                                if (K.length === 0) {
                                    if (G.loopOptions) {
                                        K = M.find("LI:first")
                                    } else {
                                        K = M.find("LI:last")
                                    }
                                }
                                if (++J >= H) {
                                    break
                                }
                            }
                            z(L, K);
                            y(L, K, E);
                            e(L, K)
                        } else {
                            u(L)
                        }
                        break
                }
            };
            var d = function (E, G) {
                E = a(E);
                var H = E.data("selectBox-control"), F = H.data("selectBox-options");
                if (H.hasClass("selectBox-disabled")) {
                    return
                }
                switch (G.keyCode) {
                    case 9:
                    case 27:
                    case 13:
                    case 38:
                    case 37:
                    case 40:
                    case 39:
                        break;
                    default:
                        if (!H.hasClass("selectBox-menuShowing")) {
                            u(E)
                        }
                        G.preventDefault();
                        clearTimeout(c);
                        B += String.fromCharCode(G.charCode || G.keyCode);
                        F.find("A").each(function () {
                            if (a(this).text().substr(0, B.length).toLowerCase() === B.toLowerCase()) {
                                z(E, a(this).parent());
                                e(E, a(this).parent());
                                return false
                            }
                        });
                        c = setTimeout(function () {
                            B = ""
                        }, 1000);
                        break
                }
            };
            var r = function (E) {
                E = a(E);
                E.attr("disabled", false);
                var F = E.data("selectBox-control");
                if (!F) {
                    return
                }
                F.removeClass("selectBox-disabled")
            };
            var k = function (E) {
                E = a(E);
                E.attr("disabled", true);
                var F = E.data("selectBox-control");
                if (!F) {
                    return
                }
                F.addClass("selectBox-disabled")
            };
            var f = function (E, H) {
                E = a(E);
                E.val(H);
                H = E.val();
                var I = E.data("selectBox-control");
                if (!I) {
                    return
                }
                var G = E.data("selectBox-settings"), F = I.data("selectBox-options");
                w(E);
                F.find(".selectBox-selected").removeClass("selectBox-selected");
                F.find("A").each(function () {
                    if (typeof(H) === "object") {
                        for (var J = 0; J < H.length; J++) {
                            if (a(this).attr("rel") == H[J]) {
                                a(this).parent().addClass("selectBox-selected")
                            }
                        }
                    } else {
                        if (a(this).attr("rel") == H) {
                            a(this).parent().addClass("selectBox-selected")
                        }
                    }
                });
                if (G.change) {
                    G.change.call(E)
                }
            };
            var x = function (L, M) {
                L = a(L);
                var H = L.data("selectBox-control"), F = L.data("selectBox-settings");
                switch (typeof(D)) {
                    case"string":
                        L.html(D);
                        break;
                    case"object":
                        L.html("");
                        for (var I in D) {
                            if (D[I] === null) {
                                continue
                            }
                            if (typeof(D[I]) === "object") {
                                var E = a('<optgroup label="' + I + '" />');
                                for (var G in D[I]) {
                                    E.append('<option value="' + G + '">' + D[I][G] + "</option>")
                                }
                                L.append(E)
                            } else {
                                var J = a('<option value="' + I + '">' + D[I] + "</option>");
                                L.append(J)
                            }
                        }
                        break
                }
                if (!H) {
                    return
                }
                H.data("selectBox-options").remove();
                var K = H.hasClass("selectBox-dropdown") ? "dropdown" : "inline";
                M = n(L, K);
                H.data("selectBox-options", M);
                switch (K) {
                    case"inline":
                        H.append(M);
                        break;
                    case"dropdown":
                        w(L);
                        a("BODY").append(M);
                        break
                }
            };
            var j = function (E) {
                a(E).css("MozUserSelect", "none").bind("selectstart", function (F) {
                    F.preventDefault()
                })
            };
            var g = function (G, H) {
                var E = a("<li />"), F = a("<a />");
                E.addClass(G.attr("class"));
                E.data(G.data());
                F.attr("rel", G.val()).text(G.text());
                E.append(F);
                if (G.attr("disabled")) {
                    E.addClass("selectBox-disabled")
                }
                if (G.attr("selected")) {
                    E.addClass("selectBox-selected")
                }
                H.append(E)
            };
            switch (l) {
                case"control":
                    return a(this).data("selectBox-control");
                case"settings":
                    if (!D) {
                        return a(this).data("selectBox-settings")
                    }
                    a(this).each(function () {
                        a(this).data("selectBox-settings", a.extend(true, a(this).data("selectBox-settings"), D))
                    });
                    break;
                case"options":
                    if (D === undefined) {
                        return a(this).data("selectBox-control").data("selectBox-options")
                    }
                    a(this).each(function () {
                        x(this, D)
                    });
                    break;
                case"value":
                    if (D === undefined) {
                        return a(this).val()
                    }
                    a(this).each(function () {
                        f(this, D)
                    });
                    break;
                case"refresh":
                    a(this).each(function () {
                        m(this)
                    });
                    break;
                case"enable":
                    a(this).each(function () {
                        r(this)
                    });
                    break;
                case"disable":
                    a(this).each(function () {
                        k(this)
                    });
                    break;
                case"destroy":
                    a(this).each(function () {
                        A(this)
                    });
                    break;
                default:
                    a(this).each(function () {
                        v(this, l)
                    });
                    break
            }
            return a(this)
        }
    }

    )
})(jQuery)
};
