<?
#################################################################
$from = "p";
require ("libs/fo_prepare.php");

$session_name = session_name();
$session_id = session_id();

$what = "general";

$z	=	explode("/", $data->GET["url"]);
//print_r($z);
//die();
for ($i=0; $i<count($z); $i++) {
	$k	=	explode(".-", $z[$i]);
	//print_r($k);
	$data->GET[$k[0]]	=	$k[1];
}

$pid	=	$cp->GetProductIDByURL($z[(count($z)-1)]);


if (!$pid) {
	header("Location: /");
	die();
}

$product					=	$cp->GetProductByID($pid);




############
###########   для просмотра раннее просмотренных товаров, сессия записывается в Cookies в fo_prepare.php
###########
$_SESSION['early_see'][$pid] = $product;

###########
#########  //  для просмотра раннее просмотренных товаров
###########

if (!$data->GET["color"]) 	{
	$colors		=	$cp->GetColorsWare($pid);
    $adtnl_colors = $cp->GetAdtnlColors($pid);
//    $col_from_ware  = $cp->GetColorsWare($pid);
//    $colors = $col_from_ware; ######## - заплатка. Временно
	$data->GET["color"]		=	$colors[0];
}




if ($product[page_descr]) 		$MESSAGES["site_description"]	=	$product[page_descr];
if ($product[page_keywords]) 	$MESSAGES["site_keywords"]	=	$product[page_keywords];
$MESSAGES["site_title"]		=	htmlspecialchars($product[name])." - ".number_format($product[price], 0, ',', ' ')." ".$MESSAGES["money_rur"];

//var_dump($product[cat_id]);

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	выбор шаблона
////////////////////////////////////////////////////////////////////////////////////////////////
if (file_exists("templates/".$_curSchema."/product_".$product[cat_id].".tpl.htm")) {
	$template_file = "product_".$product[cat_id].".tpl.htm";
} else {
	$template_file = "product.tpl.htm";
}

$t->set_file(array(
    "index"		=>	$template_file
));
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	//	выбор шаблона
////////////////////////////////////////////////////////////////////////////////////////////////


if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	новый отызв о товаре
////////////////////////////////////////////////////////////////////////////////////////////////
if ($what	==	"send_review") {
 	$cp->AddProductReview($pid, $data->POST["rating"], $user_id, $data->POST["name"], $data->POST["email"], $data->POST["text"]);
	die();
}
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	//	новый отызв о товаре
////////////////////////////////////////////////////////////////////////////////////////////////

$actions = array("general", "product_images", "add_product_images", "sizes_for_selected_color");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	страница с описанием товара
////////////////////////////////////////////////////////////////////////////////////////////////
if ($what == "general") {
$blocks->ShowHeaderBasketInfo();		//	отображаем информацию о корзине в шапке сайта
$blocks->MainCatsDrop();				//	Выпадающее окно со списком главных разделов

$sql = new SQLClass();
$sql->query("UPDATE ".$tableCollab["products"]." SET views=views+1 WHERE id='".$pid."'");
$sql->close();


$_descr	=	htmlspecialchars_decode($product[descr]);
$_full_descr	=	htmlspecialchars_decode($product[full_descr]);
$_full_descr_parts	=	explode("<div><hr></div>", $_full_descr);

if (!$product[vendor_name_country]) $blocks->HideBlock("index", "vendor-country");
if ($product[price2] == 0) $blocks->HideBlock("index", "price-2");

if (!$product[rating_count]) $product[rating_count]	=	0.0001;

$product_rating	=	round(($product[rating_sum] / $product[rating_count]) * 20);

if ($product['old_price'] == 0.00)
     $style_o = "nonOldPrice";
else
     $style_o = "oldPrice";

$t->set_var(array(
	"PRODUCT_ID"					=>	$product[id],
	"PRODUCT_URL"					=>	$product[url],
	"PRODUCT_NAME"					=>	$product[name],
	"PRODUCT_CODE"					=>	$product[code],
	"PRODUCT_VENDOR_NAME"			=>	$product[vendor_name],
	"PRODUCT_VENDOR_COUNTRY"		=>	$product[vendor_name_country],
	"PRODUCT_PRICE"					=>	number_format($product[price], 0, ',', ' '),
	"PRODUCT_FORUNIT"				=>	$product[for_unit],
	"PRODUCT_PRICE2"				=>	number_format($product[price2], 0, ',', ' '),
	"PRODUCT_FORUNIT2"				=>	$product[for_unit2],
	"PRODUCT_WAREHOUSE"				=>	$product[warehouse],
	"PRODUCT_UNIT"					=>	$cp->GetUnitByID($product[units]),
	"PRODUCT_SHORT_DESCRIPTION"		=>	$_descr,
	"PRODUCT_FULL_DESCRIPTION"		=>	$_full_descr,
	"PRODUCT_FULL_DESCRIPTION_PART1"=>	$_full_descr_parts[0],
	"PRODUCT_FULL_DESCRIPTION_PART2"=>	$_full_descr_parts[1],
	"PRODUCT_RATING"				=>	$product_rating,
	"PRODUCT_COLOR"					=>	$data->GET["color"],

    "PRODUCT_OLD_PRICE"             =>  number_format($product[old_price], 0, ',', ' '),
    "PRICE_OLD_STYLE"               =>  $style_o
));

//	######################################	ДОПОЛНИТЕЛЬНЫЕ ФОТОГРАФИИ	################################## //
$imgs	=	$cp->GetImagesForPID($_default_images_dir_products, $product[id], $data->GET["color"]);
if (!count($imgs)) {
	$blocks->HideBlock("index", "add_photo");
} else {
	$t->set_block("index", "add_photo", "_add_photo");
	for ($i=0; $i<count($imgs); $i++) {

		$mask	=	str_replace(".".$_default_images_dir_products."/".$product[id]."_", "", $imgs[$i]);
		$mask	=	str_replace("-".$data->GET["color"]."_.jpg", "", $mask);

		$t->set_var(array(
			"IMAGE_I"					=>	($i+2),
			"IMAGE_ID"					=>	$mask,
            "COLOR_ID_M"                => 'AS'
		));
		$t->parse("_add_photo", "add_photo", true);
	}
}
//	######################################	//	ДОПОЛНИТЕЛЬНЫЕ ФОТОГРАФИИ	################################## //

###############################################	Свойства
$properties	=	$cp->GetPropertiesValues($product[id]);
$t->set_block("index", "propertys", "_propertys");
if (!count($properties)) {
	$blocks->HideBlock("index", "is_propertys");
} else {
	for ($i=0; $i<count($properties); $i++) {
		if (($i/2) == round($i/2)) $propertie_background = "#ffffff"; else $propertie_background = "#f5f6ef";
		$t->set_var(array(
			"PROPERTY_BACKGROUND"	=>	$propertie_background,
			"PROPERTY_NAME"			=>	$properties[$i][property_name],
			"PROPERTY_VALUE"		=>	$properties[$i][value]
		));
		$t->parse("_propertys", "propertys", true);
	}
}
unset($properties);

############################################### //	Свойства
//$t->set_block("index", "is_tags", "_is_tags");
###############################################	МЕТКИ (TAGS)
if(!$product[page_keywords]) $product[page_keywords] = "Sportform.ru";
if (!$product[page_keywords]) {
	$blocks->HideBlock("index", "is_tags");
} else {
	$t->set_block("index", "tags", "_tags");
	$tags	=	explode(",", $product[page_keywords]);
	for ($i=0; $i<count($tags); $i++) {
		$tag_name	=	trim($tags[$i]);
		$tag_url	=	str_replace(" ", "%20", $tag_name);
		if ($i < (count($tags)-1)) $tag_point	=	","; else $tag_point	=	"";
		$t->set_var(array(
			"P_TAG"			=>	$tag_name,
			"P_TAG_URL"		=>	$tag_url,
			"P_TAG_POINT"	=>	$tag_point
		));
		$t->parse("_tags", "tags", true);
	}
}
###############################################	//	МЕТКИ (TAGS)

###############################################	Цвета товара
$t->set_block("index", "available-colors", "_available-colors");
$colors		    = $cp->GetProductColorList($pid);
$col_from_ware  = $cp->GetColorsWare($pid);
//var_dump($data->GET["color"]);
$colors=$col_from_ware;//****** - заплатка
    $t->set_var(array(
            "COLOR_THIS"    => $colors[0]
        ));

for ($i=0; $i<count($colors); $i++) {

	$color		=	$cp->GetProductColor($colors[$i]);

//    echo "<pre>", var_dump($color), "</pre>";
	$t->set_var(array(
		"COLOR_ID"		=>	$color["id"],
		"COLOR_NAME"	=>	$color["name"],
		"COLOR_COLOR"	=>	$color["color"]
	));
	$t->parse("_available-colors", "available-colors", true);
}
###############################################	//	Цвета товара

###############################################	Дополнительные цвета товара
$t->set_block("index", "available-colors_ad", "_available-colors_ad");
$colors_ad  	    = $cp->GetProductColorList($pid);
$col_from_ware_ad   = $cp->GetAdtnlColors($pid);
//var_dump($data->GET["color"]);
$colors_ad=$col_from_ware_ad;//****** - заплатка

if (count($colors_ad)<1){
   $t->set_block("index","av-col-ad","_av-col-ad");
   $t->set_var("av-col-ad","");
   $t->parse("_av-col-ad","av-col-ad");
}

for ($i=0; $i<count($colors_ad); $i++) {
	$color_ad		=	$cp->GetProductColor($colors_ad[$i]);
	$t->set_var(array(
		"COLOR_ID_AD"		=>	$color_ad["id"],
		"COLOR_NAME_AD"	=>	$color_ad["name"],
		"COLOR_COLOR_AD"	=>	$color_ad["color"]
	));
	$t->parse("_available-colors_ad", "available-colors_ad", true);
}
###############################################	//	Дополнительные цвета товара

###############################################	Размеры
$t->set_block("index", "av_sizes", "_av_sizes");

##
    ## OLD CODE
##

$sizes	=	$cp->GetProductWarehouseSizes_2($pid, $data->GET["color"]);
//var_dump($sizes); echo "<pre></pre>";
		if (!count($sizes)) {
			$hide_buy_button	=	"display: none;";
			$t->set_var("_av_sizes", "нет в наличии");
		} else {
			$hide_buy_button	=	"";
			for ($is=0; $is<count($sizes); $is++) {
				if ($is < (count($sizes)-1)) $av_sizes_point = " - "; else $av_sizes_point = "";

				$t->set_var(array(
					"AV_SIZE_NAME"		=>	$sizes[$is]["name"],
					"AV_SIZE_POINT"		=>	$av_sizes_point
				));
   				$t->parse("_av_sizes", "av_sizes", true);
		 	}
		}

// //OLD CODE


$t->set_block("index", "available-sizes", "_available-sizes");

$sizes	=	$cp->GetSizesArray($product["id"], $data->GET['color'],$product['size_chart']);

for ($i=0; $i<count($sizes); $i++) {
	$t->set_var(array(
		"SIZE_ID"		=>	$sizes[$i]["id"],
		"SIZE_NAME"		=>	$sizes[$i]["name"]
	));
	$t->parse("_available-sizes", "available-sizes", true);
}
###############################################	// Размеры

###############################################	Сопутствующие товары
$p2p	=	$cp->GetP2PID($product[id]);

if (!count($p2p)) {
	$blocks->HideBlock("index", "is_parts");
} else {
	shuffle($p2p);
	$t->set_block("index", "recomended-products", "_recomended-products");
	for ($i=0; $i<count($p2p); $i++) {
		$p2product					=	$cp->GetProductByID($p2p[$i]);

        if (!$p2product[rating_count]) $p2product[rating_count]	=	0.0001;
        		$p2product_rating	=	round(($p2product[rating_sum] / $p2product[rating_count]) * 20);


		$t->set_var(array(
			"REC_ID"					=>	$p2product[id],
			"REC_URL"					=>	$p2product[url],
			"REC_NAME"					=>	$p2product[name],
			"REC_CODE"					=>	$p2product[code],
			"REC_VENDOR_NAME"			=>	$p2product[vendor_name],
			"REC_VENDOR_COUNTRY"		=>	$p2product[vendor_name_country],
			"REC_PRICE"					=>	number_format($p2product[price], 0, ',', ' '),
			"REC_FORUNIT"				=>	$p2product[for_unit],
			"REC_RATING"				=>	$p2product_rating
		));
		$t->parse("_recomended-products", "recomended-products", true);
	}
}
###############################################	//	Сопутствующие товары


////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	рисуем категории
////////////////////////////////////////////////////////////////////////////////////////////////
if (!$data->GET["c"]) 	{
	$in_cats	=	$cp->GetCatsOfProduct($pid);
	$cid	=	$in_cats[0];
} else {
	$cid	=	$cp->GetCategoryIDByURL($data->GET["c"]);
}
$cid_is_parent	=	$cp->IsThisParentCat($cid);
$path = $cp->GetPathFromCID($cid);


if (!$cid_is_parent) {
	if (count($path) < 5) $cat_level0	=	count($path)-1; else $cat_level0	=	3;
} else {
	if (count($path) < 5) $cat_level0	=	count($path)-1; else $cat_level0	=	2;
}

$t->set_var(array(
	"CAT_LEVEL0_NAME"		=>	$path[$cat_level0]["short_name"],
	"CAT_LEVEL0_URL"		=>	$path[$cat_level0]["url"],
	"CAT_LEVEL1_NAME"		=>	$path[($cat_level0-1)]["short_name"],
	"CAT_LEVEL1_URL"		=>	$path[($cat_level0-1)]["url"],
));

$t->set_block("index", "cat_level2", "_cat_level2");
$t->set_block("cat_level2", "cat_level3", "_cat_level3");
$cat_level2	=	$cp->GetSimpleCatsList($path[($cat_level0-1)]["id"]);
if (!count($cat_level2)) $t->set_var("_cat_level2", "");
for ($i=0; $i<count($cat_level2); $i++) {
	if ($cat_level2[$i]["id"] == $path[($cat_level0-2)]["id"]) $cat_level2_open = "open"; else $cat_level2_open = "";

	$t->set_var(array(
		"CAT_LEVEL2_URL"		=>	$cat_level2[$i]["url"],
		"CAT_LEVEL2_NAME"		=>	$cat_level2[$i]["short_name"],
		"CAT_LEVEL2_OPEN"		=>	$cat_level2_open
	));

	if ($cat_level2[$i]["id"] == $path[($cat_level0-2)]["id"]) {
		$cat_level3	=	$cp->GetSimpleCatsList($path[($cat_level0-2)]["id"]);
		if (!count($cat_level3)) $t->set_var("_cat_level3", "");
		for ($i3=0; $i3<count($cat_level3); $i3++) {
			if ($cat_level3[$i3]["id"] == $path[($cat_level0-3)]["id"]) $cat_level3_open = "open"; else $cat_level3_open = "";
			$t->set_var(array(
				"CAT_LEVEL3_URL"		=>	$cat_level3[$i3]["url"],
				"CAT_LEVEL3_NAME"		=>	$cat_level3[$i3]["short_name"],
				"CAT_LEVEL3_OPEN"		=>	$cat_level3_open
			));
			$t->parse("_cat_level3", "cat_level3", true);
		}
	}

	$t->parse("_cat_level2", "cat_level2", true);
	$t->set_var("_cat_level3", "");
}

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	//	рисуем категории
////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////       captcha
////////////////////////////////////////////////////////////////////////////////////////////////

    $t->set_var(array("SES_NAME" => $session_name,
                      "SES_ID" => $session_id));


//if($_REQUEST[session_name()]){
//	$_SESSION['captcha_keystring'] = $captcha->getKeyString();
//}
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	//	captcha
////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	рисует отзывы о товаре
////////////////////////////////////////////////////////////////////////////////////////////////

$reviews	=	$cp->GetProductReviews($pid);
if (!count($reviews)) {
	$blocks->HideBlock("index", "have_reviews");
} else {
	$blocks->HideBlock("index", "no_reviews");
	$t->set_block("index", "review-list", "_review-list");
	for ($i=0; $i<count($reviews); $i++) {
		$t->set_var(array(
			"REVIEW_DATE"		=>	date("d-m-Y", $reviews[$i]["time"]),
			"REVIEW_RATING"		=>	($reviews[$i]["rating"] * 20),
			"REVIEW_NAME"		=>	$reviews[$i]["name"],
			"REVIEW_TEXT"		=>	nl2br($reviews[$i]["text"])
		));
		$t->parse("_review-list", "review-list", true);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	//	рисует отзывы о товаре
////////////////////////////////////////////////////////////////////////////////////////////////



$t->set_block("index", "index_general", "_index_general");
$t->parse("_index_general", "index_general", true);

}
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	//	страница с описанием товара
////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	фотографии товара в зависимости от выбранного цвета
////////////////////////////////////////////////////////////////////////////////////////////////
if ($what == "product_images") {

	$t->set_var(array(
		"PRODUCT_ID"					=>	$product[id],
		"PRODUCT_URL"					=>	$product[url],
		"PRODUCT_NAME"					=>	$product[name],
		"COLOR_ID"						=>	$data->GET['color']
	));
}
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	//	фотографии товара в зависимости от выбранного цвета
////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	ДОПОЛНИТЕЛЬНЫЕ фотографии товара в зависимости от выбранного цвета
////////////////////////////////////////////////////////////////////////////////////////////////
if ($what == "add_product_images") {
	$t->set_var(array(
		"PRODUCT_ID"					=>	$product[id],
		"PRODUCT_URL"					=>	$product[url],
		"PRODUCT_NAME"					=>	$product[name],
		"COLOR_ID"						=>	$data->GET["color"]
	));

	//	######################################	ДОПОЛНИТЕЛЬНЫЕ ФОТОГРАФИИ	################################## //
	$imgs	=	$cp->GetImagesForPID($_default_images_dir_products, $product[id], $data->GET["color"]);
//    echo "<pre>",var_dump($imgs),"</pre>";
	if (!count($imgs)) {
		$blocks->HideBlock("index", "add_photo");
	} else {
		$t->set_block("index", "add_photo", "_add_photo");
		for ($i=0; $i<count($imgs); $i++) {

			$mask	=	str_replace(".".$_default_images_dir_products."/".$product[id]."_", "", $imgs[$i]);
			$mask	=	str_replace("-".$data->GET["color"]."_.jpg", "", $mask);

			$t->set_var(array(
				"IMAGE_I"					=>	($i+2),
				"IMAGE_ID"					=>	$mask
			));
			$t->parse("_add_photo", "add_photo", true);
		}
	}

	//	######################################	//	ДОПОЛНИТЕЛЬНЫЕ ФОТОГРАФИИ	################################## //
}
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	//	ДОПОЛНИТЕЛЬНЫЕ фотографии товара в зависимости от выбранного цвета
////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	Размеры по выбранному цвету
////////////////////////////////////////////////////////////////////////////////////////////////
if ($what == "sizes_for_selected_color") {
$t->set_block("index", "available-sizes", "_available-sizes");
$sizes	=	$cp->GetProductWarehouseSizes($product["id"], $data->GET["color"]);
for ($i=0; $i<count($sizes); $i++) {
	$t->set_var(array(
		"SIZE_ID"		=>	$sizes[$i]["id"],
		"SIZE_NAME"		=>	$sizes[$i]["name"]
	));
	$t->parse("_available-sizes", "available-sizes", true);
}
}
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////	//	Размеры по выбранному цвету
////////////////////////////////////////////////////////////////////////////////////////////////


$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
