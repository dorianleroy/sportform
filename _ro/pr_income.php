<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();
$cp	=	new CATS_AND_PRODUCTSClass();

$t->set_file(array(
	    "index"		=>	"pr_income.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];


$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


########################################### ФОРМА С ВХОДОМ В СИСТЕМУ
if ($what == "general") {	$t->set_block("index", "income", "_income");
	$sql = new SQLClass();
	$a = $sql->query("SELECT pid, time FROM fo_products_income WHERE enabled='1' ORDER BY time DESC");
	for ($i=0; $i<mysql_num_rows($a); $i++) {
		$sql->fetch();
		$z = $sql->Record;

		$pr = $cp->GetProductByID($z['pid']);

		if (($i/2) == round($i/2)) $bg_color = "#f5f5f5"; else $bg_color = "#eaeaea";
		$t->set_var(array(
			"BGCOLOR"			=>	$bg_color,
			"INCOME_DATE"		=>	date("d-m-Y H:i", $z['time']),
			"INCOME_NAME"		=>	$pr['name'],
			"INCOME_WAREHOUSE"	=>	$pr['warehouse'],
			"INCOME_PRICE"		=>	number_format($pr['price'], 2)
		));
		$t->parse("_income", "income", true);
	}
	$sql->close();
}





$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
