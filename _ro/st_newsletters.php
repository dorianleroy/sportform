<?
$_subscribes	=	427;
$_dir			=	"../newsletters/logs/";
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"st_newsletters.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];


$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


########################################### ФОРМА С ВХОДОМ В СИСТЕМУ
if ($what == "general") {	$t->set_var(array(
		"RECEIVERS"		=>	$_subscribes
	));
	$t->set_block("index", "newsletters", "_newsletters");
	$files	=	array();
	$d = dir($_dir);
	while (false !== ($entry = $d->read())) {
	   if (strpos($entry, "-")) {
	   	$files[count($files)]	=	$entry;
	   }
	}
	$d->close();

	rsort($files);
	$_i	=	0;
	foreach ($files as $key => $val) {
		$uniq	=	array();
		$nl_users	=	array();
		$f	=	file($_dir . $val);
		for ($i=0; $i<count($f); $i++) {
			$z	=	explode("\t", $f[$i]);
			if (!in_array($z[2], $uniq)) $uniq[count($uniq)]	=	$z[2];

			if (count($z)	>	6) {				if (!in_array($z[4], $nl_users)) $nl_users[count($nl_users)]	=	$z[4];			}
		}

		if (($_i/2) == round($_i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";

		$t->set_var(array(
				"I"					=>	$i,
				"BGCOLOR"			=>	$bg_color,
				"NL_NAME"			=>	$val,
				"NL_REED"			=>	count($uniq),
				"NL_PROCENT"		=>	round((count($uniq)*100/$_subscribes), 2),
				"NL_USERS"			=>	count($nl_users),
				"NL_WIDTH"			=>	round((count($uniq)*100/$_subscribes) * 2)
		));

		$t->parse("_newsletters", "newsletters", true);

		unset($f);
		unset($uniq);
		$_i++;
	}
}





$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
