<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();
$customer = new CustomersClass();

$t->set_file(array(
	    "index"		=>	"cl_users.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

if ($what == "save") {
    $customer->UpdateCustomerInfo($data->POST["uid"],
    								$data->POST["login"],
    								$data->POST["password"],
    								$data->POST["company"],
    								$data->POST["city"],
    								$data->POST["address"],
    								$data->POST["post_address"],
    								$data->POST["phone"],
    								$data->POST["fax"],
    								$data->POST["email"],
    								$data->POST["icq"],
    								$data->POST["skype"],
    								$data->POST["site"],
    								$data->POST["user_"],
    								$data->POST["user_boss"],
    								$data->POST["user_fin"],
    								$data->POST["status"],
    								$data->POST["newsletter"],
    								$data->POST["whois"],
    								$data->POST["cash_payment"],
    								$data->POST["organization"]);
	header("Location: ".$data->POST["return"]);
	die();
}

if ($what == "del") {
	$customer->DelCustomerByID($data->GET["uid"]);
	header("Location: ".$data->GET["return"]);
	die();
}


$actions = array("general", "edit");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


######################################################################################
if ($what == "general") {
	$t->set_var(array(
		"USERS_AUTH"		=>	$customer->GetCustomersCount(1),
		"USERS_NONAUTH"		=>	$customer->GetCustomersCount(0)
	));

	$users_list	=	$customer->GetCustomers();
	$t->set_block("index", "users_list", "_users_list");
	for ($i=0; $i<count($users_list); $i++) {
		if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";
		if ($users_list[$i][lastlogin]) $last_login	=	date("d-m-Y H:i", $users_list[$i][lastlogin]); else $last_login = "";
		if ($users_list[$i][status]) $status_color = "#047b2e"; else $status_color = "#a40a0a";
		$t->set_var(array(
			"BGCOLOR"			=>	$bg_color,
			"USER_ID"			=>	$users_list[$i][id],
			"USER_LOGIN"		=>	$users_list[$i][login],
			"USER_COMPANY"		=>	$users_list[$i][company],
			"USER_CITY"			=>	$users_list[$i][city],
			"USER_USER"			=>	($users_list[$i]['metro']         ? "".$users_list[$i]['metro'] : " ").
                                    ($users_list[$i]['street']        ? ", ".$users_list[$i]['street'].", " : " ").
                                    ($users_list[$i]['house']         ? $users_list[$i]['house']."" : " ").
                                    ($users_list[$i]['building']      ? "/".$users_list[$i]['building'] : " ").
                                    ($users_list[$i]['section']       ? " - ".$users_list[$i]['section'] : " ").
                                    ($users_list[$i]['entrance']      ? ", п".$users_list[$i]['entrance'] : " ").
                                    ($users_list[$i]['level']         ? ", э-".$users_list[$i]['level'] : " ").
                                    ($users_list[$i]['apartment']     ? ", кв-".$users_list[$i]['apartment'] : " ").
                                    ($users_list[$i]['code']          ? ", код-".$users_list[$i]['code'] : " "),
			"USER_PHONE"		=>	$users_list[$i][phone],
			"USER_EMAIL"		=>	$users_list[$i][email],
//			"USER_TIME"			=>	date("d-m-Y H:i", $users_list[$i][time]),
			"USER_TIME"			=>	$users_list[$i][time],
			"USER_LAST_LOGIN"	=>	$users_list[$i][lastlogin],
			"USER_STATUS_COLOR"	=>	$status_color
		));
		$t->parse("_users_list", "users_list", true);
	}

}


######################################################################################
if ($what == "edit") {
		$user_info		=		$customer->GetCustomerByID($data->GET["uid"]);
		if ($data->GET["return"]) $return_url = $data->GET["return"]; else $return_url = $links[cl_users];
		if ($user_info[status]) $status = "checked"; else $status = "";
		if ($user_info[newsletter]) $newsletter = "checked"; else $newsletter = "";
		if ($user_info[cash_payment]) $cash_payment = "checked"; else $cash_payment = "";
		$user_info[company] = eregi_replace("\"([^\"]*)\"" , "«\\1»", $user_info[company]);
		$user_info[company] = eregi_replace("\&quot;([^\"]*)\&quot;" , "«\\1»", $user_info[company]);
		$user_info[company] = stripcslashes($user_info[company]);
		$user_info[company] = stripcslashes($user_info[company]);
		$t->set_var(array(
			"USER_ID"				=>	$user_info[id],
			"USER_LOGIN"			=>	$user_info[login],
			"USER_PASSWORD"			=>	$user_info[password],
			"USER_COMPANY"			=>	$user_info[company],
			"USER_CITY"				=>	$user_info[city],
			"USER_ADDRESS"			=>	$user_info['city'].", ".
                                        ($user_info['metro']      ? "метро - ".$user_info['metro'] : " ").
                                        ($user_info['street']        ? ", ".$user_info['street'].", " : " ").
                                        ($user_info['house']         ? $user_info['house']."" : " ").
                                        ($user_info['building']      ? "/".$user_info['building'] : " ").
                                        ($user_info['section']       ? " - ".$user_info['section'] : " ").
                                        ($user_info['entrance']      ? ", подъезд - ".$user_info['entrance'] : " ").
                                        ($user_info['level']      ? ", этаж - ".$user_info['level'] : " ").
                                        ($user_info['apartment']      ? ", кв - ".$user_info['apartment'] : " ").
                                        ($user_info['code']      ? ", код - ".$user_info['code'] : " ")
                                        ,
			"USER_POST_ADDRESS"		=>	$user_info[post_address],
			"USER_PHONE"			=>	$user_info[phone],
			"USER_FAX"				=>	$user_info[fax],
			"USER_EMAIL"			=>	$user_info[email],
			"USER_ICQ"				=>	$user_info[icq],
			"USER_SKYPE"			=>	$user_info[skype],
			"USER_SITE"				=>	$user_info[site],
			"USER_USER"				=>	$user_info[user_],
			"USER_USER_FIN"			=>	$user_info[user_fin],
			"USER_USER_BOSS"		=>	$user_info[user_boss],
			"USER_WHOIS"			=>	$user_info[whois],
			"USER_ORGANIZATION"		=>	$user_info[organization],
			"USER_CASHPAYMENT"		=>	$cash_payment,
			"USER_NEWSLETTER"		=>	$newsletter,
			"USER_STATUS"			=>	$status,
			"USER_REFERER"			=>	$user_info[referer],
			"RETURN_URL"			=>	$return_url
		));
}




$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
