<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"bo_sections.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

if (strlen($data->POST["pid"])) $pid = $data->POST["pid"];
if (strlen($data->GET["pid"])) $pid = $data->GET["pid"];

if (!$pid) $pid = 0;

######################################################################	��������� �������
if ($what == "save_sections") {
	$ids = $data->POST["ids"];
	$names = $data->POST["names"];
	$urls = $data->POST["urls"];
	$enabled = $data->POST["enabled"];
	
	for ($i=0; $i<count($ids); $i++) {
		$users->UpdateSection($ids[$i], $names[$i], $urls[$i], $enabled[$i]);
	}
	
	header("Location: ".$links['bo_sections']."?pid=".$pid);
	die();
}

######################################################################	��������� ������
if ($what == "add_section") {
	
	$users->AddSection($pid, $data->POST["new_name"], $data->POST["new_url"], $data->POST["new_enabled"]);
	
	header("Location: ".$links['bo_sections']."?pid=".$pid);
	die();
}

$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


########################################### 
if ($what == "general") {

	$t->set_var("P_ID", $pid);
	
	if ($pid) $blocks->HideBlock("index", "drop_down");
	$sections = $users->GetSectionsByPID($pid);
	$t->set_block("index", "sections_list", "_sections_list");
	for ($i=0; $i<count($sections); $i++) {
		if ($sections[$i][enabled]) $tmp_checked = "checked"; else $tmp_checked = "";
		
		$t->set_var(array(
			"SECTION_ID"		=>	$sections[$i][id],
			"SECTION_NAME"		=>	$sections[$i][name],
			"SECTION_URL"		=>	$sections[$i][url],
			"SECTION_I"			=>	$i,
			"SECTION_CHECKED"	=>	$tmp_checked,
		));
		$t->parse("_sections_list", "sections_list", true);
	}
}





$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
