<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$customer 	= new CustomersClass();
$cp			=	new CATS_AND_PRODUCTSClass();

$t->set_file(array(
	    "index"		=>	"or_basket.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];


$actions = array("general", "info");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


########################################### ФОРМА С ВХОДОМ В СИСТЕМУ
if ($what == "general") {

	$t->set_block("index", "basket", "_basket");

	$sql = new SQLClass();
 	$res = $sql->query("SELECT DISTINCT(user_id) FROM fo_basket ORDER BY time DESC, id DESC");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;

		$ci		=	$customer->GetCustomerByID($z[0]);
		if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";

		$time	=	0;
		$sum	=	0;

		$sql0 = new SQLClass();
	 	$res0 = $sql0->query("SELECT product_id, count, time FROM fo_basket WHERE user_id='".$z[0]."'");
	 	$pr_count	=	mysql_num_rows($res0);
		for ($i0=0; $i0<mysql_num_rows($res0); $i0++) {
			$sql0->fetch();
			$z0 = $sql0->Record;
			if ($time < $z0['time']) $time	=	$z0['time'];

			$pr = $cp->GetProductByID($z0['product_id']);

			$sum	+=	($z0['count'] * $pr['price']);
		}
		$sql0->close();


		$t->set_var(array(
			"BGCOLOR"		=>	$bg_color,
			"BS_USER_ID"	=>	$ci[id],
			"BS_USER"		=>	$ci[company]." (".$ci[city].") - ".$ci[login],
			"BS_COUNT"		=>	$pr_count,
			"BS_SUM"		=>	number_format($sum, 2, ",", " "),
			"BS_TIME"		=>	date("d-m-Y H:i", $time)
		));
		$t->parse("_basket", "basket", true);
	}
	$sql->close();
}


########################################### ФОРМА С ВХОДОМ В СИСТЕМУ
if ($what == "info") {
	$ci		=	$customer->GetCustomerByID($data->GET["uid"]);
	$t->set_var(array(
		"USER_COMPANY"		=>	$ci[company],
		"USER_CITY"			=>	$ci[city]
	));

	$t->set_block("index", "basket", "_basket");

	$sql0 = new SQLClass();
 	$res0 = $sql0->query("SELECT product_id, count, time FROM fo_basket WHERE user_id='".$ci['id']."' ORDER BY time DESC");
	for ($i0=0; $i0<mysql_num_rows($res0); $i0++) {
		$sql0->fetch();
		$z0 = $sql0->Record;
		if (($i0/2) == round($i0/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";

		$pr = $cp->GetProductByID($z0['product_id']);

		$t->set_var(array(
			"BGCOLOR"		=>	$bg_color,
			"PR_NAME"		=>	$pr['name'],
			"PR_WAREHOUSE"	=>	$pr['warehouse'],
			"PR_COUNT"		=>	$z0['count'],
			"PR_PRICE"		=>	number_format($pr['price'], 2, ",", " "),
			"PR_TIME"		=>	date("d-m-Y H:i", $z0['time'])
		));
		$t->parse("_basket", "basket", true);
	}
	$sql0->close();
}


$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
