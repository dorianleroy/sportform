<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

function translitIt($str)
{
    $tr = array(
        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "_", "."=> "", "/"=> "_", "," => "", "ё"=>"e",
        ";"=>":", "\""=>"", "'"=>"", "№"=>"N", "«"=>"", "»"=>"",
        "+"=>"-", "&"=>"", "("=>"", ")"=>"", "«"=>"", "»"=>"", "$"=>""
    );
    return strtr($str,$tr);
}

$t->set_file(array(
	    "index"		=>	"ns_news.tpl.htm"
	    ));

$what = "general";

$news = new NewsClass();

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

################################################################################
if ($what == "add") {
	$news_id	=	$news->AddNews($data->POST["new_name"], $data->POST["new_enabled"]);
	header("Location: ".$links['ns_news']."?what=edit&page=&id=".$news_id);
	die();
}

################################################################################
if ($what == "save") {
	$names = $data->POST["names"];
	$enabled = $data->POST["enabled"];
	$ids = $data->POST["ids"];
	for ($i=0; $i<count($ids); $i++) {
		$news->UpdateNews($ids[$i], $names[$i], $enabled[$i]);
	}
	header("Location: ".$links['ns_news']."?page=".$data->POST["page"]);
	die();
}

################################################################################
if ($what == "save_edit") {
	$enabled = $data->POST["enabled"];
	$id = $data->POST["id"];
	$cat_id = $data->POST["cat"];
	$url = $data->POST["url"];
	$forum_id = $data->POST["forum"];
	$name = trim($data->POST["name"]);
	$short = trim($data->POST["short"]);
	$full = trim($data->POST["full"]);
	################################################
	$new = $news->GetNewsByID($data->GET["id"]);
	$day = trim($data->POST["day"]);
	$month = trim($data->POST["month"]);
	$year = trim($data->POST["year"]);
	$time = mktime(date("H", $new[time]), date("i", $new[time]), date("s", $new[time]), $month, $day, $year);
	unset($new);
	################################################
	$news->SaveNews($id, $cat_id, $name, $time, $short, html_entity_decode($full), $forum_id, $enabled, $url);
	header("Location: ".$links['ns_news']."?page=".$data->POST["page"]);
	die();
}

################################################################################
if ($what == "add_img") {
	if ($_FILES['new_img'][size]) {
		$news->AddImage($data->POST["id"], $_default_images_dir.$_default_images_dir_news, $_FILES['new_img'], "");
	}
	header("Location: ".$links['ns_news']."?what=edit&page=".$data->POST["page"]."&id=".$data->POST["id"]);
	die();
}

################################################################################
if ($what == "del_img") {
	$news->DelImage($data->GET["img"], $_default_images_dir.$_default_images_dir_news);
	header("Location: ".$links['ns_news']."?what=edit&page=".$data->GET["page"]."&id=".$data->GET["id"]);
	die();
}



$actions = array("general", "edit");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");

########################################### ФОРМА С ВХОДОМ В СИСТЕМУ
if ($what == "general") {

	$t->set_var(array(
		"PAGE"		=>	$data->GET["page"]
	));

	$list = $news->NewsList();
	$t->set_block("index", "list", "_list");
	if (!count($list)) $t->set_var("_list", "");
	$from 	= ($data->GET["page"] * $_default_news_per_page);
	$to		= $from + $_default_news_per_page;
	if ($to > count($list)) $to = count($list);
	$ii = 0;
	for ($i=$from; $i<$to; $i++) {		if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";
		if ($list[$i][enabled]) $tmp_checked = "checked"; else $tmp_checked = "";

		$t->set_var(array(
			"BGCOLOR"	=>	$bg_color,
			"ID"		=>	$list[$i][id],
			"NAME"		=>	$list[$i][headline],
			"DATE"		=>	date("d-m-Y", $list[$i][publishdate]),
			"TYPE"		=>	$list[$i][cat_name],
			"I"			=>	$ii++,
			"CHECKED"	=>	$tmp_checked,
		));
		$t->parse("_list", "list", true);
	}

	############################################################# страницы
	$pages = @(count($list)/$_default_news_per_page);
	$t->set_block("index", "news_pages", "_news_pages");
	if ($pages<=1) $t->set_var("news_pages", "");
	if (!$pages) $t->set_var("_news_pages", "");
	for ($i=0; $i<$pages; $i++) {
		$start = ($i * $_default_news_per_page) + 1;
		$finish = ($i * $_default_news_per_page) + $_default_news_per_page;
		if ($finish>count($list)) $finish = count($list);
		if ($i == $data->GET["page"]) $tmp = "<b>[".$start."-".$finish."]</b>"; else $tmp = "[".$start."-".$finish."]";
		$t->set_var(array(
				"NEWS_PAGE"			=>	$i,
				"NEWS_PAGE_NAME"	=>	$tmp
	    ));
		$t->parse("_news_pages", "news_pages", true);
	}
	#############################################################

	unset($list);

}


###########################################
if ($what == "edit") {
	$t->set_var(array(
		"NEWSLETTER_TEXT"		=>	$newsletter_status
	));
	$new = $news->GetNewsByID($data->GET["id"]);
	if ($new[enabled]) $tmp_checked = "checked"; else $tmp_checked = "";

	//$new[headline] = eregi_replace("\"([^\"]*)\"" , "«\\1»", $new[headline]);
	//$new[details] = eregi_replace("\"([^\"]*)\"" , "«\\1»", $new[details]);
	$new[headline] = stripcslashes($new[headline]);
	$new[headline] = stripcslashes($new[headline]);

	if (!strlen($new[url])) {		$new[url] = str_replace("&quot;", "\"", $new[headline]);		$new[url] = translitIt($new[url]);
	}

	$t->set_var(array(
		"ID"		=>	$data->GET["id"],
		"PID"		=>	$pid,
		"NAME"		=>	$new[headline],
		"URL"		=>	$new[url],
		"TEXTID"	=>	$new[txt_id],
		"SHORT"		=>	$new[annot],
		"FULL"		=>	$new[details],
		"FORUM"		=>	$new[forum_id],
		"CHECKED"	=>	$tmp_checked,
		"PAGE"		=>	$data->GET["page"]
	));

	$t->set_block("index", "cat", "_cat");
	$news_cats = $news->GetNewsCats();
	for ($i=0; $i<count($news_cats); $i++) {
		if ($news_cats[$i][id] == $new[cat_id]) $tmp_selected = "selected"; else $tmp_selected = "";
		$t->set_var(array(
			"CAT"			=>	$news_cats[$i][id],
			"CAT_NAME"		=>	$news_cats[$i][name],
			"CAT_SELECTED"	=>	$tmp_selected
		));
		$t->parse("_cat", "cat", true);
	}
	unset($news_cats);

	############################################################################################################################################
	$t->set_block("index", "day", "_day");
	for ($i=1; $i<32; $i++) {
		if ($i == date("d", $new[publishdate])) $tmp_selected = "selected"; else $tmp_selected = "";
		$t->set_var(array(
			"DAY"			=>	$i,
			"DAY_SELECTED"	=>	$tmp_selected
		));
		$t->parse("_day", "day", true);
	}

	$t->set_block("index", "month", "_month");
	for ($i=1; $i<13; $i++) {
		if ($i == round(date("m", $new[publishdate]))) $tmp_selected = "selected"; else $tmp_selected = "";
		$t->set_var(array(
			"MONTH"				=>	$i,
			"MONTH_SELECTED"	=>	$tmp_selected
		));
		$t->parse("_month", "month", true);
	}

	$t->set_block("index", "year", "_year");
	for ($i=2003; $i<(date("Y")+2); $i++) {
		if ($i == date("Y", $new[publishdate])) $tmp_selected = "selected"; else $tmp_selected = "";
		$t->set_var(array(
			"YEAR"				=>	$i,
			"YEAR_SELECTED"		=>	$tmp_selected
		));
		$t->parse("_year", "year", true);
	}
	############################################################################################################################################


	$imgs = $news->GetImagesForNews($data->GET["id"], $_default_images_dir.$_default_images_dir_news);
	$t->set_block("index", "img_list", "_img_list");
	if (!count($imgs)) $t->set_var("_img_list", "");
	for ($i=0; $i<count($imgs); $i++) {
		$t->set_var(array(
			"IMG_URL"	=>	$_default_images_dir.$_default_images_dir_news.$imgs[$i],
			"IMG_NAME"	=>	$imgs[$i]
		));
		$t->parse("_img_list", "img_list", true);
	}
}





$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
