<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"bo_users.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

######################################################################	сохранить статусы у пользователей
if ($what == "save_status") {
	$enabled = $_REQUEST["enabled"];
	$fio = $_REQUEST["fio"];
	$ids = $_REQUEST["ids"];

	for ($i=0; $i<count($ids); $i++) {
		$users->ChangeStatus($ids[$i], $enabled[$i]);
		$users->ChangeName($ids[$i], $fio[$i]);
	}

	header("Location: ".$_SERVER["SCRIPT_NAME"]);
	die();
}

######################################################################	добавляем пользователя
if ($what == "add_user") {
	$login = trim($_REQUEST["login"]);
	$name = trim($_REQUEST["name"]);
	$password = trim($_REQUEST["password"]);

	if ( strlen($login) && strlen($password) ) {
		$login_exist = $users->CheckLogin($login);
		if (!$login_exist) $users->AddUser($login, $password, $name);
	}

	header("Location: ".$_SERVER["SCRIPT_NAME"]);
	die();
}

######################################################################	добавляем пользователя
if ($what == "save_sections") {	$users->SaveSectionsAccess($_REQUEST["user_id"], $_REQUEST["ids"], $_REQUEST["enabled"]);

	header("Location: ".$_SERVER["SCRIPT_NAME"]);
	die();
}

$actions = array("general", "sections");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


########################################### список пользователей
if ($what == "general") {
	$users_list = $users->GetUsers();
	$t->set_block("index", "users_list", "_users_list");
	for ($i=0; $i<count($users_list); $i++) {
		if ($users_list[$i][enabled]) $tmp_checked = "checked"; else $tmp_checked = "";
		$t->set_var(array(
			"USER_ID"		=>	$users_list[$i][id],
			"USER_NAME"		=>	$users_list[$i][login],
			"USER_FIO"		=>	$users_list[$i][name],
			"USER_I"		=>	$i,
			"USER_CHECKED"	=>	$tmp_checked
		));
		$t->parse("_users_list", "users_list", true);
	}
}


########################################### список разделов
if ($what == "sections") {
	$t->set_var(array(
		"USER_NAME"		=>	$users->GetLoginById($data->GET["user_id"]),
		"USER_FIO"		=>	$users->GetNameById($data->GET["user_id"]),
		"USER_ID"		=>	$data->GET["user_id"]
		));

	$sections = $users->GetSections();
	$t->set_block("index", "sections_list", "_sections_list");
	for ($i=0; $i<count($sections); $i++) {
		if ($sections[$i][sub]) $tmp_tab = "&nbsp;&nbsp;&nbsp;"; else $tmp_tab = "";
		if ($users->GetSectionAccess($data->GET["user_id"], $sections[$i][id])) $tmp_checked = "checked"; else $tmp_checked = "";

		$t->set_var(array(
			"SECTION_ID"		=>	$sections[$i][id],
			"SECTION_NAME"		=>	$sections[$i][name],
			"SECTION_I"			=>	$i,
			"SECTION_CHECKED"	=>	$tmp_checked,
			"TAB"				=>	$tmp_tab
		));
		$t->parse("_sections_list", "sections_list", true);
	}
}


$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
