<?
################################################################# �������� ��, ��� ����
require ("libs/library.php");


setlocale(LC_ALL, "ru_RU.utf8");
$sql = new SQLClass();
$res = $sql->query("SET NAMES utf8");
$sql->close();

function html2specialchars($str){
   $trans_table = array_flip(get_html_translation_table(HTML_ENTITIES));
   return strtr($str, $trans_table);
}

function GetVar($data, $var) {
	if (strlen($data->POST[$var])) $value = $data->POST[$var];
	if (strlen($data->GET[$var])) $value = $data->GET[$var];
	return $value;
}

################################################################# ������� ������
$t = new Template("templates/".$_curSchema."/", "keep");

################################################################# ��������� ������ ������ � ���������

$t->set_file(array(
   		"overall_header" 		=> "overall_header.tpl.htm",
		"overall_footer" 		=> "overall_footer.tpl.htm"
		));

##################################################################	������������� ������
$t->set_var(array(
	"CSS_FILE_LINK"		=>	"templates/".$_curSchema."/style.css",
	"META_ADDITIONAL"	=>	"",
	"IMAGE_DIR"			=>	"templates/".$_curSchema."/"
	));

##################################################################	������� ������� ����
$t->set_var(array(
		"URL_INDEX"					=>	$links['index'],
		"URL_LOGIN"					=>	$links['login'],
		"URL_LOGOUT"				=>	$links['logout'],
		"URL_BO_USERS"				=>	$links['bo_users'],
		"URL_BO_SECTIONS"			=>	$links['bo_sections'],
		"URL_BO_CHANGEPASSWORD"		=>	$links['bo_changepassword'],

		"URL_PR_CATS"				=>	$links['pr_cats'],
		"URL_PR_PRODUCTS"			=>	$links['pr_products'],
		"URL_PR_PRICELIST"			=>	$links['pr_pricelist'],

		"URL_CL_USERS"				=>	$links['cl_users'],

		"URL_NS_NEWS"				=>	$links['ns_news'],

		"URL_ORDERS"				=>	$links['or_orders'],
    ));


$users 		= new UsersClass();
$blocks 	= new BlocksClass();
$_userid = $users->UserInSystem();

if (!$_userid) {
	$blocks->HideBlock("overall_header", "section_menu");
	$blocks->HideBlock("overall_header", "logout_menu");
} else {
	#############################################################	������� ������� ����, ��������� ��������
	$t->set_block("overall_header", "section_menus", "_section_menus");
	$_sections = $users->GetUserSections();
	for ($i=0; $i<count($_sections); $i++) {
		$t->set_var(array(
			"SECTION_NAME"		=>	$_sections[$i][name],
			"SECTION_URL"		=>	$_sections[$i][url]
		));
		$t->parse("_section_menus", "section_menus", true);
	}

	#############################################################	������� �������, ��������� ��������
	$t->set_block("overall_header", "sub_section_menus", "_sub_section_menus");
	$_sections = $users->GetUserSubSections();
	if (!count($_sections)) { $t->set_var("_sub_section_menus", ""); }
	for ($i=0; $i<count($_sections); $i++) {
		$t->set_var(array(
			"SUB_SECTION_NAME"		=>	$_sections[$i][name],
			"SUB_SECTION_URL"		=>	$_sections[$i][url]
		));
		$t->parse("_sub_section_menus", "sub_section_menus", true);
	}
}


?>