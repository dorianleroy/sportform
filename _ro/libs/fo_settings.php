<?
## URL's
$links['index']						=	"./index.php";
$links['login']						=	"./login.php";
$links['logout']					=	"./logout.php?what=logout";
$links['bo_index']					=	"./bo_index.php";
$links['bo_users']					=	"./bo_users.php";
$links['bo_sections']				=	"./bo_sections.php";
$links['bo_changepassword']			=	"./bo_changepassword.php";
$links['bo_index']					=	"./bo_index.php";

$links['pr_cats']					=	"./pr_cats.php";
$links['pr_products']				=	"./pr_products.php";
$links['pr_pricelist']				=	"./pr_pricelist.php";

$links['cl_users']					=	"./cl_users.php";

$links['ns_news']					=	"./ns_news.php";

$links['or_orders']					=	"./or_orders.php";
//$links['www']						=	"http://www.cue.ru/_bo/";

## SITE
$_default_images_dir				=	"../uploads/";
$_default_images_dir_texts			=	"images/texts/";
$_default_images_dir_photoalbum		=	"images/photoalbum/";
$_default_images_dir_clubs			=	"images/clubs/";
$_default_images_dir_schools		=	"images/schools/";
$_default_images_dir_news			=	"images/news/";
$_default_images_dir_tips			=	"images/tips/";
$_default_images_dir_categories		=	"categories/";
$_default_images_dir_products		=	$_default_images_dir."images/products/";

$_default_min_year					=	2000;

##	ORDERS
$_default_orders_per_page			=	5;		//	������� ������� �� ��������
$_default_orders_colors[1]			=	"#99ffcc";	//
$_default_orders_colors[2]			=	"#ffff99";
$_default_orders_colors[3]			=	"#e9e9e9";
$_default_orders_colors[4]			=	"#ffcccc";
$_default_orders_colors[5]			=	"#ffcc99";
$_default_orders_colors[6]			=	"#ffccff";
$_default_orders_colors[7]			=	"#ffcccc";
$_default_orders_colors[8]			=	"#ffcccc";
$_default_orders_colors[9]			=	"#ffcc99";
$_default_orders_colors[10]			=	"#ffcc00";
$_default_orders_colors[11]			=	"#99ff00";
$_default_orders_colors[12]			=	"#99ff00";
$_default_orders_colors[13]			=	"#e9e9e9";
$_default_orders_colors[14]			=	"#ff66ff";
$_default_orders_colors[15]			=	"#ff9966";
$_default_orders_colors[16]			=	$_default_orders_colors[5];


#########################################
$_default_products_per_page			=	30;
$_default_news_per_page				=	30;

## cookies
$cookies_uniqid 	=	"fortuna_ccid";	//	��� ����, ������� ����� ���������� ����������� ����������
$cookies_uniqid_exp	=	3600*24*90;		//	����� ����� ���� ����������� �����

## month
$_months						=	array("", "������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������", "�������");

?>
