<?
########################################### by cooper
ini_set("error_reporting",	E_ALL & ~E_NOTICE);
ini_set("display_errors",	true);
ini_set("session.bug_compat_warn",	0);
########################################### by cooper
session_name("TIPSY_OSTRICH");
session_start();

if (!isset($_SITELIB["libdir"])) {
	$_SITELIB["libdir"]  = "libs/";
}

require_once($_SITELIB["libdir"] . "xss.class.php");
require_once($_SITELIB["libdir"] . "fo_settings.php"); ## include FRONT SIDE variables
require_once($_SITELIB["libdir"] . "settings.php"); ## Loading settings
require_once($_SITELIB["libdir"] . "phplib/prepend.php"); ## Loading phplib libraries

foreach ((array)$classCollab as $class) {
     require_once($_SITELIB["libdir"] . "classes/".$class);  ## Loading all required classes
}

if (!$_SESSION["lang_id"]){
	$lang_id = 1; //default value
}elseif (isset($HTTP_POST_VARS["lang_id"])) {
	$lang_id = $HTTP_POST_VARS["lang_id"];
}elseif (isset($HTTP_GET_VARS["lang_id"])){
	$lang_id = $HTTP_GET_VARS["lang_id"];
}
$_SESSION["lang_id"];

############## by cooper
$lang_id = 1;

if (!isset($_SITELIB["no_lang_files"])) {
	require_once("languages/language_".$langCollab[$lang_id].".inc");  ## Loading language spicific variables
}
?>
