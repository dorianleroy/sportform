<?
class ProductsClass {

function ProductsClass() {
}

####################################################################################
function GetCategories($pid) {
	global $tableCollab;
	
	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, short_name, full_name, enabled FROM ".$tableCollab["categories"]." WHERE pid='".$pid."' ORDER BY seq");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	
	return $q;
}

####################################################################################
function GetCatByID($id) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, short_name, full_name, seq, enabled FROM ".$tableCollab["categories"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	
	$sql = new SQLClass();
	$res = $sql->query("SELECT short, full FROM ".$tableCollab["categories_articles"]." WHERE cat_id='".$id."'");
	$sql->fetch();
	$q = $sql->Record;
	$sql->close();
	
	$z[short] = $q[short];
	$z[full] = $q[full];
	
	return $z;
}

####################################################################################
function CreateCat($pid, $short_name, $full_name, $enabled) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["categories"]." (pid, seq, short_name, full_name, enabled) 
						VALUES('".$pid."', '".time()."', '".$short_name."', '".$full_name."', '".$enabled."')");
	$sql->close();
}

######################################################	������� ��������� ������
function DeleteCat($id) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["categories"]." WHERE id='".$id."'");
	$sql->close();
}

####################################################################################
function SaveCat($id, $short_name, $full_name, $enabled) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["categories"]." SET short_name='".$short_name."', full_name='".$full_name."', enabled='".$enabled."' WHERE id='".$id."'");
	$sql->close();
}

####################################################################################
function UpdateCatSeq($pid, $id, $what) {
	global $tableCollab;
	
	$_this = $this->GetCatByID($id);
	
	if ($what == "up") $tmp_seq = "AND seq<'".$_this[seq]."' ORDER BY seq DESC"; else $tmp_seq = "AND seq>'".$_this[seq]."' ORDER BY seq";
	
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, seq FROM ".$tableCollab["categories"]." WHERE pid='".$pid."' ".$tmp_seq." LIMIT 1");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	
	if ($z[id]) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["categories"]." SET seq='".$z[seq]."' WHERE id='".$id."'");
		$sql->query("UPDATE ".$tableCollab["categories"]." SET seq='".$_this[seq]."' WHERE id='".$z[id]."'");
		$sql->close();
	}
	unset($_this);
}

######################################################	������� ������ ��������� ��������
function SaveCatInfo($id, $short_name, $full_name, $article, $enabled) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["categories"]." SET short_name='".$short_name."', full_name='".$full_name."', enabled='".$enabled."' WHERE id='".$id."'");
	$sql->query("DELETE FROM ".$tableCollab["categories_articles"]." WHERE cat_id='".$id."'");
	$sql->query("INSERT INTO ".$tableCollab["categories_articles"]." (cat_id, full) VALUES ('".$id."', '".$article."')");
	$sql->close();
}

####################################################################################
function AddImage($text_id, $img_dir, $img, $name) {
	$fileinfo = pathinfo($img[name]);
	$extension = $fileinfo[extension];
	if (strlen($name)) $m_name = $name; else $m_name = substr(md5(uniqid(rand(), true)), 0, 5);
	$new_name = $text_id."_".$m_name."_.".$extension;
	copy($img[tmp_name], $img_dir.$new_name);
	chmod($img_dir.$new_name, 0666);
}

####################################################################################
function GetProducts($cid) {
	global $tableCollab;
	
	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, short_name, full_name, enabled FROM ".$tableCollab["products"]." WHERE cat_id='".$cid."' ORDER BY seq");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	
	return $q;
}

####################################################################################
function AddProduct($cat, $name, $enabled) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["products"]."(cat_id, seq, full_name, enabled) VALUES('".$cat."', '".time()."', '".$name."', '".$enabled."')");
	$sql->close();
}

####################################################################################
function DelProduct($id) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["products"]." WHERE id='".$id."'");
	$sql->query("DELETE FROM ".$tableCollab["products_articles"]." WHERE pr_id='".$id."'");
	$sql->close();
}

####################################################################################
function SaveProductStatus($pid, $enabled) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET enabled='".$enabled."' WHERE id='".$pid."'");
	$sql->close();
}

####################################################################################
function SaveProductInfo($id, $short_name, $full_name, $short, $full, $enabled) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET short_name='".$short_name."', full_name='".$full_name."', enabled='".$enabled."' WHERE id='".$id."'");
	$sql->query("DELETE FROM ".$tableCollab["products_articles"]." WHERE pr_id='".$id."'");
	$sql->query("INSERT INTO ".$tableCollab["products_articles"]." (pr_id, short, full) VALUES ('".$id."', '".$short."', '".$full."')");
	$sql->close();
}

####################################################################################
function UpdateProductSeq($cat, $id, $what) {
	global $tableCollab;
	
	$_this = $this->GetProductByID($id);
	
	if ($what == "up") $tmp_seq = "AND seq<'".$_this[seq]."' ORDER BY seq DESC"; else $tmp_seq = "AND seq>'".$_this[seq]."' ORDER BY seq";
	
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, seq FROM ".$tableCollab["products"]." WHERE cat_id='".$cat."' ".$tmp_seq." LIMIT 1");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	
	if ($z[id]) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["products"]." SET seq='".$z[seq]."' WHERE id='".$id."'");
		$sql->query("UPDATE ".$tableCollab["products"]." SET seq='".$_this[seq]."' WHERE id='".$z[id]."'");
		$sql->close();
	}
	unset($_this);
}

####################################################################################
function GetProductByID($id) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, short_name, full_name, enabled, seq FROM ".$tableCollab["products"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	
	$sql = new SQLClass();
	$res = $sql->query("SELECT short, full FROM ".$tableCollab["products_articles"]." WHERE pr_id='".$id."'");
	$sql->fetch();
	$q = $sql->Record;
	$sql->close();
	
	$z[short] = $q[short];
	$z[full] = $q[full];
	
	return $z;
}

####################################################################################
function UploadPricelist($pricelist_dir, $pricelist_name, $pricelist) {
	if ($pricelist) {
		unlink($pricelist_dir.$pricelist_name);
		copy($pricelist[tmp_name], $pricelist_dir.$pricelist_name);
		chmod($pricelist_dir.$pricelist_name, 0666);
	}
}

}
?>