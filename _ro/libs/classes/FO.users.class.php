<?
class UsersClass {

function UsersClass() {
}

####################################################################################
function GetScriptName() {
	global $_SERVER;

	//phpinfo();
	//$url = pathinfo($_SERVER[SCRIPT_URI]);
	$url = pathinfo($_SERVER[SCRIPT_NAME]);

	return $url['basename'];
}

####################################################################################
function UserInSystem() {
	global $_SESSION, $tableCollab;

	if ($_SESSION["random_id"]) {
		$sql = new SQLClass();
		$res = $sql->query("SELECT id FROM ".$tableCollab["users"]." WHERE uniqid='".$_SESSION["random_id"]."' AND enabled='1'");
		if (mysql_num_rows($res)) {
			$sql->fetch();
			$z = $sql->Record;
		}
		$sql->close();
	}
	return $z[id];
}

####################################################################################
function UserLogin($login, $password) {
	global $_SESSION, $tableCollab;

	$_SESSION["random_id"]	=	"";

	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["users"]." WHERE login='".$login."' AND password='".md5($password)."' AND enabled='1'");
	if (mysql_num_rows($res)) {
		$sql->fetch();
		$z = $sql->Record;

		$random_id = md5(uniqid(rand(), true));
		$sql0 = new SQLClass();
		$sql0->query("UPDATE ".$tableCollab["users"]." SET uniqid='".$random_id."' WHERE id='".$z[id]."'");
		$sql0->close();

		//session_register("random_id");
		$_SESSION["random_id"]	=	$random_id;
	}
	$sql->close();

	return $z[id];
}

####################################################################################
function UserLogout() {
	global $_SESSION, $tableCollab;

	$userid = $this->UserInSystem();
	$random_id = md5(uniqid(rand(), true));
	$sql0 = new SQLClass();
	$sql0->query("UPDATE ".$tableCollab["users"]." SET uniqid='".$random_id."' WHERE id='".$userid."'");
	$sql0->close();

	session_unregister("random_id");
}

####################################################################################
function GetUserSections() {
	global $_SESSION, $tableCollab;

	$q = array();
	$_userid	=	$this->UserInSystem();
	$sql = new SQLClass();
	$res = $sql->query("SELECT s.id, s.name, s.url
						FROM ".$tableCollab["sections"]." s, ".$tableCollab["users2sections"]." u2s
						WHERE u2s.user_id='".$_userid."' AND s.id=u2s.section_id AND s.pid='0' AND s.enabled='1'
						ORDER BY s.name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetUserSubSections() {
	global $_SERVER, $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, pid FROM ".$tableCollab["sections"]." WHERE url='".$this->GetScriptName()."' AND enabled='1'");
	if ($res) {
		$sql->fetch();
		$z = $sql->Record;
	}
	$sql->close();

	if ($z[pid]) {
		$pid = $z[pid];
	} else {
		if ($z[id]) {
			$pid = $z[id];
		} else $pid = -1;
	}

	$q = array();
	$_userid	=	$this->UserInSystem();

	$sql = new SQLClass();
	$res = $sql->query("SELECT s.id, s.name, s.url
						FROM ".$tableCollab["sections"]." s, ".$tableCollab["users2sections"]." u2s
						WHERE u2s.user_id='".$_userid."' AND s.id=u2s.section_id AND s.pid='".$pid."' AND s.enabled='1'
						ORDER BY s.name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetUsers() {
	global $tableCollab;

	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, login, name, enabled FROM ".$tableCollab["users"]." ORDER BY login");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################################
function ChangeStatus($user_id, $enabled) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["users"]." SET enabled='".$enabled."' WHERE id='".$user_id."'");
	$sql->close();
}

####################################################################################
function ChangeName($user_id, $name) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["users"]." SET name='".$name."' WHERE id='".$user_id."'");
	$sql->close();
}

####################################################################################
function CheckAccess() {
	global $_SERVER, $tableCollab, $links, $MESSAGES;

	$_userid	=	$this->UserInSystem();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["sections"]." WHERE url='".$this->GetScriptName()."' AND enabled='1'");
	if ($res) {
		$sql->fetch();
		$z = $sql->Record;
		$MESSAGES["site_title"]	=	$MESSAGES["small_name"].": ".$z[name]." | ".$this->GetNameById($_userid);
	}
	$sql->close();

	$sql = new SQLClass();
	$res = $sql->query("SELECT section_id FROM ".$tableCollab["users2sections"]." WHERE section_id='".$z[id]."' AND user_id='".$_userid."'");
	if (mysql_num_rows($res)) {
	} else {
		header("Location: ".$links["index"]."?what=&url=".base64_encode($_SERVER[REQUEST_URI]));
		die();
	}
	$sql->close();
}

####################################################################################
function AddUser($login, $password, $name) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["users"]." (login, password, name, enabled, uniqid) VALUES('".$login."', '".md5($password)."', '".$name."', '0', '".md5(uniqid(rand(), true))."')");
	$sql->close();
}

####################################################################################
function CheckLogin($login) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["users"]." WHERE login='".$login."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[id];
}

####################################################################################
function GetLoginById($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT login FROM ".$tableCollab["users"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[login];
}

####################################################################################
function GetNameById($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT name FROM ".$tableCollab["users"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[name];
}

####################################################################################
function GetSections() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["sections"]." WHERE pid='0'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;

		$q[count($q)] = $z;

		$sql0 = new SQLClass();
		$res0 = $sql0->query("SELECT id, name FROM ".$tableCollab["sections"]." WHERE pid='".$z[id]."'");
		for ($i0=0; $i0<mysql_num_rows($res0); $i0++) {
			$sql0->fetch();
			$z0 = $sql0->Record;
			$z0[sub] = 1;
			$q[count($q)] = $z0;
		}
		$sql0->close();

	}
	$sql->close();

	return $q;
}

####################################################################################
function GetSectionsByPID($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name, enabled, url FROM ".$tableCollab["sections"]." WHERE pid='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetSectionAccess($user_id, $section_id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT section_id FROM ".$tableCollab["users2sections"]." WHERE user_id='".$user_id."' AND section_id='".$section_id."'");
	if (mysql_num_rows($res)) $ok = 1; else $ok = 0;
	$sql->close();

	return $ok;
}

####################################################################################
function SaveSectionsAccess($user_id, $ids, $enabled) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["users2sections"]." WHERE user_id='".$user_id."'");
	$sql->close();

	for ($i=0; $i<count($ids); $i++) {
		if ($enabled[$i]) {
			$sql = new SQLClass();
			$sql->query("INSERT INTO ".$tableCollab["users2sections"]." (user_id, section_id) VALUES ('".$user_id."', '".$ids[$i]."')");
			$sql->close();
		}
	}
}

####################################################################################
function ChangePassword($new_password) {
	global $tableCollab;

	$_userid	=	$this->UserInSystem();
	if ($_userid) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["users"]." SET password='".md5($new_password)."' WHERE id='".$_userid."'");
		$sql->close();
	}
}

####################################################################################
function UpdateSection($id, $name, $url, $enabled) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["sections"]." SET name='".$name."', url='".$url."', enabled='".$enabled."' WHERE id='".$id."'");
	$sql->close();
}

####################################################################################
function AddSection($pid, $name, $url, $enabled) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["sections"]." (pid, name, url, enabled) VALUES ('".$pid."', '".$name."', '".$url."', '".$enabled."')");
	$sql->close();
}

#################################################################	���������� �������� ��� ����������
function SetVisVar($vis_id, $varname, $value) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("SELECT COUNT(value) FROM ".$tableCollab["vars"]." WHERE visuniqid='".$vis_id."' AND varname='".$varname."'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();
	if ($z[0]) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["vars"]." SET value='".$value."' WHERE visuniqid='".$vis_id."' AND varname='".$varname."'");
		$sql->close();
	} else {
		$sql = new SQLClass();
		$sql->query("INSERT INTO ".$tableCollab["vars"]."(visuniqid, varname, value) VALUES('".$vis_id."', '".$varname."', '".$value."')");
		$sql->close();
	}
}

#################################################################	���������� ������ ����������
function GetVisVar($vis_id, $varname) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("SELECT value FROM ".$tableCollab["vars"]." WHERE visuniqid='".$vis_id."' AND varname='".$varname."'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();

	return $z[value];
}

####################################################################	�������� ��������� � ���
function SendTextToChat($vis_id, $sender, $text) {
	global $tableCollab;

	$t = time();
	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["vars_chat"]." (visitor_id, time, sender, text) VALUES ('".$vis_id."', '".$t."', '".$sender."', '".$text."')");
	$sql->close();
	return $t;
}

####################################################################	�������� ��������� � ���
function DeleteChat($vis_id) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["vars_chat"]." WHERE visitor_id='".$vis_id."'");
	$sql->close();
}

####################################################################	�������� ��������� � ���
function GetChat($vis_id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT time, sender, text FROM ".$tableCollab["vars_chat"]." WHERE visitor_id='".$vis_id."' ORDER BY time");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################	�������� ��������� � ���
function GetLastMessageTime($vis_id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT time FROM ".$tableCollab["vars_chat"]." WHERE visitor_id='".$vis_id."' ORDER BY time DESC LIMIT 1");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[time];
}

}
?>