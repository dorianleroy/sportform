<?
class parser {
  var $img_prefix;
  var $quote_header;
  var $quote_footer;
  var $img_counter;
  var $cl;
  var $acl;
////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

// Constructor
  function parser() {
    $this->img_counter=1;
    $this->img_prefix="";
    $this->quote_header="";
    $this->quote_footer="";
    $this->cl="";
  }

// set prefix, to load images, quote header and quote footer
  function set_params($pr="", $q_h="", $q_f="") {

    $this->img_prefix=$pr;
    $this->quote_header=$q_h;
    $this->quote_footer=$q_f;
  }

// set default style class
  function set_class($cls) {
    $this->cl=$cls;
  }

// set href style class
  function set_aclass($cls) {
    $this->acl=$cls;
  }

// parse data

  function parse($t) {
    $text=nl2br($t);
    $retval="";
    $len=strlen($text);
    $counter=0;
    $cond=0;
    $tmp="";
    $param="";
    while($counter<($len+1)) {
      switch($cond) {
      case 0: // outside
        switch($text{$counter}) {
        case '[':
          $cond=1;
          break;
        default:
          $retval.=$text{$counter};
          break;
        }
        break;
      case 1: // inside
        switch($text{$counter}) {
        case ']':
          $cond=4;
          break;
        case ' ':
          $cond=3;
          break;
        default:
          $tmp.=$text{$counter};
          break;
        }
        break;
      case 3: // reading param
        switch($text{$counter}) {
        case ']';
          $cond=4;
          break;
        default:
          $param.=$text{$counter};
          break;
        }
        break;
      case 4: // serving pair
	  	$cooper_tmp = $this->serve($tmp, $param);
        $retval.= $cooper_tmp;
        $retval.=$text{$counter};
        $tmp="";
        $param="";
        $cond=0;
        break;
      default:
        break;
      }
      $counter++;
    }
    return $retval;
  }

// Serve pair, and put HTML
  function serve($param, $value) {
  
    if($this->cl!="") $cl="class=\"".$this->cl."\"";
	if($this->acl!="") $acl="class=\"".$this->acl."\"";
    $retval="";
    $param=trim(strtolower($param));
    $value=trim(strtolower($value));
    if(strlen($param)==0) return "";
    switch($param) {
    case "f": // font
      $style="";
      $align="";
      $text_d="";
      $len=strlen($value) + 1;
      for($i=0; $i<$len; $i++) {
        switch($value{$i}) {
		case "\""; // class
          $cooper_class = "";
		  $i++;
		  while ($value{$i}!="\"") {
		  	$cooper_class .= $value{$i};
			$i++;
		  }
          break;
        case "b"; // bold
          $style.="font-weight: bold;";
          break;
        case "u"; // underlined
          $text_d="underline";
          break;
        case "s": // striked out
          $text_d="line-through";
          break;
        case "o"; // overlined
          $text_d="overline";
          break;
        case "i"; // italic text
          $style.="font-style: italic;";
          break;
        case "r"; // right alignment
          $align="right";
          break;
        case "c"; // center alignment
          $align="center";
          break;
        case "l"; // left alignment
          $align="left";
          break;
        }
      }
      if(strlen($value)==0) $retval="<FONT $cl>";
      else $retval="<FONT align=\"$align\" style=\"text-decoration:$text_d;$style\" $cl>";
	  if (strlen($cooper_class)) { $retval="<FONT class=\"$cooper_class\">"; }
      break;
    case "/f":
      $retval="</FONT>";
      break;
	case "li":
		$retval	=	"<li>";
		break;
	case "/li":
		$retval	=	"</li>";
		break;
	case "ul":
		$retval	=	"<ul>";
		break;
	case "/ul":
		$retval	=	"</ul>";
		break;
    case "img": // image
      $i_s=$this->img_prefix;
      $pr="";
      switch(substr($value,0,2)) {
      case "l ":
        $pr="align=\"left\"";
        $img_sr=substr($value, 2);
        break;
      case "r ":
        $pr="align=\"right\"";
        $img_sr=substr($value, 2);
        break;
      case "c ":
        $pr="align=\"center\"";
        $img_sr=substr($value, 2);
        break;
      default:
        $img_sr=$value;
        break;
      }
      $img_sr=$this->img_prefix."/".$img_sr;
      $retval="<IMG src=\"$img_sr\" $pr $cl>";
      break;
    case "q": // quotation
      $retval=$this->quote_header;
      break;
    case "/q":
      $retval=$this->quote_footer;
      break;
    case "url":
      if($value{0} == "s" && $value{1}==" ") {
        $target="";
        $tr=substr($value, 2);
      } else {
        $target="target=\"blank_\"";
        $tr=$value;
      }
      $retval="<a href=\"$tr\" $target $acl>";
      break;
    case "/url":
      $retval="</a>";
      break;
    default:
      if(strlen($value)==0) {
        $retval="[$param]";
      } else {
        $retval="[$param $value]";
      }
      break;
    }
    return $retval;
  }
}

?>
