<?
class CATS_AND_PRODUCTSClass {

function CATS_AND_PRODUCTSClass() {
}


################################################################# добавляем
################################################################# категорию
function AddCat($cid, $name, $short_name, $url) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["categoryes"]." (pid, seq, name, short_name, url) VALUES ('".$cid."', '".time()."', '".$name."', '".$short_name."', '".$url."')");
	$sql->close();

	return $id;
}

################################################################# Сохранить информацию
################################################################# о категории
function UpdateCat($id, $short_name, $name, $enabled, $url, $desc, $keywords) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["categoryes"]." SET short_name='".$short_name."', name='".$name."', enabled='".$enabled."', url='".$url."', _desc='".$desc."', _keywords='".$keywords."' WHERE id='".$id."'");
	$sql->close();

	return $id;
}

################################################################# Изменить сортировку
################################################################# категории
function UpdateCatSeq($pid, $id, $what) {
	global $tableCollab;

	$_this = $this->GetCatByID($id);

	if ($what == "up") $tmp_seq = "AND seq<'".$_this[seq]."' ORDER BY seq DESC"; else $tmp_seq = "AND seq>'".$_this[seq]."' ORDER BY seq";

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, seq FROM ".$tableCollab["categoryes"]." WHERE pid='".$pid."' ".$tmp_seq." LIMIT 1");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	if ($z[id]) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["categoryes"]." SET seq='".$z[seq]."' WHERE pid='".$pid."' AND id='".$id."'");
		$sql->query("UPDATE ".$tableCollab["categoryes"]." SET seq='".$_this[seq]."' WHERE pid='".$pid."' AND id='".$z[id]."'");
		$sql->close();
	}
}

################################################################# транслитерация
################################################################# русского текста
function translitIt($str) {
    $tr = array(
        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "_", "."=> "", "/"=> "_", "," => "", "ё"=>"e",
        ";"=>":", "\""=>"_", "'"=>"", "№"=>"N", "«"=>"", "»"=>"",
        "+"=>"-", "&"=>"", "("=>"", ")"=>"", "«"=>"", "»"=>"", "$"=>""
    );
    return strtr($str,$tr);
}

################################################################# добавляем товар
################################################################# в категорию
function AddProduct($cid, $name) {
	global $tableCollab;

	if (strpos($name, "'")) {		$name	=	addslashes($name);	}
    $seq = time();
	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["products"]." (name, url,seq) VALUES ('".$name."', '".$this->translitIt($name)."','".$seq."')");
	$id = mysql_insert_id();
	$sql->query("INSERT INTO ".$tableCollab["cats2prods"]." (cat_id, prod_id) VALUES ('".$cid."', '".$id."')");
	$sql->close();

	return $id;
}

################################################################# функиця, возвращающая
################################################################# текущий курс
function GetCurrency($name) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("SELECT value FROM ".$tableCollab["currency"]." WHERE currency='".$name."'");
	$sql->fetch();
  	$z = $sql->Record;
	$sql->close();
	return $z[value];
}

################################################################# взять ID продукта, который
################################################################# можно купить вместе с этим продуктом, но дешевле
function GetBuyTogetherID($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT pid2 FROM ".$tableCollab["buytogether"]." WHERE pid1='".$pid."'");
	$sql->fetch();
  	$z = $sql->Record;
	$sql->close();
	return $z[pid2];
}

################################################################# обновить товар, который
################################################################# можно купить вместе с этим продуктом, но дешевле
function UpdateBuyTogetherID($pid, $item) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["buytogether"]." WHERE pid1='".$pid."'");
	$sql->query("INSERT INTO ".$tableCollab["buytogether"]." (pid1, pid2) VALUES('".$pid."', '".$item."')");
	$sql->close();
}

################################################################# обновить товар, который
################################################################# можно купить вместе с этим продуктом, но дешевле
function DelBuyTogetherID($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["buytogether"]." WHERE pid1='".$pid."'");
	$sql->close();
}

################################################################# взять все товары,
################################################################# которые просмотрели за указанный период
function GetProductsViewsByPeriod($from_date, $to_date) {
	global $tableCollab;

	$q = array();

	if (($to_date-$from_date)>(60*60*24)) $limit = "LIMIT 100"; else $limit = "";

	$sql = new SQLClass();
	$res = $sql->query("SELECT DISTINCT(pid), SUM(views) s
				 FROM ".$tableCollab["stat_products_views"]."
				 WHERE time>='".$from_date."' AND time<='".$to_date."'
				 GROUP BY pid
				 ORDER BY s DESC
				 ".$limit);
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

################################################################# взять список ID статей, которые
################################################################# могут быть интересны для данного продукта
function GetP2ARTICLE($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT article FROM ".$tableCollab["products2articles"]." WHERE product_id='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z[article];
	}
	$sql->close();
	return $q;
}

################################################################# Удалить товары из P2ARTICLE
#################################################################
function DelFromP2ARTICLE($pid, $article) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["products2articles"]." WHERE product_id='".$pid."' AND article='".$article."'");
	$sql->close();
}

################################################################# Добавить товар
################################################################# для P2ARTICLE
function AddP2ARTICLE($pid, $article) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["products2articles"]." (product_id, article) VALUES ('".$pid."', '".$article."')");
	$sql->close();
}

################################################################# взять список ID продуктов, которые
################################################################# надо не забыть купить с товарами этой категории
function GetP2CID($cid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT product_id FROM ".$tableCollab["products2cats"]." WHERE cat_id='".$cid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z[product_id];
	}
	$sql->close();
	return $q;
}

################################################################# Удалить товары из P2C
################################################################# товар
function DelFromP2C($cid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["products2cats"]." WHERE cat_id='".$cid."' AND product_id='".$p2p."'");
	$sql->close();
}

################################################################# Добавить товар
################################################################# для P2C
function AddP2C($cid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["products2cats"]." (cat_id, product_id) VALUES ('".$cid."', '".$p2p."')");
	$sql->close();
}


################################################################# взять список ID продуктов, которые
################################################################# "часто" покупают вместе с этим продуктом
function GetP2PID($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT product_id2 FROM ".$tableCollab["products2products"]." WHERE product_id1='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z[product_id2];
	}
	$sql->close();
	return $q;
}

################################################################# Удалить товары из P2P
################################################################# товар
function DelFromP2P($pid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["products2products"]." WHERE product_id1='".$pid."' AND product_id2='".$p2p."'");
	$sql->close();
}

################################################################# Добавить товар
################################################################# для P2P
function AddP2P($pid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["products2products"]." (product_id1, product_id2) VALUES ('".$pid."', '".$p2p."')");
	$sql->close();
}

################################################################# взять список ID продуктов, которые
################################################################# похожи (альтернатива) с этим продуктом
function GetP2ALT($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT product_id2 FROM ".$tableCollab["products2alternative"]." WHERE product_id1='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z[product_id2];
	}
	$sql->close();
	return $q;
}

################################################################# Удалить товары из P2ALT
################################################################# товар
function DelFromP2ALT($pid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["products2alternative"]." WHERE product_id1='".$pid."' AND product_id2='".$p2p."'");
	$sql->close();
}

################################################################# Добавить товар
################################################################# для P2ALT
function AddP2ALT($pid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["products2alternative"]." (product_id1, product_id2) VALUES ('".$pid."', '".$p2p."')");
	$sql->close();
}

################################################################# обновить цены и статус
################################################################# у продукта
function UpdateProductGeneralInfo($pid, $price, $price2, $enabled, $warehouse, $seq) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET price='".$price."', price2='".$price2."', enabled='".$enabled."', warehouse='".$warehouse."', seq='".$seq."' WHERE id='".$pid."'");
	$sql->close();
}

################################################################# обновить цены и статус
################################################################# у продукта
function UpdateProductUrl($pid, $url, $enabled) {
	global $tableCollab;
	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET url='".$url."', enabled='".$enabled."' WHERE id='".$pid."'");
	$sql->close();
}

################################################################# обновить цены и статус
################################################################# у продукта
function UpdateProductName($pid, $name, $enabled) {
	global $tableCollab;
	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET name='".$name."', enabled='".$enabled."' WHERE id='".$pid."'");
	$sql->close();
}

################################################################# обновить цены и статус
################################################################# у продукта
function UpdateProductFullInfo($pid, $name, $vendor, $url, $descr, $full_descr, $price, $old_price, $enabled, $page_descr, $page_keywords, $code, $prcode, $youtube, $for_unit, $units, $f_new, $f_hot, $f_popular, $price2, $for_unit2, $size_chart, $color) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET
				 name='".$name."', vendor='".$vendor."', descr='".$descr."', full_descr='".$full_descr."', price='".$price."',
				 old_price='".$old_price."', enabled='".$enabled."', page_descr='".$page_descr."', page_keywords='".$page_keywords."',
				 code='".$code."', prcode='".$prcode."', youtube='".$youtube."', url='".$url."', for_unit='".$for_unit."', units='".$units."',
				 price2='".$price2."', for_unit2='".$for_unit2."', size_chart='".$size_chart."', color='".$color."'
				 WHERE id='".$pid."'");
	$sql->close();
}

################################################################# создаем массив
################################################################# категорий
function GetProductsTree($pid, $tree, $notproducts="", $enabled="") {
	global $tableCollab;

	$cat = $this->GetSimpleCatsList($pid);
	for ($i=0; $i<count($cat); $i++) {
		$tmp = count($tree);
		$tree[$tmp][cat_name] 	= $cat[$i][name];
		$tree[$tmp][cat_id] 	= $cat[$i][id];
		if (!$notproducts) $tree[$tmp][products]	= $this->GetProductsByCID($cat[$i][id], null, null, null, null, 'old_price DESC, price', $enabled);

		$tree = $this->GetProductsTree($cat[$i][id], $tree, $notproducts, $enabled);
	}

	return $tree;
}

################################################################# создаем массив
################################################################# категорий
function GetSimpleCatsList($pid) {
	global $tableCollab;
	$q = array();
	if ($pid == "-1") $_and_pid = ""; else $_and_pid = "AND pid='".$pid."'";
	$sql = new SQLClass();
	$sql->query("SELECT id, short_name, name, url, pid, enabled
    	         FROM ".$tableCollab["categoryes"]."
				 WHERE 1 $_and_pid
				 ORDER BY seq
			 	 ");
	while($sql->fetch()) {
    	$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

################################################################# вернуть список
################################################################# статей для категории
function GetArticlesIDforCat($pid) {
	global $tableCollab;
	$sql = new SQLClass();
	$sql->query("SELECT articles FROM ".$tableCollab["categoryes"]." WHERE 1 AND id='".$pid."'");
	$sql->fetch();
    $z = $sql->Record;
	$sql->close();
	return $z[articles];
}

################################################################# создаем массив
################################################################# всех производителей
function GetVendorsList() {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT id, name FROM ".$tableCollab["vendors"]." ORDER BY name");
	while($sql->fetch()) {
    	$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

################################################################# создаем массив
################################################################# всех едениц измерения
function GetUnitsList() {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT id, name FROM ".$tableCollab["units"]." ORDER BY name");
	while($sql->fetch()) {
    	$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

################################################################# создаем массив
################################################################# всех едениц измерения
function GetUnitByID($id) {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT name FROM ".$tableCollab["units"]." WHERE id='".$id."'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();
	return $z[name];
}

################################################################# создаем массив
################################################################# всех едениц измерения
function GetUnitShortNameByID($id) {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT short_name FROM ".$tableCollab["units"]." WHERE id='".$id."'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();
	return $z[short_name];
}

################################################################# взять информацию
################################################################# о конкретном производителе
function GetVendorById($vendor_id, $img_dir) {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT id, name FROM ".$tableCollab["vendors"]." WHERE id='".$vendor_id."'");
	$sql->fetch();
    $z = $sql->Record;
	$q[title] = $z[title];
	$sql->close();

	$sql = new SQLClass();
	$sql->query("SELECT text FROM ".$tableCollab["vendors_texts"]." WHERE vendor_id='".$vendor_id."'");
	$sql->fetch();
    $z = $sql->Record;
	$q[text] = $z[text];
	$sql->close();

	##### img logo
	if (file_exists($img_dir.$vendor_id."_small_logo.gif")) $q[logo] = $img_dir.$vendor_id."_small_logo.gif";
	if (file_exists($img_dir.$vendor_id."_small_logo.jpg")) $q[logo] = $img_dir.$vendor_id."_small_logo.jpg";
	if (file_exists($img_dir.$vendor_id."_small_logo.png")) $q[logo] = $img_dir.$vendor_id."_small_logo.png";

	return $q;
}

################################################################# категория по ID
#################################################################
function GetCatByID($id) {
	global $tableCollab;
	$sql = new SQLClass();
	$sql->query("SELECT id, pid, short_name, name, url, _desc, _keywords, enabled, seq FROM ".$tableCollab["categoryes"]." WHERE id='".$id."'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();
	return $z;
}

################################################################# массив всех подкатегорий
#################################################################
function GetSubCatsInCat($pid, $list) {
	global $tableCollab;

	$list[count($list)] = $pid;

	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["categoryes"]." WHERE pid='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$list = $this->GetSubCatsInCat($z[id], $list);
	}
	$sql->close();
	return array_unique($list);
}

################################################################# проверка на то
################################################################# дочерняя ли это категория
function IsThisChildCat($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("SELECT pid FROM ".$tableCollab["categoryes"]." WHERE id='".$pid."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[pid];
}

################################################################# проверка на то
################################################################# родительская ли это категория
function IsThisParentCat($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("SELECT COUNT(id) FROM ".$tableCollab["categoryes"]." WHERE pid='".$pid."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[0];
}

################################################################# формируем список производителей
################################################################# в этой категории
function GetVendorsInCat($cid) {
	global $tableCollab;

$q = array();

$vendors = $this->GetAllVendorsInCat($cid, "");

$v = explode(", ", $vendors);
$v = array_unique($v);
$vendors = implode(", ", $v);

if (strlen($vendors)) {

$sql = new SQLClass();
$a = $sql->query("SELECT id, name FROM ".$tableCollab["vendors"]." WHERE id IN(".$vendors.") ORDER BY name");
for ($i=0; $i<mysql_num_rows($a); $i++) {
	$sql->fetch();
	$z = $sql->Record;
	$q[count($q)] = $z;
}
$sql->close();

}

return $q;

}


################################################################# формируем список серий
################################################################# в этой категории
function GetSeriesInCat($cid) {
	global $tableCollab;

$q = array();

$sql = new SQLClass();
$a = $sql->query("SELECT ".$tableCollab["series"].".id, ".$tableCollab["series"].".name, ".$tableCollab["series"].".descr
				  FROM ".$tableCollab["series"]."
				  WHERE cat_id='".$cid."' ORDER BY seq");
for ($i=0; $i<mysql_num_rows($a); $i++) {
	$sql->fetch();
	$z = $sql->Record;
	$q[count($q)] = $z;
}
$sql->close();

return $q;

}

################################################################# формируем список производителей
################################################################# (в качестве их ID; не уникальные)
function GetAllVendorsInCat($cid, $vendors) {
	global $tableCollab;

	$sql = new SQLClass();
	$a = $sql->query("SELECT id, name FROM ".$tableCollab["categoryes"]." WHERE pid='".$cid."'");
	for ($i=0; $i<mysql_num_rows($a); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$vendors = $this->GetAllVendorsInCat($z[id], $vendors);
	}
	$sql->close();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$a = $sql->query("
			SELECT ".$tp.".vendor
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id
	");
	for ($i=0; $i<mysql_num_rows($a); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$vendors .= $z[vendor];
		if ($i < (mysql_num_rows($a)-1)) $vendors .= ", ";
	}
	$sql->close();

	return $vendors;
}


################################################################# считаем сколько
################################################################# товаров в этой категории
function GetProductsCountInCat($cid, $count) {
	global $tableCollab;

	$sql = new SQLClass();
	$a = $sql->query("SELECT id, name FROM ".$tableCollab["categoryes"]." WHERE pid='".$cid."'");
	for ($i=0; $i<mysql_num_rows($a); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$count = $this->GetProductsCountInCat($z[id], $count);
	}
	$sql->close();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$a = $sql->query("
			SELECT COUNT(".$tp.".id)
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id
	");
	$sql->fetch();
	$z = $sql->Record;
	$count += $z[0];
	$sql->close();

	return $count;
}

################################################################# взять картинку для
################################################################# товара
function Increaseorders($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET orders=orders+1 WHERE id='".$pid."'");
	$sql->close();
}

################################################################# взять картинку для
################################################################# товара
function GetImageForPID($img_dir, $type, $pid) {
	global $_curSchema;
	if (file_exists($img_dir.$pid."_".$type."_.gif")) return $img_dir.$pid."_".$type."_.gif";
	if (file_exists($img_dir.$pid."_".$type."_.jpg")) return $img_dir.$pid."_".$type."_.jpg";
	if (file_exists($img_dir.$pid."_".$type."_.png")) return $img_dir.$pid."_".$type."_.png";
	return $img_dir."noimage.jpg";
}

################################################################# взять все картинки для товара
#################################################################
function GetImagesForPID($img_dir, $pid, $color=0) {
	global $_curSchema;
	$q	=	array();
    $dir = $img_dir."/".$pid."_*-".$color."_.jpg";
##
    ## FALLBACK ##
##

//    if ($handle = opendir($img_dir)) {
//    while (false !== ($filename = readdir($handle))) {
//       if (strstr($filename, $pid))
//       if (!strpos($filename, "_main"))
//       if (!strpos($filename,"x"))
//           $q[count($q)] = $img_dir."/".$filename;
//    }
//    }

// //FALLBACK
    
    foreach (glob($dir) as $filename) {

        if (!strpos($filename, "_main")) $q[count($q)] = $filename;
    }
    
	sort($q, SORT_STRING);
	return $q;
}

####################################################################################
function AddImage($pid, $img_dir, $img, $name) {
	$fileinfo = pathinfo($img[name]);
	$extension = $fileinfo[extension];
	if (strlen($name)) $m_name = $name; else $m_name = substr(md5(uniqid(rand(), true)), 0, 5);
	$new_name = $pid."_".$m_name."_.".$extension;
	copy($img[tmp_name], $img_dir.$new_name);
	chmod($img_dir.$new_name, 0666);
}

####################################################################################
function DelImage($img, $img_dir) {
	unlink($img_dir.$img);
}

################################################################# берем весь путь с
################################################################# самой главной и вниз
function GetPathFromCID($cid) {
	global $tableCollab;

	$q = array();
	$tmp = $cid;

	while ($tmp) {
		$sql = new SQLClass();
		$sql->query("SELECT id, pid, name, short_name, url FROM ".$tableCollab["categoryes"]." WHERE id='".$tmp."'");
		$sql->fetch();
   		$z = $sql->Record;
		$tmp = $z[pid];
		$q[count($q)] = $z;
		$sql->close();
	}

	return $q;
}

################################################################# взять самую верхнюю
################################################################# главную категорию
function GetMainCatByPID($pid) {
	global $tableCollab;

	$q = $pid;

	while ($q) {
		$sql = new SQLClass();
		$sql->query("SELECT id, pid FROM ".$tableCollab["categoryes"]." WHERE id='".$q."'");
		$sql->fetch();
   		$z = $sql->Record;
		$q = $z[pid];
		$id = $z[id];
		$sql->close();
	}

	return $id;
}

################################################################# рекурсивное составление
################################################################# массива всех подкатегорий
function RecursiveSubCats($cid, $cats) {
	global $tableCollab;

	if ($this->IsThisParentCat($cid)) {
		$subcats = $this->GetSimpleCatsList($cid);
		for ($i=0; $i<count($subcats); $i++) {
			$cats = $this->RecursiveSubCats($subcats[$i][id], $cats);
		}
	} else {
		$cats[count($cats)] = $cid;
	}
	return $cats;
}

################################################################# список хитов продаж по номеру категории
################################################################# при этом категория может быть родительская
function GetBestSellers($cid, $limit) {
	global $tableCollab;

	######	для начала возьмем список всех подкатегорий
	$cats = array($cid);
	$cats = $this->RecursiveSubCats($cid, $cats);

	######	составляем из массива список через запятую
	$in_cats = implode(",", $cats);

	######	ну а теперь сами продукты
	$q = array();
	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id IN (".$in_cats.") AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series."
			ORDER BY ".$tp.".orders DESC
			LIMIT ".$limit."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}


################################################################# список рекомендованных по номеру категории
################################################################# при этом категория может быть родительская
function GetRecomended($cid, $limit) {
	global $tableCollab;

	######	для начала возьмем список всех подкатегорий
	$cats = array($cid);
	$cats = $this->RecursiveSubCats($cid, $cats);

	######	составляем из массива список через запятую
	$in_cats = implode(",", $cats);

	######	ну а теперь сами продукты
	$q = array();
	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id IN (".$in_cats.") AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series."
			ORDER BY ".$tp.".views DESC
			LIMIT ".$limit."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# список товаров
################################################################# по номеру категории
function GetProductsByCID($cid, $vendor, $series, $from, $limit, $sort = "", $enabled="") {
	global $tableCollab, $MESSAGES;

	if (!$this->GetCatByID($cid)) return 0;
	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];
	$ts = $tableCollab["series"];

	if (!strlen($sort)) $order_by	=	$tp.".price"; else $order_by	=	$tp.".".$sort;
	if (!$enabled) $where_enabled = ""; else $where_enabled = "AND ".$tp.".enabled='1'";
	if (!$vendor) $where_vendor = ""; else $where_vendor = "AND ".$tp.".vendor='".$vendor."'";
	if (strlen($from)&&strlen($limit)) $_limit = "LIMIT $from, $limit"; else $_limit = "";
	if (strlen($series)) $where_series = "AND series='".$series."'"; else $where_series = "";

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tp.".vendor, ".$tp.".url, ".$tp.".name, ".$tp.".code, ".$tp.".prcode, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".url,
			       ".$tp.".old_price, ".$tp.".views, ".$tp.".orders, ".$tp.".enabled, ".$tp.".warehouse, ".$tp.".units, ".$tp.".for_unit, ".$tp.".descr, ".$tp.".seq,
			       ".$tp.".price2, ".$tp.".for_unit2
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series." ".$where_enabled."
			ORDER BY ".$order_by.", ".$tp.".name
			$_limit
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[name]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# список товаров
################################################################# по номеру категории
function GetProductsByCatsList($list, $vendor, $series, $from, $limit) {
	global $tableCollab, $MESSAGES;

	if (!count($list)) return 0;
	$list_txt = implode(", ", $list);
	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];
	$ts = $tableCollab["series"];

	if (!$vendor) $where_vendor = ""; else $where_vendor = "AND ".$tp.".vendor='".$vendor."'";
	if (strlen($from)&&strlen($limit)) $_limit = "LIMIT $from, $limit"; else $_limit = "";
	if (strlen($series)) $where_series = "AND series='".$series."'"; else $where_series = "";

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders,
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id IN(".$list_txt.") AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series."
			ORDER BY ".$tp.".price, ".$tp.".series
			$_limit
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[title]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[title];

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["series"]." WHERE id='".$z[series]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		$z[series_name] = $z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}


################################################################# берем какое-то
################################################################# поле из продуктов по категории
function GetProductsFieldByCID($cid, $field) {
	global $tableCollab;

	if (!$this->GetCatByID($cid)) return 0;
	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".".$field."
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor."
			");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z[$field];
	}
	$sql->close();

	return $q;
}

################################################################# список товаров
################################################################# по номеру категории
function GetProductsCountInCID($cid, $vendor, $series) {
	global $tableCollab;

	if (!$this->GetCatByID($cid)) return 0;

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	if (!$vendor) $where_vendor = ""; else $where_vendor = "AND ".$tp.".vendor='".$vendor."'";
	if (strlen($series)) $where_series = "AND series='".$series."'"; else $where_series = "";

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT COUNT(".$tp.".id)
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series."
		");
	$sql->fetch();
	$z = $sql->Record;

	return $z[0];
}

################################################################# список товаров
################################################################# по списку категории
function GetProductsCountInList($list, $vendor, $series) {
	global $tableCollab;

	if (!count($list)) return 0;
	$list_txt = implode(", ", $list);

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	if (!$vendor) $where_vendor = ""; else $where_vendor = "AND ".$tp.".vendor='".$vendor."'";
	if (strlen($series)) $where_series = "AND series='".$series."'"; else $where_series = "";

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT COUNT(".$tp.".id)
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id IN(".$list_txt.") AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series."
		");
	$sql->fetch();
	$z = $sql->Record;

	return $z[0];
}

################################################################# взять ID категории
################################################################# по его URL
function GetCategoryIDByURL($url) {
	global $tableCollab, $MESSAGES;
	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["categoryes"]." WHERE url='".$url."' AND enabled='1'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z[id];
}

################################################################# взять ID товара
################################################################# по его URL
function GetProductIDByURL($url) {
	global $tableCollab, $MESSAGES;
	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["products"]." WHERE url='".$url."' AND enabled='1'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z[id];
}

################################################################# взять один товар
################################################################# по номеру товара
function GetProductByID($pid) {
	global $tableCollab, $MESSAGES;

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tc2p.".cat_id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price,
			".$tp.".orders, ".$tp.".enabled, ".$tp.".page_descr, ".$tp.".url, ".$tp.".size_chart,
			".$tp.".page_keywords, ".$tp.".code, ".$tp.".prcode, ".$tp.".warehouse, ".$tp.".youtube, ".$tp.".units, ".$tp.".for_unit, ".$tp.".price2, ".$tp.".for_unit2,
			".$tp.".color
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tp.".id='".$pid."' AND ".$tc2p.".prod_id='".$pid."'
		");
	if (!mysql_num_rows($res)) return 0;

	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	if (!$cat = $this->GetCatByID($z[cat_id])) return 0;
	$z[cat_name] = $cat[name];
	$z[cat_short_name] = $cat[short_name];

	$sql0 = new SQLClass();
	$sql0->query("SELECT name, country FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
	$sql0->fetch();
	$z0 = $sql0->Record;
	$sql0->close();
	if (!$z0[name]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[name];
	$z[vendor_name_country] = $z0[country];

	return $z;
}

################################################################# список товаров
################################################################# по случайному выбору
function GetRandomProducts($count) {
	global $tableCollab, $MESSAGES;

	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tc2p.".cat_id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders, MD5( RAND()*NOW() ) AS myRandom
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".prod_id=".$tp.".id
			ORDER BY myRandom
			LIMIT ".$count."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[title]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[title];

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["series"]." WHERE id='".$z[series]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		$z[series_name] = $z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# список популярных товаров
################################################################# в случайном порядке
function GetPopularProducts($count) {
	global $tableCollab, $MESSAGES;

	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT id, vendor, name, code, prcode, descr, full_descr, price, url,
			       old_price, views, orders, enabled, warehouse, units, for_unit, descr, MD5( RAND()*NOW() ) AS myRandom
			FROM ".$tp."
			WHERE enabled='1' AND f_popular='1'
			ORDER BY myRandom
			LIMIT ".$count."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

   		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[name]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# список новых товаров
################################################################# в случайном порядке
function GetNewProducts($count) {
	global $tableCollab, $MESSAGES;

	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT id, vendor, name, code, prcode, descr, full_descr, price, url,
			       old_price, views, orders, enabled, warehouse, units, for_unit, descr, MD5( RAND()*NOW() ) AS myRandom
			FROM ".$tp."
			WHERE enabled='1' AND f_new='1'
			ORDER BY myRandom
			LIMIT ".$count."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

   		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[name]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# список мнений о товаре
################################################################# по ид товара
function GetProductReviews($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT time, name, email, text, rating FROM ".$tableCollab["products_reviews"]." WHERE product_id='".$pid."' ORDER BY time DESC");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$z[rating] .= "-0";

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# берем последние
################################################################# новинки
function GetNoveltyProducts($count) {
	global $tableCollab;

	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tc2p.".cat_id, ".$tp.".vendor, ".$tp.".name, ".$tp.".prcode, ".$tp.".code, ".$tp.".url, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tp.".enabled='1' AND ".$tc2p.".prod_id=".$tp.".id
			ORDER BY ".$tp.".time_create DESC
			LIMIT ".$count."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[title]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[title];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# берем товары
################################################################# которые надо купить
################################################################# для этой категории
function GetDontForgetProductsByCID($cid) {
	global $tableCollab, $MESSAGES;

	if (!$this->GetCatByID($cid)) return 0;
	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["products2cats"];
	$tc2p_ = $tableCollab["cats2prods"];
	$ts = $tableCollab["series"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".product_id=".$tp.".id
			ORDER BY ".$tp.".views, ".$tp.".price
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[title]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[title];

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["series"]." WHERE id='".$z[series]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		$z[series_name] = $z0[name];

		$sql0 = new SQLClass();
		$sql0->query("SELECT cat_id FROM ".$tc2p_." WHERE prod_id='".$z[id]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		$z[cat_id] = $z0[cat_id];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# поиск товаров
################################################################# по ключевым словам
################################################################# и возможно выбранной категории
function SearchInProducts($cats, $find0) {
	global $tableCollab, $MESSAGES;
	################################################# узнаем в каких категориях искать
	if ($cats) {
		$catz = array($cats);
	} else {
		$catz = array();
	}
	$catz = $this->RecursiveSubCats($cats, $catz);
	################################################# узнаем в каких категориях искать

	######	ну а теперь сами продукты
	$q = array();
	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	######	составляем из массива список через запятую
	$in_cats = implode(",", $catz);
	if (strlen($in_cats)) $where_cats = "AND ".$tc2p.".cat_id IN (".$in_cats.")"; else $where_cats = "";

	$find	=	explode(" ", $find0);

	$where_find = "";
	for ($i=0; $i<count($find); $i++) {
		$where_find .= "AND (name LIKE '%".addslashes($find[$i])."%' OR descr LIKE '%".addslashes($find[$i])."%' OR full_descr LIKE '%".addslashes($find[$i])."%' OR page_keywords LIKE '%".addslashes($find[$i])."%')";
	}

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id
			FROM ".$tp.", ".$tc2p."
			WHERE 1 ".$where_cats." AND ".$tc2p.".prod_id=".$tp.".id ".$where_find."
			ORDER BY ".$tp.".views DESC, ".$tp.".name
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# взять список всех ID категорий, в которых находится товар
function GetCatsOfProduct($pid) {
	global $tableCollab, $MESSAGES;

	$q	=	array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT cat_id FROM ".$tableCollab["cats2prods"]." WHERE prod_id='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$q[count($q)]	=	$z['cat_id'];
	}
	$sql->close();

	return $q;
}

################################################################# Добавить новую категорию, в которой присутствует товар
function AddCatOfProduct($pid, $cat) {
	global $tableCollab, $MESSAGES;

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["cats2prods"]." (cat_id, prod_id) VALUES ('".$cat."', '".$pid."')");
	$sql->close();
}

################################################################# Удалить категорию в которой присутствует товар
function DelCatOfProduct($pid, $cat) {
	global $tableCollab, $MESSAGES;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["cats2prods"]." WHERE cat_id='".$cat."' AND prod_id='".$pid."'");
	$sql->close();
}

################################################################# список свойств
#################################################################
function GetProperties() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["properties"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}


################################################################# Добавить новое свойство
#################################################################
function AddProperty($name) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["properties"]." (name) VALUES ('".$name."')");
	$sql->close();
}

################################################################# Добавить новое свойство
################################################################# со значением для товара
function AddPropertyValue($pid, $property, $value) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' AND property_id='".$property."'");
	$sql->close();

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["prop2prod"]." (product_id, property_id, seq, value, enabled) VALUES ('".$pid."', '".$property."', '".time()."', '".$value."', '1')");
	$sql->close();
}

################################################################# Выбрать свойства
################################################################# и значения для товара
function GetPropertiesValues($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT property_id, value, seq FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' ORDER BY seq");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["properties"]." WHERE id='".$z[property_id]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		$z[property_name]	=	$z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# Выбрать свойства
################################################################# и значения для товара
function GetProductPropertyValue($pid, $property) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT value, seq FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' AND property_id='".$property."'");
	$sql->fetch();
	$z = $sql->Record;

	$sql0 = new SQLClass();
	$sql0->query("SELECT name FROM ".$tableCollab["properties"]." WHERE id='".$property."'");
	$sql0->fetch();
	$z0 = $sql0->Record;
	$sql0->close();
	$z[property_name]	=	$z0[name];
	$sql->close();

	return $z;
}

################################################################# Выбрать свойства
################################################################# и значения для товара
function SavePropertyValue($pid, $property, $value) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["prop2prod"]." SET value='".$value."' WHERE product_id='".$pid."' AND property_id='".$property."'");
	$sql->close();
}

################################################################# Выбрать свойства
################################################################# и значения для товара
function DelProductProperty($pid, $property) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' AND property_id='".$property."'");
	$sql->close();
}

####################################################################################
function UpdatePropertySeq($pid, $id, $what) {
	global $tableCollab;

	$_this = $this->GetProductPropertyValue($pid, $id);

	if ($what == "up") $tmp_seq = "AND seq<'".$_this[seq]."' ORDER BY seq DESC"; else $tmp_seq = "AND seq>'".$_this[seq]."' ORDER BY seq";

	$sql = new SQLClass();
	$res = $sql->query("SELECT property_id, seq FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' ".$tmp_seq." LIMIT 1");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	if ($z[property_id]) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["prop2prod"]." SET seq='".$z[seq]."' WHERE property_id='".$id."' AND product_id='".$pid."'");
		$sql->query("UPDATE ".$tableCollab["prop2prod"]." SET seq='".$_this[seq]."' WHERE property_id='".$z[property_id]."' AND product_id='".$pid."'");
		$sql->close();
	}
}

####################################################################################
function UpdateColorsSeq($pid, $id, $what) {
	global $tableCollab;

	$_this = $this->GetProductColorByID($pid, $id);

	if ($what == "up")
        $tmp_seq = "AND seq<'".$_this[seq]."' ORDER BY seq DESC";
    else
        $tmp_seq = "AND seq>'".$_this[seq]."' ORDER BY seq";

	$sql = new SQLClass();
	$res = $sql->query("SELECT color, seq FROM ".$tableCollab["products2colors"]." WHERE pid='".$pid."' ".$tmp_seq." LIMIT 1");

	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
//    var_dump($z); exit;
	if ($z[color]) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["products2colors"]." SET seq='".$z[seq]."' WHERE color='".$id."' AND pid='".$pid."'");
		$sql->query("UPDATE ".$tableCollab["products2colors"]." SET seq='".$_this[seq]."' WHERE color='".$z[color]."' AND pid='".$pid."'");
		$sql->close();
	}
}

####################################################################################
function UpdateProductSeq($pid, $what, $cid) {
    global $tableCollab;

    $_this = $this->GetProductColorByID($pid);

    if ($what == "up")
        $tmp_seq = "AND seq<'".$_this[seq]."' ORDER BY seq DESC";
    else
        $tmp_seq = "AND seq>'".$_this[seq]."' ORDER BY seq";

    $sql = new SQLClass();
    $res = $sql->query("
    SELECT id,seq
    FROM ".$tableCollab["products"].", ".$tableCollab['cats2prods']."
    WHERE
        ".$tableCollab['cats2prods'].".`prod_id` = ".$tableCollab["products"].".`id`
            AND ".$tableCollab['cats2prods'].".`cat_id` = ".$cid."
                ".$tmp_seq."
    LIMIT 1");
//    echo "
//        SELECT id,seq
//        FROM ".$tableCollab["products"].", ".$tableCollab['cats2prods']."
//        WHERE
//            ".$tableCollab['cats2prods'].".`prod_id` = ".$tableCollab["products"].".`id`
//                AND ".$tableCollab['cats2prods'].".`cat_id` = ".$cid."
//                    ".$tmp_seq."
//        LIMIT 1"; exit;
    $sql->fetch();
    $z = $sql->Record;

    $sql->close();
//     var_dump($z); exit;
    if ($z[seq]) {
        $sql = new SQLClass();
//        var_dump("UPDATE ".$tableCollab["products"]." SET seq='".$z[seq]."' WHERE id='".$pid."'");
//        var_dump("UPDATE ".$tableCollab["products"]." SET seq='".$_this[seq]."' WHERE id='".$z['id']."'");
//        exit;
        $sql->query("UPDATE ".$tableCollab["products"]." SET seq='".$z[seq]."' WHERE id='".$pid."'");
        $sql->query("UPDATE ".$tableCollab["products"]." SET seq='".$_this[seq]."' WHERE id='".$z['id']."'");
        $sql->close();
    }
}

####################################################################################
function UploadPricelist($img_dir, $img) {
	copy($img[tmp_name], $img_dir);
	chmod($img_dir, 0666);
}

####################################################################################
function SaveProductColor($id, $name, $color) {
	global $tableCollab;

	if ($id > 0) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["products_colors"]." SET name='".$name."', color='".$color."' WHERE id='".$id."'");
		$sql->close();
	} else {
		$sql = new SQLClass();
		$sql->query("INSERT INTO ".$tableCollab["products_colors"]."(name, color) VALUES('".$name."', '".$color."')");
		$sql->close();
	}
}

####################################################################################
function GetProductColors() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name, color FROM ".$tableCollab["products_colors"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
//        echo "<pre>", var_dump($z), "</pre>";
        if($z['color'] != '')
            $q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}
function GetProductColorsAdd() {
    global $tableCollab;

    $q = array();

    $sql = new SQLClass();
    $res = $sql->query("SELECT id, name, color FROM ".$tableCollab["products_colors"]." ORDER BY name");
    for ($i=0; $i<mysql_num_rows($res); $i++) {
        $sql->fetch();
        $z = $sql->Record;
    //        echo "<pre>", var_dump($z), "</pre>";
        if($z['color'] == '')
            $q[count($q)] = $z;
    }
    $sql->close();

    return $q;
}

####################################################################################
function GetProductColorsArray() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["products_colors"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z["id"];
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetProductColor($color) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name, color FROM ".$tableCollab["products_colors"]." WHERE id='".$color."'");
	$sql->fetch();
	$z = $sql->Record;
	return $z;
}

####################################################################################
function GetProductColorList($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT color FROM ".$tableCollab["products2colors"]." WHERE pid='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z["color"];
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetProductColorByID($pid, $id = NULL) {
	global $tableCollab;

    if ($id)
    {
        $add_query     = "AND color='".$id."'";
        $add_query2    = ', color';
        $add_query3    = 'pid';
        $table         = $tableCollab["products2colors"];

    }
    else
    {
        $add_query     = "";
        $add_query2    = "";
        $add_query3    = "id";
        $table = $tableCollab["products"];
    }

	$sql = new SQLClass();
	$res = $sql->query("SELECT $add_query3, seq$add_query2  FROM ".$table." WHERE $add_query3='".$pid."' $add_query ");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z;
}

####################################################################################
function GetSizeGroups() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["size_group"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetSizeLine($group) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["size_line"]." WHERE size_group='".$group."' ORDER BY seq");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetProductWarehouse($pid, $color, $size) {
	global $tableCollab;
	$sql = new SQLClass();
	$res = $sql->query("SELECT pid FROM ".$tableCollab["products_warehouse"]." WHERE pid='".$pid."' AND color='".$color."' AND size='".$size."'");
	$c	=	mysql_num_rows($res);
	$sql->close();
	return $c;
}

####################################################################################
function GetProductWarehouseSizesCount($pid, $color='') {
	global $tableCollab;
	$q	=	array();
	if ($color) $where_color	=	"AND color='".$color."'"; else $where_color = "";

	$sql = new SQLClass();
	$res = $sql->query("SELECT DISTINCT(size) FROM ".$tableCollab["products_warehouse"]." WHERE pid='".$pid."' ".$where_color."");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
   		$q[count($q)] = $z[0];
	}
	$sql->close();

	return count($q);
}

}
?>