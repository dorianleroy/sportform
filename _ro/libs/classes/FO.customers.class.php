<?
class CustomersClass {

function CustomersClass() {
}

###################################################################################################################
function FormatPhone($phone, $delimeter=", ") {
	$phone	=	str_replace(";", ",", $phone);
	$tmp	=	ereg_replace("([:alpha:])|( )|(\+)|(\()|(\))|(\-)", "", $phone);
	$s		=	explode(",", $tmp);
	$tmp	=	array();
	//print_r($s);
	for ($i=0; $i<count($s); $i++) {
		$tmp[$i]	=	substr($s[$i], strlen($s[$i])-7, 3)."-".substr($s[$i], strlen($s[$i])-4, 2)."-".substr($s[$i], strlen($s[$i])-2, 2);
		if (strlen($s[$i]) >= 10) {
			$tmp[$i]	=	substr($s[$i], strlen($s[$i])-10, 3)."-".$tmp[$i];
		}
		if (strlen($s[$i]) > 10) {
			$tmp[$i]	=	substr($s[$i], 0, strlen($s[$i])-10)."-".$tmp[$i];
		}
	}
	if (count($s) > 1) {
		$tmp	=	implode($delimeter, $tmp);
	} else {
		$tmp	=	$tmp[0];
	}
	return $tmp;
}


###################################################################################################################
function GetCustomerEmail($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT email FROM ".$tableCollab["customers"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[email];
}

###################################################################################################################
function GetDeliveryGeneralInfo($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT name, phone, town_id, town FROM ".$tableCollab["customers_delivery"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	if (strlen($z[town])>0) {
		$z[town_name]	=	$z[town];
	} else {
		$sql = new SQLClass();
		$res = $sql->query("SELECT name FROM ".$tableCollab["towns"]." WHERE id='".$z[town_id]."'");
		$sql->fetch();
		$tmp = $sql->Record;
		$sql->close();
		$z[town_name]	=	$tmp[name];
	}

	return $z;
}

###################################################################################################################
function GetDeliveryFullInfo($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT name, company, phone, fax, town_id, town, street, house, apartment, building, section, entrance, level, code, metro, also FROM ".$tableCollab["customers_delivery"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	$z[town_name]	=	$this->GetTownByID($z[town_id]);
	$z[metro_name]	=	$this->GetMetroByID($z[metro]);

	return $z;
}

###################################################################################################################
function GetTownByID($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT name FROM ".$tableCollab["towns"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[name];
}

###################################################################################################################
function GetMetroByID($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT name FROM ".$tableCollab["metro"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[name];
}

###################################################################################################################
function GetLastVisitors($time) {
	global $tableCollab;

	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT visuniqid FROM ".$tableCollab["vars"]." WHERE varname='last_time_click' AND value>='".$time."' ORDER BY value DESC");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;

		$sql0 = new SQLClass();
		$res0 = $sql0->query("SELECT varname, value FROM ".$tableCollab["vars"]." WHERE visuniqid='".$z[visuniqid]."'");
		for ($i0=0; $i0<mysql_num_rows($res0); $i0++) {
			$sql0->fetch();
			$z0 = $sql0->Record;
			$q[$i][$z0[varname]] = $z0[value];
			$q[$i][visuniqid] = $z[visuniqid];
		}
		$sql0->close();

		############################ ����� �� IP
		if (!strlen($q[$i][ip_city])) {
			$__f	=	@file("http://www.ip2city.ru/ip2city.php?ip=".$q[$i][ip]);
			if (strlen($__f[0])) {
				$ip_city	=	trim (substr($__f[0], strpos($__f[0], "=")+1, strlen($__f[0])) );
			} else {
				$ip_city	=	"none";
			}
			unset($__f);
			$_user = new UsersClass();
			$_user->SetVisVar($z[visuniqid], "ip_city", $ip_city);
		}
		############################ //	����� �� IP
	}
	$sql->close();

	return $q;
}

###################################################################################################################
function GetVisitorsWithBasket() {
	global $tableCollab;

	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT visuniqid FROM ".$tableCollab["vars"]." WHERE varname='basket' AND value<>''");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = "'".$z[visuniqid]."'";
	}
	$sql->close();
	$q	=	array_unique($q);

	$q_in	=	implode(',', $q);

	unset($q);

	$sql = new SQLClass();
	$res = $sql->query("SELECT visuniqid FROM ".$tableCollab["vars"]." WHERE visuniqid IN (".$q_in.") AND varname='email' AND value<>''");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = "'".$z[visuniqid]."'";
	}
	$sql->close();
	$q	=	array_unique($q);
	$q_in	=	implode(',', $q);
	unset($q);

	$sql = new SQLClass();
	$res = $sql->query("SELECT visuniqid FROM ".$tableCollab["vars"]." WHERE visuniqid IN (".$q_in.") AND varname='last_time_click' ORDER BY value DESC");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z[visuniqid];
	}
	$sql->close();
	$q0	=	array_unique($q);

	unset($q);

	for ($i=0; $i<count($q0); $i++) {

		$sql0 = new SQLClass();
		$res0 = $sql0->query("SELECT varname, value FROM ".$tableCollab["vars"]." WHERE visuniqid='".$q0[$i]."'");
		for ($i0=0; $i0<mysql_num_rows($res0); $i0++) {
			$sql0->fetch();
			$z0 = $sql0->Record;
			$q[$i][$z0[varname]] = $z0[value];
			$q[$i][visuniqid] = $q0[$i];
		}
		$sql0->close();

		############################ ����� �� IP
		if (!strlen($q[$i][ip_city])) {
			$__f	=	@file("http://www.ip2city.ru/ip2city.php?ip=".$q[$i][ip]);
			if (strlen($__f[0])) {
				$ip_city	=	trim (substr($__f[0], strpos($__f[0], "=")+1, strlen($__f[0])) );
			} else {
				$ip_city	=	"none";
			}
			unset($__f);
			$_user = new UsersClass();
			$_user->SetVisVar($q0[$i], "ip_city", $ip_city);
		}
		############################ //	����� �� IP

	}

	return $q;
}

###################################################################################################################
function GetCustomers($status='', $last_login='') {
	global $tableCollab;

	$order	=	"id DESC";
	$where_status	=	"";
	if (strlen($status)) $where_status	.=	" AND status='".$status."'";
	if (strlen($last_login)) {
		$where_status	.=	" AND lastlogin<='".$last_login."'";
		$order	=	"lastlogin DESC";
	}

	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT
                            id, login, password, name, family, company, city, street, house, apartment, building, section ,entrance, level, code, metro, post_address, phone, fax, email, icq, skype, site, user_, user_boss, user_fin, user_marketing, time, status, newsletter, lastlogin, uniqid, whois, cash_payment, referer, organization
						FROM ".$tableCollab["customers"]."
						WHERE 1 ".$where_status."
						ORDER BY ".$order);
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

###################################################################################################################
function GetCustomersCount($enabled) {
	global $tableCollab;

	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT COUNT(id) FROM ".$tableCollab["customers"]." WHERE status='".$enabled."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z[0];
}

###################################################################################################################
function GetCustomerByID($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, login, password, name, family, company, city, street, house, apartment, building, section ,entrance, level, code, metro, post_address, phone, fax, email, icq, skype, site, user_, user_boss, user_fin,
								user_marketing, time, status, newsletter, lastlogin, uniqid, whois, cash_payment, referer, organization
								FROM ".$tableCollab["customers"]."
								WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z;
}

###################################################################################################################
function DelCustomerByID($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["customers"]." WHERE id='".$id."'");
	return true;
}

###################################################################################################################
function UpdateCustomerInfo($id, $login, $password, $company, $city, $address, $post_address, $phone, $fax, $email, $icq, $skype, $site, $user_, $user_boss, $user_fin, $status, $newsletter, $whois, $cash_payment, $organization) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["customers"]." SET
					login 			= '".$login."',
					password 		= '".$password."',
					company 		= '".$company."',
					city			= '".$city."',

    				post_address	= '".$post_address."',
					phone			= '".$phone."',
					fax				= '".$fax."',
					email			= '".$email."',
					icq				= '".$icq."',
					skype			= '".$skype."',
					site			= '".$site."',
					user_			= '".$user_."',
					user_boss		= '".$user_boss."',
					user_fin		= '".$user_fin."',
					status			= '".$status."',
					newsletter		= '".$newsletter."',
					whois			= '".$whois."',
					cash_payment	= '".$cash_payment."',
					organization	= '".$organization."'
				 WHERE id='".$id."'");
	$sql->close();
}

###################################################################################################################
function GetBirthdaysCustomers() {
	global $tableCollab;

	$_from 	= 	time() - (60*60*24*1);	//	5-�� ���� �� �������� �������
	$_to	=	time() + (60*60*24*5);	//	5-�� ���� ����� �������� �������

	$_from_day		=	date("d", $_from);
	$_from_month	=	date("m", $_from);
	$_to_day		=	date("d", $_to);
	$_to_month		=	date("m", $_to);

	$q = array();

	if ($_from_month == $_to_month) {
		$sql = new SQLClass();
		$res = $sql->query("SELECT id, email, discount, b_day, b_month FROM ".$tableCollab["customers"]."
							WHERE b_day >= '$_from_day' AND b_month >= '$_from_month' AND b_day <= '$_to_day' AND b_month <= '$_to_month'
							ORDER BY b_month, b_day");
		for ($i=0; $i<mysql_num_rows($res); $i++) {
			$sql->fetch();
			$z = $sql->Record;
			$q[count($q)] = $z;
		}
		$sql->close();
	} else {
		$sql = new SQLClass();
		$res = $sql->query("SELECT id, email, discount, b_day, b_month FROM ".$tableCollab["customers"]."
							WHERE b_day >= '$_from_day' AND b_month >= '$_from_month' AND b_day <= '31' AND b_month <= '$_from_month'
							ORDER BY b_month, b_day");
		for ($i=0; $i<mysql_num_rows($res); $i++) {
			$sql->fetch();
			$z = $sql->Record;
			$q[count($q)] = $z;
		}
		$sql->close();

		$sql = new SQLClass();
		$res = $sql->query("SELECT id, email, discount, b_day, b_month FROM ".$tableCollab["customers"]."
							WHERE b_day >= '1' AND b_month >= '$_to_month' AND b_day <= '$_to_day' AND b_month <= '$_to_month'
							ORDER BY b_month, b_day");
		for ($i=0; $i<mysql_num_rows($res); $i++) {
			$sql->fetch();
			$z = $sql->Record;
			$q[count($q)] = $z;
		}
		$sql->close();
	}

	return $q;
}

###################################################################################################################
function Get1BuyCustomers($from, $to) {
	global $tableCollab;

	$q = array();

    $sql = new SQLClass();
	$res = $sql->query("SELECT DISTINCT(customer_id), COUNT(id) AS orders
                        FROM ".$tableCollab["orders"]."
						WHERE status = '5'
						GROUP BY customer_id");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
        if ($z[orders] == 1) {
		    $q[count($q)] = $z[0];
        }
	}
	$sql->close();
    $tmp    =   implode(",", $q);
    unset($q);
    $q = array();

    $sql = new SQLClass();
	$res = $sql->query("SELECT customer_id
                        FROM ".$tableCollab["orders"]."
						WHERE customer_id IN ($tmp) AND time >= '$from' AND time <= '$to' AND status='5'
                        ");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
	    $q[count($q)] = $z[0];
	}
	$sql->close();
    $tmp    =   implode(",", $q);
    unset($q);
    $q = array();

    $sql = new SQLClass();
	$res = $sql->query("SELECT id, email, discount, b_day, b_month FROM ".$tableCollab["customers"]."
						WHERE id IN ($tmp)");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
	    $q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

###################################################################################################################
function GetOrderCountsByCustomer($customer_id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT COUNT(id) FROM ".$tableCollab["orders"]." WHERE customer_id='".$customer_id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z[0];
}

###################################################################################################################
function GetLastOrderDateByCustomer($customer_id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT time FROM ".$tableCollab["orders"]." WHERE customer_id='".$customer_id."' ORDER BY time DESC LIMIT 1");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z[0];
}

###################################################################################################################
function GetCustomerSettings($customer_id) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT varname, value FROM ".$tableCollab["customers_setup"]." WHERE customer_id='".$customer_id."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[$z[varname]] = $z[value];
	}
	$sql->close();

	return $q;
}



#################################################################	���������� ������ ����������
function GetVisVar($vis_id, $varname) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("SELECT value FROM ".$tableCollab["vars"]." WHERE visuniqid='".$vis_id."' AND varname='".$varname."'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();

	return $z[value];
}

#################################################################	���������� ������ ������������
function StoreUserVariable($user_id, $variable, $value) {
	global $tableCollab;

	$value = strip_tags($value);
	$value = stripslashes($value);

	$sql = new SQLClass();
	$sql->query("SELECT COUNT(customer_id) FROM ".$tableCollab["customers_setup"]." WHERE customer_id='".$user_id."' AND varname='".$variable."'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();

	if ($z[0]) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["customers_setup"]." SET value='".addslashes($value)."' WHERE customer_id='".$user_id."' AND varname='".$variable."'");
		$sql->close();
	} else {
		$sql = new SQLClass();
		$sql->query("INSERT INTO ".$tableCollab["customers_setup"]."(customer_id, varname, value) VALUES('".$user_id."', '".$variable."', '".addslashes($value)."')");
		$sql->close();
	}
}


}
?>