<?
class SQLClass extends DB_Sql {
  var $Host     = MYSERVER;
  var $Database = MYDATABASE;
  var $User     = MYLOGIN;
  var $Password = MYPASSWORD;

  var $Auto_Free     = 1;     ## Set to 1 for automatic mysql_free_result()
  var $Debug         = 0;     ## Set to 1 for debugging messages.

	function fetch() {  
	   global $row;

	   $stat = $this->next_record();
	   $row = $this->Record;
	   return $stat;
	}
	
	function close() {
	   $this->free();
	}
}
?>
