<?
class SchoolsClass {

function SchoolsClass() {
}

######################################################	����� ������ �������, � ������� ���� ���������� �����
function GetTowns() {
	global $tableCollab;
	
	$citys = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT DISTINCT (city) FROM ".$tableCollab["schools"]." GROUP BY city");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$citys[count($citys)] = $z[0];
	}
	$sql->close();
	
	$citys = implode(",", $citys);
	
	$q	=	array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["towns"]." WHERE id IN (".$citys.") ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		
		$q[count($q)]	=	$z;
	}
	$sql->close();
	
	return $q;
}


############################################## ����� ������ ������ (id) �� id ������
function GetClubsByCity($city, $metro="", $game="", $price="") {
	global $tableCollab, $MESSAGES;
	
	if (strlen($metro)) $where_metro = "AND metro='".$metro."' "; else $where_metro = "";
	if (strlen($game)) $where_game = "AND ".$MESSAGES["club_games_db"][$game]." > '0' "; else $where_game = "";
	if (strlen($price)) $where_price = "AND price='".$price."' "; else $where_price = "";
	
	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name, metro, address, phone, pool, russian, snooker, carambol, price, discount, worktime, url, email, contact, contact_email, closed, seq, enabled 
						FROM ".$tableCollab["schools"]." 
						WHERE city='".$city."' $where_metro $where_game $where_price ORDER BY seq, name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	
	return $q;
}

############################################## �������� ����
function AddClub($city, $name) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["schools"]."(name, city) VALUES('".$name."', '".$city."')");
	$sql->close();
	
	return mysql_insert_id();
}


############################################## ����� ���� �� id
function GetClubById($id) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$res = $sql->query("SELECT name, metro, address, phone, pool, russian, snooker, carambol, additional, add_room, add_tables, add_menu, add_discount, add_tours, add_timework, add_parking, worktime, price, contact, contact_email, url, email, closed, seq, enabled
						FROM ".$tableCollab["schools"]." 
						WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	
	return $z;
}

######################################################	����� ������ �������
function GetAllTowns() {
	global $tableCollab;
	
	$q	=	array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["towns"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		
		$q[count($q)]	=	$z;
	}
	$sql->close();
	
	return $q;
}

######################################################	����� ������ ������� �����
function GetAllMetro() {
	global $tableCollab;
	
	$q	=	array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["metro"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		
		$q[count($q)]	=	$z;
	}
	$sql->close();
	
	return $q;
}

######################################################	��������� ���������� � ���������� �����
function SaveClub($club_id, $name, $city, $metro, $address, $phone, $worktime, $pool, $russian, $snooker, $carambol, $url, $email, $contact, 
				  $contact_email, $additional, $add_room, $add_tables, $add_menu, $add_discount, $add_tours, $add_timework, $add_parking, $price, $closed, $seq, $enabled) {
	global $tableCollab;
	
	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["schools"]." SET name='".$name."', city='".$city."', metro='".$metro."', address='".$address."', phone='".$phone."', 
				 pool='".$pool."', russian='".$russian."', snooker='".$snooker."', carambol='".$carambol."', worktime='".$worktime."', 
				 additional='".$additional."',  url='".$url."', email='".$email."', contact='".$contact."', contact_email='".$contact_email."', 
				 price='".$price."', closed='".$closed."', seq='".$seq."', enabled='".$enabled."', 
				 add_room='".$add_room."', add_tables='".$add_tables."', add_menu='".$add_menu."', add_discount='".$add_discount."', add_tours='".$add_tours."', 
				 add_timework='".$add_timework."', add_parking='".$add_parking."' 
				 WHERE id='".$club_id."'");
	$sql->close();
}

######################################################	�������� ���������� ����, ������� ����� � ���� �����
function AddTable2Club($club_id, $table_id) {
	global $tableCollab;
	
	$q	=	array();
	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["clubs_tables"]."(club_id, table_id) VALUES ('".$club_id."', '".$table_id."')");
	$sql->close();
	
	return $q;
}

######################################################	����� ������ ������, ������� ����� � ���� �����
function GetTables4Club($club_id) {
	global $tableCollab;
	
	$q	=	array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT table_id FROM ".$tableCollab["clubs_tables"]." WHERE club_id='".$club_id."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		
		$q[count($q)]	=	$z[table_id];
	}
	$sql->close();
	
	return $q;
}

######################################################	������� ���������� ����, ������� ����� � ���� �����
function DelTableAtClub($club_id, $table_id) {
	global $tableCollab;
	
	$q	=	array();
	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["clubs_tables"]." WHERE club_id='".$club_id."' AND table_id='".$table_id."'");
	$sql->close();
	
	return $q;
}

####################################################################################
function AddImageForClub($text_id, $img_dir, $img, $name) {
	$fileinfo = pathinfo($img[name]);
	$extension = $fileinfo[extension];
	if (strlen($name)) $m_name = $name; else $m_name = substr(md5(uniqid(rand(), true)), 0, 5);
	$new_name = $text_id."_".$m_name."_.".$extension;
	copy($img[tmp_name], $img_dir.$new_name);
	chmod($img_dir.$new_name, 0666);
}

####################################################################################
function DelImageForClub($img, $img_dir) {
	unlink($img_dir.$img);
}

####################################################################################
function GetImagesForClub($text_id, $img_dir) {
	
	$q = array();
	$d = dir($img_dir);
	while (false !== ($entry = $d->read())) {
		$s = explode("_", $entry);
		if ($s[0] == $text_id) $q[count($q)] = $entry;
	}
	$d->close();
	
	return $q;
}

}
?>