<?
$MESSAGES["monthes"] = array("", "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");

######################## site
$MESSAGES["site_title"] 		=	"Sportform.ru Back Office";
$MESSAGES["small_name"] 		=	"BO";
$MESSAGES["site_keywords"]		=	"";
$MESSAGES["site_description"]	=	"";

######################## general text

######################## errors messages
$MESSAGES["error_access"]		=	"Нет прав на выполнение этой фукции.";
$MESSAGES["error_logon"]		=	"Не удалось войти. Вы не правильно указали либо логин, либо пароль.";
$MESSAGES["error_password"]		=	"Проверьте правильность ввода пароля.";

$MESSAGES["vendor_noname"]		=	"не указан";

######################### phones
$MESSAGES["phonenumber"]		=	"";
$MESSAGES["phonenumber2"]		=	"";
$MESSAGES["faxnumber"]			=	"";

######################### emails
$MESSAGES["email_general"]		=	"SPORTFORM.RU <xxx@sportform.ru>";
?>