<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();
$cp	=	new CATS_AND_PRODUCTSClass();

$t->set_file(array(
	    "index"		=>	"pr_colors.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

###########################################
if ($what == "save") { 	$cp->SaveProductColor($data->POST["id"], $data->POST["name"], $data->POST["color"]);
	die();
}


$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


###########################################
if ($what == "general") {	$t->set_block("index", "colors", "_colors");
    $t->set_block("index", "colors_add", "_colors_add");
	$colors	    =	$cp->GetProductColors();
    $colors_add =   $cp->GetProductColorsAdd();

	for ($i=0; $i<count($colors); $i++) {
		$t->set_var(array(
			"PR_ID"			=>	$colors[$i]["id"],
			"PR_NAME"		=>	$colors[$i]["name"],
			"PR_COLOR"		=>	$colors[$i]["color"]
		));
		$t->parse("_colors", "colors", true);
	}

    for ($i=0; $i<count($colors_add); $i++) {
		$t->set_var(array(
			"PR_ID_ADD"			=>	$colors_add[$i]["id"],
			"PR_NAME_ADD"		=>	$colors_add[$i]["name"],
			"PR_COLOR_ADD"		=>	$colors_add[$i]["color"]
		));
		$t->parse("_colors_add", "colors_add", true);
	}
    $sql->close();

}





$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
