<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();
$customer = new CustomersClass();

$t->set_file(array(
	    "index"		=>	"cl_lost.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

$actions = array("general", "edit");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


######################################################################################
if ($what == "general") {
	$time	=	time()	-	(60*60)*24*30;

	$users_list	=	$customer->GetCustomers('1', $time);
	$t->set_var("LOST_COUNT", count($users_list));
	$t->set_block("index", "users_list", "_users_list");
	for ($i=0; $i<count($users_list); $i++) {
		if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";
		if ($users_list[$i][lastlogin]) $last_login	=	date("d-m-Y H:i", $users_list[$i][lastlogin]); else $last_login = "";
		if ($users_list[$i][status]) $status_color = "#047b2e"; else $status_color = "#a40a0a";
		$t->set_var(array(
			"BGCOLOR"			=>	$bg_color,
			"USER_ID"			=>	$users_list[$i][id],
			"USER_LOGIN"		=>	$users_list[$i][login],
			"USER_COMPANY"		=>	$users_list[$i][company],
			"USER_CITY"			=>	$users_list[$i][city],
			"USER_USER"			=>	$users_list[$i][user_],
			"USER_PHONE"		=>	$users_list[$i][phone],
			"USER_EMAIL"		=>	$users_list[$i][email],
			"USER_TIME"			=>	date("d-m-Y H:i", $users_list[$i][time]),
			"USER_LAST_LOGIN"	=>	$last_login,
			"USER_STATUS_COLOR"	=>	$status_color
		));
		$t->parse("_users_list", "users_list", true);
	}

}


######################################################################################
if ($what == "edit") {		$user_info		=		$customer->GetCustomerByID($data->GET["uid"]);
		if ($user_info[status]) $status = "checked"; else $status = "";
		if ($user_info[newsletter]) $newsletter = "checked"; else $newsletter = "";
		if ($user_info[cash_payment]) $cash_payment = "checked"; else $cash_payment = "";
		$user_info[company] = eregi_replace("\"([^\"]*)\"" , "«\\1»", $user_info[company]);
		$user_info[company] = eregi_replace("\&quot;([^\"]*)\&quot;" , "«\\1»", $user_info[company]);
		$user_info[company] = stripcslashes($user_info[company]);
		$user_info[company] = stripcslashes($user_info[company]);
		$t->set_var(array(
			"USER_ID"				=>	$user_info[id],
			"USER_LOGIN"			=>	$user_info[login],
			"USER_PASSWORD"			=>	$user_info[password],
			"USER_COMPANY"			=>	$user_info[company],
			"USER_CITY"				=>	$user_info[city],
			"USER_ADDRESS"			=>	$user_info[address],
			"USER_POST_ADDRESS"		=>	$user_info[post_address],
			"USER_PHONE"			=>	$user_info[phone],
			"USER_FAX"				=>	$user_info[fax],
			"USER_EMAIL"			=>	$user_info[email],
			"USER_ICQ"				=>	$user_info[icq],
			"USER_SKYPE"			=>	$user_info[skype],
			"USER_SITE"				=>	$user_info[site],
			"USER_USER"				=>	$user_info[user_],
			"USER_USER_FIN"			=>	$user_info[user_fin],
			"USER_USER_BOSS"		=>	$user_info[user_boss],
			"USER_WHOIS"			=>	$user_info[whois],
			"USER_CASHPAYMENT"		=>	$cash_payment,
			"USER_NEWSLETTER"		=>	$newsletter,
			"USER_STATUS"			=>	$status
		));}




$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
