<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"pr_products.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

$cp	=	new CATS_AND_PRODUCTSClass();

#############################################################################################################
if ($what == "del_prop2prod") {
	$cp->DelProductProperty($data->GET["pid"], $data->GET["delp2p"]);
	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->GET["cid"]."&page=".$data->GET["page"]."&pid=".$data->GET["pid"]."&url=".$data->GET["url"]);
	die();
}

#############################################################################################################
if (($what == "up")||($what == "down")) {
	$cp->UpdatePropertySeq($data->GET["pid"], $data->GET["prop"], $what);
	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->GET["cid"]."&page=".$data->GET["page"]."&pid=".$data->GET["pid"]."&url=".$data->GET["url"]);
	die();
}

#############################################################################################################
if ($what == "save_prop2prod") {
	$data->POST["add_value"]	=	trim($data->POST["add_value"]);
	if ($data->POST["add_prop"]	==	"new_property") {		//	���������� ������ ��������
		if (strlen($data->POST["add_value"])) $cp->AddProperty($data->POST["add_value"]);
	}

	if (is_numeric($data->POST["add_prop"])) {					//	���������� ������ �������� �� ��������� ��� ����� ������
		if (strlen($data->POST["add_value"])) {
			$cp->AddPropertyValue($data->POST["pid"], $data->POST["add_prop"], $data->POST["add_value"]);
		}
	}

	$ids		=	$data->POST["ids"];
	$prop_value	=	$data->POST["prop_value"];
	for ($i=0; $i<count($ids); $i++) {
		$cp->SavePropertyValue($data->POST["pid"], $ids[$i], $prop_value[$i]);
	}

	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->POST["cid"]."&page=".$data->POST["page"]."&pid=".$data->POST["pid"]."&url=".$data->POST["url"]);
	die();
}

#############################################################################################################
if ($what == "add_product") {
	$new_name = trim($data->POST["new_name"]);
	if (strlen($new_name)) {
		$pid = $cp->AddProduct($data->POST["cid"], $new_name);
		header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->POST["cid"]."&page=".$data->POST["page"]."&pid=".$pid);
	} else {
		header("Location: ".$links[pr_products]."?cid=".$data->POST["cid"]."&page=".$data->POST["page"]);
	}
	die();
}

#############################################################################################################
if ($what == "save_products") {
	$ids 		= $data->POST["ids"];
	$price 		= $data->POST["price"];
	$price2		= $data->POST["price2"];
	$warehouse	= $data->POST["warehouse"];
	$seq		= $data->POST["seq"];
	$enabled	= $data->POST["enabled"];
	for ($i=0; $i<count($ids); $i++) {
		$cp->UpdateProductGeneralInfo($ids[$i], $price[$i], $price2[$i], $enabled[$i], $warehouse[$i], $seq[$i]);
	}
	header("Location: ".$links[pr_products]."?cid=".$data->POST["cid"]."&page=".$data->POST["page"]);
	die();
}

#############################################################################################################
if ($what == "save") {
	$data->POST["descr"]	=	str_replace("&lt;", "<", $data->POST["descr"]);
	$data->POST["descr"]	=	str_replace("&gt;", ">", $data->POST["descr"]);

	$data->POST["full_descr"]	=	str_replace("&lt;", "<", $data->POST["full_descr"]);
	$data->POST["full_descr"]	=	str_replace("&gt;", ">", $data->POST["full_descr"]);


	$cp->UpdateProductFullInfo($data->POST["pid"], $data->POST["name"], $data->POST["vendor"], $data->POST["purl"], $data->POST["descr"],
	$data->POST["full_descr"], $data->POST["price"], $data->POST["special"], $data->POST["enabled"], $data->POST["page_descr"],
	$data->POST["page_keywords"], $data->POST["code"], $data->POST["prcode"], $data->POST["youtube"], $data->POST["for_unit"], $data->POST["units"],
	$data->POST["f_new"], $data->POST["f_hot"], $data->POST["f_popular"], $data->POST["price2"], $data->POST["for_unit2"], $data->POST["size_chart"], $data->POST["color"]);

	if (strlen($data->POST["url"])) {
		header("Location: ".base64_decode($data->POST["url"]));
	} else {
		header("Location: ".$links[pr_products]."?cid=".$data->POST["cid"]."&page=".$data->POST["page"]);
	}
	die();
}

#############################################################################################################
if ($what == "delbt") {
	$cp->DelBuyTogetherID($data->GET["pid"]);
	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->GET["cid"]."&page=".$data->GET["page"]."&pid=".$data->GET["pid"]);
	die();
}

#############################################################################################################
if ($what == "del_p2p") {
	$cp->DelFromP2P($data->GET["pid"], $data->GET["delp2p"]);
	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->GET["cid"]."&page=".$data->GET["page"]."&pid=".$data->GET["pid"]);
	die();
}

#############################################################################################################
if ($what == "del_p2alt") {
	$cp->DelFromP2ALT($data->GET["pid"], $data->GET["delp2p"]);
	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->GET["cid"]."&page=".$data->GET["page"]."&pid=".$data->GET["pid"]);
	die();
}

#############################################################################################################
if ($what == "del_in") {
	$cp->DelFromP2ARTICLE($data->GET["pid"], $data->GET["del_article"]);
	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->GET["cid"]."&page=".$data->GET["page"]."&pid=".$data->GET["pid"]);
	die();
}

#############################################################################################################
if ($what == "add_pics") {
	if ($_FILES['pic_small'][size]) $cp->AddImage($data->POST["pid"], $_default_images_dir_products, $_FILES['pic_small'], time());

	if ($_FILES['pic_main'][size]) $cp->AddImage($data->POST["pid"], $_default_images_dir_products, $_FILES['pic_main'], "main");

	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->POST["cid"]."&page=".$data->POST["page"]."&pid=".$data->POST["pid"]);
	die();
}

#############################################################################################################
if ($what == "del_pic") {
	$cp->DelImage($data->GET["pic"], $_default_images_dir_products);
	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->GET["cid"]."&page=".$data->GET["page"]."&pid=".$data->GET["pid"]);
	die();
}

#############################################################################################################
if ($what == "del_cat") {
	$cp->DelCatOfProduct($data->GET["pid"], $data->GET["delcid"]);
	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->GET["cid"]."&page=".$data->GET["page"]."&pid=".$data->GET["pid"]);
	die();
}

#############################################################################################################
if ($what == "add_cat") {
	$cp->AddCatOfProduct($data->GET["pid"], $data->GET["newcat"]);
	header("Location: ".$links[pr_products]."?what=edit_product&cid=".$data->GET["cid"]."&page=".$data->GET["page"]."&pid=".$data->GET["pid"]);
	die();
}

$actions = array("general", "edit_product", "p2p");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");

if (!$data->GET["cid"]) {
	$blocks->HideBlock("index", "path");
} else {
	$t->set_block("index", "path_rows", "_path_rows");
	$path = $cp->GetPathFromCID($data->GET["cid"]);
	for ($i=count($path)-1; $i>-1; $i--) {
		$t->set_var(array(
				"PATH_CID"			=>	@$path[$i][id],
				"PATH_NAME"			=>	@$path[$i][short_name]
	    ));
		$t->parse("_path_rows", "path_rows", true);
	}
	unset($path);
}

$t->set_var(array(
	"PAGE"				=>	$data->GET["page"],
	"CID"				=>	$data->GET["cid"],
	"PID"				=>	$data->GET["pid"],
	"URL"				=>	$data->GET["url"],
	"CURRENT_PAGE_URL"	=>	base64_encode($_SERVER["REQUEST_URI"])
));

#################################################################################################################################
if ($what == "general") {
	$t->set_block("index", "cats_list", "_cats_list");
    if($data->GET["cid"]=="")
        $data->GET["cid"] = 177;
	$cats = $cp->GetSimpleCatsList($data->GET["cid"]);

	if (!count($cats)) $blocks->HideBlock("index", "cats");
	for ($i=0; $i<count($cats); $i++) {
		if (($i/2) == round($i/2)) $bg_color = "#f5f5f5"; else $bg_color = "#eaeaea";
		$t->set_var(array(
			"COLOR"			=>	$bg_color,
			"CAT_NAME"		=>	$cats[$i][name],
			"CAT_ENABLED"	=>	$cats[$i][enabled],
			"CAT_ID"		=>	$cats[$i][id],
		));
		$t->parse("_cats_list", "cats_list", true);
	}

	$is_products = $cp->GetProductsCountInCID($data->GET["cid"], "", "");
	if ($is_products) {
		$prs = $cp->GetProductsByCID($data->GET["cid"], null, null, $data->GET["page"]*$_default_products_per_page, $_default_products_per_page, "seq");
		$t->set_block("index", "pr_list", "_pr_list");
		for ($i=0; $i<count($prs); $i++) {

			if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";

			if ($prs[$i][enabled]) {
				$product_enabled		=	"checked";
				$product_enabled_color 	= 	"#047b2e";
			} else {
				$product_enabled		=	"";
				$product_enabled_color 	= 	"#a40a0a";
			}

			$t->set_var(array(
				"I"					=>	$i,
				"COLOR"				=>	$bg_color,
				"PR_COLOR"			=>	$product_enabled_color,
				"PR_ENABLED"		=>	$product_enabled,
				"PR_NAME"			=>	$prs[$i][name],
				"PR_VENDOR_NAME"	=>	$prs[$i][vendor_name],
				"PR_PRICE"			=>	$prs[$i][price],
				"PR_PRICE2"			=>	$prs[$i][price2],
				"PR_WAREHOUSE"		=>	$prs[$i][warehouse],
				"PR_SEQ"			=>	$prs[$i][seq],
				"PR_ID"				=>	$prs[$i][id],
				"PR_BUYTOGETHER"	=>	$buytogether,
				"PR_P2P"			=>	$p2p
			));
			$t->parse("_pr_list", "pr_list", true);
		}
	} else {
		$blocks->HideBlock("index", "products");
	}

	############################################################# ��������
	$pages = $is_products/$_default_products_per_page;
	$t->set_block("index", "news_pages", "_news_pages");
	if ($pages<=1) $t->set_var("news_pages", "");
	if (!$pages) $t->set_var("_news_pages", "");
	for ($i=0; $i<$pages; $i++) {
		$start = ($i * $_default_products_per_page) + 1;
		$finish = ($i * $_default_products_per_page) + $_default_products_per_page;
		if ($finish>$is_products) $finish = $is_products;
		if ($i == $data->GET["page"]) $tmp = "<b>[".$start."-".$finish."]</b>"; else $tmp = "[".$start."-".$finish."]";
		$t->set_var(array(
				"PR_PAGE"		=>	$i,
				"PR_PAGE_NAME"	=>	$tmp
	    ));
		$t->parse("_news_pages", "news_pages", true);
	}
	#############################################################

	unset($is_products);
}

#################################################################################################################################
if ($what == "edit_product") {
	$pr = $cp->GetProductByID($data->GET["pid"]);
	if ($pr[enabled]) $enabled = "checked"; else $enabled = "";

	if ($pr[f_new]) $f_new = "checked"; else $f_new = "";
	if ($pr[f_hot]) $f_hot = "checked"; else $f_hot = "";
	if ($pr[f_popular]) $f_popular = "checked"; else $f_popular = "";

	if ($pr[fastdelivery]) $fastdelivery = "checked"; else $fastdelivery = "";
	if ($pr[freeshipping]) $freeshipping = "checked"; else $freeshipping = "";
	//$pr[name] = preg_replace("\"([^\"]*)\"" , "�\\1�", $pr[name]);
	//$pr[name] = preg_replace("\&quot;([^\"]*)\&quot;" , "�\\1�", $pr[name]);
	$pr[name] = stripcslashes($pr[name]);
	$pr[name] = stripcslashes($pr[name]);
	$t->set_var(array(
		"PR_NAME"			=>	$pr[name],
		"PR_URL"			=>	$pr[url],
		"PR_CODE"			=>	$pr[code],
		"PR_PRCODE"			=>	$pr[prcode],
		"PR_WEIGHT"			=>	$pr[weight],
		"PR_PRICE"			=>	$pr[price],
		"PR_SPECIAL"		=>	$pr[old_price],
		"PR_FOR_UNIT"		=>	$pr[for_unit],
		"PR_PRICE2"			=>	$pr[price2],
		"PR_FOR_UNIT2"		=>	$pr[for_unit2],
		"PR_DESCR"			=>	$pr[descr],
		"PR_FULL_DESCR"		=>	$pr[full_descr],
		"PR_PAGE_DESCR"		=>	$pr[page_descr],
		"PR_PAGE_KEYWORDS"	=>	$pr[page_keywords],
		"PR_YOUTUBE"		=>	$pr[youtube],
		"PR_ENABLED"		=>	$enabled,
		"PR_F_NEW"			=>	$f_new,
		"PR_F_HOT"			=>	$f_hot,
		"PR_F_POPULAR"		=>	$f_popular,
		"PR_FASTDELIVERY"	=>	$fastdelivery,
		"PR_FREESHIPPING"	=>	$freeshipping
	));

	######################################################## �������������
	$vendors = $cp->GetVendorsList();
	$t->set_block("index", "vendors_list", "_vendors_list");
	for ($i=0; $i<count($vendors); $i++) {
		if ($vendors[$i][id] == $pr[vendor]) $vendor_selected = "selected"; else $vendor_selected = "";
		$t->set_var(array(
			"VENDOR_ID"			=>	$vendors[$i][id],
			"VENDOR_NAME"		=>	$vendors[$i][name],
			"VENDOR_SELECTED"	=>	$vendor_selected
		));
		$t->parse("_vendors_list", "vendors_list", true);
	}
	unset($vendors);
	######################################################## �������������

	######################################################## ������� �����
	$colors_list			=	$cp->GetProductColorsArray();
	$t->set_block("index", "p_color_list", "_p_color_list");
	for ($i=0; $i<count($colors_list); $i++) {
		$color	=	$cp->GetProductColor($colors_list[$i]);
		if ($colors_list[$i] == $pr[color]) $vendor_selected = "selected"; else $vendor_selected = "";
		$text_color	=	"#".dechex(16777215 - hexdec($color[color]));
		$t->set_var(array(
			"P_COLOR_ID"		=>	$color[id],
			"P_COLOR_NAME"		=>	$color[name],
			"P_COLOR_CODE"		=>	$color[color],
			"P_COLOR_TEXT"		=>	$text_color,
			"P_COLOR_SELECTED"	=>	$vendor_selected
		));
		$t->parse("_p_color_list", "p_color_list", true);
	}
	unset($vendors);
	######################################################## // ������� �����

	######################################################## ������ ��������
	$size_groups = $cp->GetSizeGroups();
	$t->set_block("index", "size_list", "_size_list");
	for ($i=0; $i<count($size_groups); $i++) {
		if ($size_groups[$i][id] == $pr[size_chart]) $vendor_selected = "selected"; else $vendor_selected = "";
		$t->set_var(array(
			"SIZE_LINE_ID"			=>	$size_groups[$i][id],
			"SIZE_LINE_NAME"		=>	$size_groups[$i][name],
			"SIZE_LINE_SELECTED"	=>	$vendor_selected
		));
		$t->parse("_size_list", "size_list", true);
	}
	unset($vendors);
	######################################################## //	������ ��������

	######################################################## ������� ���������
	$units = $cp->GetUnitsList();
	$t->set_block("index", "units_list", "_units_list");
	for ($i=0; $i<count($units); $i++) {
		if ($units[$i][id] == $pr[units]) $units_selected = "selected"; else $units_selected = "";
		$t->set_var(array(
			"UNIT_ID"			=>	$units[$i][id],
			"UNIT_NAME"			=>	$units[$i][name],
			"UNIT_SELECTED"		=>	$units_selected
		));
		$t->parse("_units_list", "units_list", true);
	}
	unset($units);
	######################################################## // ������� ���������

	######################################################## ������ �������
	$t->set_block("index", "properties_list", "_properties_list");
	$prop_list	=	$cp->GetProperties();
	for ($i=0; $i<count($prop_list); $i++) {
		$t->set_var(array(
			"PROPERTY_ID"		=>	$prop_list[$i][id],
			"PROPERTY_NAME"		=>	$prop_list[$i][name]
		));
		$t->parse("_properties_list", "properties_list", true);
	}
	unset($prop_list);
	######################################################## ������ �������

	######################################################## ������ ������� ��� ����� ������
	$t->set_block("index", "prop2prod_list", "_prop2prod_list");
	$prop_list	=	$cp->GetPropertiesValues($data->GET["pid"]);
	if (!count($prop_list)) $t->set_var("_prop2prod_list", "");
	for ($i=0; $i<count($prop_list); $i++) {
		if (($i/2) == round($i/2)) $bgcolor = "#fdfdfd"; else $bgcolor = "#eaeaea";
		$t->set_var(array(
			"COLOR"				=>	$bgcolor,
			"PROPERTY_ID"		=>	$prop_list[$i][property_id],
			"PROPERTY_NAME"		=>	$prop_list[$i][property_name],
			"PROPERTY_VALUE"	=>	$prop_list[$i][value],
			"I"					=>	$i
		));
		$t->parse("_prop2prod_list", "prop2prod_list", true);
	}
	unset($prop_list);
	######################################################## ������ ������� ��� ����� ������

	######################################################## ������������
	$p2pids = $cp->GetP2ALT($data->GET["pid"]);
	$t->set_block("index", "p2alternative_list", "_p2alternative_list");
	if (!count($p2pids)) $t->set_var("_p2alternative_list", "");
	for ($i=0; $i<count($p2pids); $i++) {
		if (($i/2) == round($i/2)) $bg_color = "#f5f5f5"; else $bg_color = "#eaeaea";
		$pr = $cp->GetProductByID($p2pids[$i]);
		if ($pr[special]>0) {
			$product_name = "<b>".$pr[name]."</b>";
			$product_special = number_format($pr[special], 2);
		} else {
			$product_name = $pr[name];
			$product_special = "";
		}
		$t->set_var(array(
			"COLOR"				=>	$bg_color,
			"P2ALT_NAME"			=>	$product_name,
			"P2ALT_VENDOR_NAME"	=>	$pr[vendor_name],
			"P2ALT_PRICE"			=>	number_format($pr[price], 2),
			"P2ALT_SPECIAL"		=>	$product_special,
			"P2ALT_ID"			=>	$pr[id]
		));
		$t->parse("_p2alternative_list", "p2alternative_list", true);
	}
	######################################################## // ������������

	######################################################## ������ ��������
	$pic_main 	= $cp->GetImageForPID($_default_images_dir_products, "main", $data->GET["pid"]);
	$pic_main_name = pathinfo($pic_main);

	$t->set_var(array(
		"PHOTO_URL_MAIN"		=>	$pic_main,
		"PHOTO_URL_MAIN_NAME"	=>	$pic_main_name[basename]
	));

	$pics	=	$cp->GetImagesForPID($_default_images_dir_products, $data->GET["pid"], 0);
	$t->set_block("index", "photos_list", "_photos_list");
	if (!count($pics)) $t->set_var("_photos_list", "");
	for ($i=0; $i<count($pics); $i++) {		$tmp	=	pathinfo($pics[$i]);		$t->set_var(array(
			"PHOTO_URL_NAME"	=>	$tmp[basename],
			"PHOTO_URL"			=>	$pics[$i]
		));		$t->parse("_photos_list", "photos_list", true);	}
	######################################################## ������ ��������

	######################################################## � ����� ���������� ��������� �����
	$incats	=	$cp->GetCatsOfProduct($data->GET["pid"]);
	$t->set_block("index", "in_cats", "_in_cats");
	$t->set_block("in_cats", "incat_rows", "_incat_rows");
	if (!count($incats)) $t->set_var("_in_list", "");
	for ($i=0; $i<count($incats); $i++) {

		$path = $cp->GetPathFromCID($incats[$i]);
		for ($i0=count($path)-1; $i0>-1; $i0--) {
			$t->set_var(array(
				"INCAT_NAME"			=>	@$path[$i0][short_name]
		    ));
			$t->parse("_incat_rows", "incat_rows", true);
		}
		unset($path);

		$t->set_var(array(
			"INCAT_ID"			=>	$incats[$i]
	    ));

		$t->parse("_in_cats", "in_cats", true);
		$t->set_var("_incat_rows", "");
	}
	unset($incats);

	$tree	=	$cp->GetProductsTree(0, $tree, 1);
	$t->set_block("index", "new_cat", "_new_cat");
	for ($i=0; $i<count($tree); $i++) {
		$tmp	=	$cp->IsThisChildCat($tree[$i][cat_id]);
		if (!$tmp) $tab	=	""; else $tab	=	"&nbsp;&nbsp;";
		if (!$cp->IsThisParentCat($tree[$i][cat_id])) $tab .= "&nbsp;&nbsp;";
		$t->set_var(array(
			"NEWCAT_ID"				=>	$tree[$i][cat_id],
			"NEWCAT_NAME"			=>	$tab.$tree[$i][cat_name]
	    ));
		$t->parse("_new_cat", "new_cat", true);
	}
	unset($tree);
	######################################################## � ����� ���������� ��������� �����

	unset($pr);
}





$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
