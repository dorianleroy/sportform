<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"pr_cats.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

if (!$data->GET["cid"]) $data->GET["cid"]	=	0;

$cp	=	new CATS_AND_PRODUCTSClass();

#############################################################################################################
if ($what == "add_cat") {	$data->POST["new_url"]			=	str_replace(" ", "_", $data->POST["new_url"]);
	$data->POST["new_shortname"]	=	str_replace("&quot;", "\"", $data->POST["new_shortname"]);
	$data->POST["new_name"]			=	str_replace("&quot;", "\"", $data->POST["new_name"]);

	$cp->AddCat($data->POST["cid"], $data->POST["new_name"], $data->POST["new_shortname"], $data->POST["new_url"]);
	header("Location: ".$links[pr_cats]."?cid=".$data->POST["cid"]);
	die();
}

################################################################################
if ( ($what == "up")||($what == "down") ) {
	$cp->UpdateCatSeq($data->GET["cid"], $data->GET["pid"], $what);
	header("Location: ".$links[pr_cats]."?cid=".$data->GET["cid"]);
	die();
}

#############################################################################################################
if ($what == "save") {	$data->POST["url"]			=	str_replace(" ", "_", $data->POST["url"]);
	$data->POST["short_name"]	=	str_replace("&quot;", "\"", $data->POST["short_name"]);
	$data->POST["name"]			=	str_replace("&quot;", "\"", $data->POST["name"]);
	$cp->UpdateCat($data->POST["cid"], $data->POST["short_name"], $data->POST["name"], $data->POST["enabled"], $data->POST["url"], $data->POST["_desc"], $data->POST["_keywords"]);
	header("Location: ".$links[pr_cats]."?cid=".$data->POST["pid"]);
	die();
}

#############################################################################################################
if ($what == "del_p2c") {
	$cp->DelFromP2C($data->GET["cat"], $data->GET["delp2c"]);
	header("Location: ".$links[pr_cats]."?what=edit&cid=".$data->GET["cid"]."&cat=".$data->GET["cat"]);
	die();
}


$actions = array("general", "edit");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


if (!$data->GET["cid"]) {
	$blocks->HideBlock("index", "path");
} else {
	$t->set_block("index", "path_rows", "_path_rows");
	$path = $cp->GetPathFromCID($data->GET["cid"]);
	for ($i=count($path)-1; $i>-1; $i--) {
		$t->set_var(array(
				"PATH_CID"			=>	@$path[$i][id],
				"PATH_NAME"			=>	@$path[$i][short_name]
	    ));
		$t->parse("_path_rows", "path_rows", true);
	}
	unset($path);
}

$t->set_var(array(
	"PAGE"			=>	$data->GET["page"],
	"CUR_CID"		=>	$data->GET["cid"],
	"PID"			=>	$data->GET["pid"],
	"CAT"			=>	$data->GET["cat"]
));


###########################################
if ($what == "general") {
	$t->set_block("index", "cats_list", "_cats_list");
//    var_dump($data->GET['cid']);
	$cats = $cp->GetSimpleCatsList(177);
//    echo "<pre>",var_dump($cats),"</pre>";
	if (!count($cats)) $blocks->HideBlock("index", "cats");
	for ($i=0; $i<count($cats); $i++) {
		if (($i/2) == round($i/2)) $bg_color = "#f5f5f5"; else $bg_color = "#eaeaea";
		$t->set_var(array(
			"COLOR"				=>	$bg_color,
			"CAT_NAME"			=>	$cats[$i][name],
			"CID"				=>	$cats[$i][id],
			"CAT_SHORT_NAME"	=>	$cats[$i][short_name],
			"CAT_URL"			=>	$cats[$i][url],
			"CAT_ENABLED"		=>	$cats[$i][enabled],
		));
		$t->parse("_cats_list", "cats_list", true);
	}

	$t->set_var(array(
		"CUR_CID"				=>	$data->GET["cid"]
	));
}


###########################################
if ($what == "edit") {

	$this_cat	=	$cp->GetCatByID($data->GET["cid"]);

	if ($this_cat[enabled]) $enabled = "checked"; else $enabled = "";

	$t->set_var(array(
		"CID"				=>	$data->GET["cid"],
		"PID"				=>	$data->GET["pid"],
		"PAGE"				=>	$data->GET["page"],
		"URL"				=>	$data->GET["url"],
		"CAT_SHORT_NAME"	=>	$this_cat[short_name],
		"CAT_NAME"			=>	$this_cat[name],
		"CAT_DESC"			=>	$this_cat[_desc],
		"CAT_KEYWORDS"		=>	$this_cat[_keywords],
		"CAT_URL"			=>	$this_cat[url],
		"CAT_ENABLED"		=>	$enabled
	));

	unset($this_cat);
}




$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
