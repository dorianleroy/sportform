<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"pr_urls.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

$cp	=	new CATS_AND_PRODUCTSClass();

$t->set_var(array(
	"URL_SELF"		=>	$_SERVER["PHP_SELF"]
));

#############################################################################################################
if ($what == "save_products") {
	$ids 		= $data->POST["ids"];
	$url 		= $data->POST["url"];
	$enabled	= $data->POST["enabled"];
	for ($i=0; $i<count($ids); $i++) {
		$cp->UpdateProductUrl($ids[$i], $url[$i], $enabled[$i]);
	}
	header("Location: ".$_SERVER["PHP_SELF"]."?cid=".$data->POST["cid"]."&page=".$data->POST["page"]);
	die();
}

$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");

if (!$data->GET["cid"]) {
	$blocks->HideBlock("index", "path");
} else {
	$t->set_block("index", "path_rows", "_path_rows");
	$path = $cp->GetPathFromCID($data->GET["cid"]);
	for ($i=count($path)-1; $i>-1; $i--) {
		$t->set_var(array(
				"PATH_CID"			=>	@$path[$i][id],
				"PATH_NAME"			=>	@$path[$i][short_name]
	    ));
		$t->parse("_path_rows", "path_rows", true);
	}
	unset($path);
}

$t->set_var(array(
	"PAGE"		=>	$data->GET["page"],
	"CID"		=>	$data->GET["cid"],
	"PID"		=>	$data->GET["pid"],
	"URL"		=>	$data->GET["url"]
));

#################################################################################################################################
if ($what == "general") {
	$t->set_block("index", "cats_list", "_cats_list");
    if($data->GET["cid"]=="")
            $data->GET["cid"] = 177;
	$cats = $cp->GetSimpleCatsList($data->GET["cid"]);
	if (!count($cats)) $blocks->HideBlock("index", "cats");
	for ($i=0; $i<count($cats); $i++) {
		if (($i/2) == round($i/2)) $bg_color = "#f5f5f5"; else $bg_color = "#eaeaea";
		$t->set_var(array(
			"COLOR"			=>	$bg_color,
			"CAT_NAME"		=>	$cats[$i][name],
			"CAT_ENABLED"	=>	$cats[$i][enabled],
			"CAT_ID"		=>	$cats[$i][id],
		));
		$t->parse("_cats_list", "cats_list", true);
	}

	$is_products = $cp->GetProductsCountInCID($data->GET["cid"], "", "");
	if ($is_products) {
		$prs = $cp->GetProductsByCID($data->GET["cid"], null, null, $data->GET["page"]*$_default_products_per_page, $_default_products_per_page, "seq");
		$t->set_block("index", "pr_list", "_pr_list");
		for ($i=0; $i<count($prs); $i++) {

			if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";

			if ($prs[$i][enabled]) {
				$product_enabled		=	"checked";
				$product_enabled_color 	= 	"#047b2e";
			} else {
				$product_enabled		=	"";
				$product_enabled_color 	= 	"#a40a0a";
			}

			$t->set_var(array(
				"I"					=>	$i,
				"COLOR"				=>	$bg_color,
				"PR_COLOR"			=>	$product_enabled_color,
				"PR_ENABLED"		=>	$product_enabled,
				"PR_NAME"			=>	$prs[$i][name],
				"PR_URL"			=>	$prs[$i][url],
				"PR_ID"				=>	$prs[$i][id]
			));
			$t->parse("_pr_list", "pr_list", true);
		}
	} else {
		$blocks->HideBlock("index", "products");
	}

	############################################################# страницы
	$pages = $is_products/$_default_products_per_page;
	$t->set_block("index", "news_pages", "_news_pages");
	if ($pages<=1) $t->set_var("news_pages", "");
	if (!$pages) $t->set_var("_news_pages", "");
	for ($i=0; $i<$pages; $i++) {
		$start = ($i * $_default_products_per_page) + 1;
		$finish = ($i * $_default_products_per_page) + $_default_products_per_page;
		if ($finish>$is_products) $finish = $is_products;
		if ($i == $data->GET["page"]) $tmp = "<b>[".$start."-".$finish."]</b>"; else $tmp = "[".$start."-".$finish."]";
		$t->set_var(array(
				"PR_PAGE"		=>	$i,
				"PR_PAGE_NAME"	=>	$tmp
	    ));
		$t->parse("_news_pages", "news_pages", true);
	}
	#############################################################

	unset($is_products);
}



$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
