<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"st_order_items.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];


$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");

$cp 		= 	new CATS_AND_PRODUCTSClass();
$orders		=	new OrdersClass();


########################################### ФОРМА С ВХОДОМ В СИСТЕМУ
if ($what == "general") {
	if (!$data->GET["start"]) $data->GET["start"]	=	date("d-m-Y");
	if (!$data->GET["finish"]) $data->GET["finish"]	=	date("d-m-Y");

	$t->set_var(array(
		"START"		=>	$data->GET["start"],
		"FINISH"	=>	$data->GET["finish"]
	));

 	$start	=	strtotime($data->GET["start"]);
 	$finish	=	strtotime($data->GET["finish"]) + (60*60*23) + (60*59);

	$sql = new SQLClass();
	$res = $sql->query("SELECT A.id, A.invoice, A.time, A.status, A.payment_id, A.user_id, A.delivery_id, A.manager_id, A.manager_time
						FROM ".$tableCollab["orders"]." AS A
						WHERE A.time >='".$start."' AND A.time <='".$finish."'
					   ");
	$order_count		=	mysql_num_rows($res);
	$order_sum			=	0;
	$order_items		=	array();
	$order_items_sum	=	array();
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$order_sum		+=	$orders->GetOrderValue($z[id]);

		$items	=	$orders->GetOrderItems($z[id]);

		for ($i0=0; $i0<count($items); $i0++) {			$order_items[$items[$i0]['product_id']] += $items[$i0]['count'];
			$order_items_sum[$items[$i0]['product_id']] += ($items[$i0]['count'] * $items[$i0]['price']);
	  	}
	}
	$sql->close();

	arsort($order_items);

	$t->set_block("index", "cats_view", "_cats_view");
	$_i	=	0;
	foreach ($order_items as $key => $val) {		if (($_i/2) == round($_i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";

		$pr = $cp->GetProductByID($key);

		$t->set_var(array(
				"I"					=>	$i,
				"BGCOLOR"			=>	$bg_color,
				"CV_COUNT"			=>	$val,
				"CV_SUM"			=>	number_format($order_items_sum[$key], 2, ",", " "),
				"CV_NAME"			=>	$pr[name],
				"CV_WIDTH"			=>	round($val * 1.5)
		));

		$t->parse("_cats_view", "cats_view", true);

		$_i++;	}

	$t->set_var(array(
		"ORDER_COUNT"		=>	$order_count,
		"ORDER_SUM"			=>	number_format($order_sum, 2, ",", " ")
	));
}





$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
