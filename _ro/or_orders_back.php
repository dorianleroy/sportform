<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();
$orders		=	new OrdersClass();
$customer	=	new CustomersClass();
$cp			=	new CATS_AND_PRODUCTSClass();

$t->set_file(array(
	    "index"		=>	"or_orders.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

if ($what == "exit") {	header("Location: ".$links[or_orders]."?page=".$data->POST["page"]."&find=".$data->POST["find"]."&customer=".$data->POST["customer"]."&time=".time());}

if ($what == "save") {  	$orders->UpdateOrderStatus($data->POST["oid"], $data->POST["status"], $data->POST["invoice"]);
	if ($data->POST["exit"]) {		header("Location: ".$links[or_orders]."?page=".$data->POST["page"]."&find=".$data->POST["find"]."&customer=".$data->POST["customer"]."&time=".time());	} else {		header("Location: ".$links[or_orders]."?what=edit&oid=".$data->POST["oid"]."&page=".$data->POST["page"]."&find=".$data->POST["find"]."&customer=".$data->POST["customer"]."&time=".time());	}
	die();}

$actions = array("general", "edit", "print");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


########################################### ФОРМА С ВХОДОМ В СИСТЕМУ
if ($what == "general") {	$t->set_var(array(
		"PAGE"				=>	$data->GET["page"],
		"FIND"				=>	$data->GET["find"],
		"CUSTOMER_ID"		=>	$data->GET["customer"],
		"META_ADDITIONAL"	=>	"<meta http-equiv=\"refresh\" content=\"180; URL=".$links[or_orders]."?rnd=\">",
	));

	$orders_list = $orders->GetOrdersList( ($data->GET["page"]*$_default_orders_per_page), $_default_orders_per_page, $data->GET["customer"], $data->GET["find"]);
	$orders_statuses = $orders->GetOrdersStatuses();

 	$t->set_block("index", "orders_list", "_orders_list");
  	for ($i=0; $i<count($orders_list); $i++) {		if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";
		if ($orders_list[$i][status] != 3) $bg_color = $_default_orders_colors[$orders_list[$i][status]];
		$user_info		=		$customer->GetCustomerByID($orders_list[$i][user_id]);
		if ($orders_list[$i][manager_id]) {			$manager_name	=	$users->GetNameById($orders_list[$i][manager_id])."<br>";			$manager_time	=	date("d-m-Y H:i", $orders_list[$i][manager_time]);		} else {			$manager_name	=	"";
			$manager_time	=	"";		}  		$t->set_var(array(
     		"BGCOLOR"				=>	$bg_color,
  			"ORDER_STATUS"			=>	$orders->GetStatusName($orders_list[$i][status]),
			"ORDER_ID"				=>	$orders_list[$i][id],
			"ORDER_INVOICE"			=>	$orders_list[$i][invoice],
			"ORDER_DATE"			=>	date("d-m-Y H:i", $orders_list[$i][time]),
			"ORDER_CUSTOMER_NAME"	=>	$user_info[company],
			"ORDER_CUSTOMER_CITY"	=>	$user_info[city],
			"ORDER_CUSTOMER_PHONE"	=>	$user_info[phone],
			"ORDER_TOTAL"			=>	number_format($orders->GetOrderValue($orders_list[$i][id]), 2, ",", " "),
			"ORDER_PAYMENT"			=>	$orders->GetPaymentName($orders_list[$i][payment_id]),
			"ORDER_DELIVERY"		=>	$orders->GetDeliveryName($orders_list[$i][delivery_id]),
			"ORDER_MANAGER_NAME"	=>	$manager_name,
			"ORDER_MANAGER_TIME"	=>	$manager_time
  		));  		$t->parse("_orders_list", "orders_list", true);
  		unset($user_info);  	}

	############################################################# страницы
	$orders_list = $orders->GetOrdersIDs();
	if (!$data->GET["page"]) $data->GET["page"] = 0;
	$pages = count($orders_list)/$_default_orders_per_page;
	$t->set_block("index", "orders_pages", "_orders_pages");
	if ($pages<=1) $t->set_var("orders_pages", "");
	$start_loop = 0;

	if (!$data->GET["page"]) $end_loop = 3; else $end_loop = $data->GET["page"]+3;
	if ($end_loop > $pages) $end_loop = $pages;
	$start_loop = $data->GET["page"]-2;
	if ($start_loop<0) $start_loop = 0;

	for ($i=$start_loop; $i<$end_loop; $i++) {
		$start = ($i * $_default_orders_per_page) + 1;
		$finish = ($i * $_default_orders_per_page) + $_default_orders_per_page;
		if ($finish>count($orders_list)) $finish = count($orders_list);

		if (($i == $start_loop)&&($data->GET["page"]>0)) {
		$tmp = "&laquo;&laquo;";
		$t->set_var(array(
			"ORDERS_PAGE"				=>	$data->GET["page"]-1,
			"ORDERS_PAGE_NAME"			=>	$tmp
		));
		$t->parse("_orders_pages", "orders_pages", true);
		}

		if ($i == $data->GET["page"]) $tmp = "<b>[".$start."-".$finish."]</b>"; else $tmp = "[".$start."-".$finish."]";
		$t->set_var(array(
			"ORDERS_PAGE"				=>	$i,
			"ORDERS_PAGE_NAME"			=>	$tmp
		));
		$t->parse("_orders_pages", "orders_pages", true);
		}
		if ($data->GET["page"]<$pages-1) {
		$t->set_var(array(
			"ORDERS_PAGE"				=>	$data->GET["page"]+1,
			"ORDERS_PAGE_NAME"			=>	"&raquo;&raquo;"
		));
		$t->parse("_orders_pages", "orders_pages", true);
	}
	######################################################## //	СТРАНИЦЫ
	unset($orders_list);

}


######################################################################################
if ($what == "edit") {
	$t->set_var(array(
		"PAGE"				=>	$data->GET["page"],
		"FIND"				=>	$data->GET["find"],
		"CUSTOMER_ID"		=>	$data->GET["customer"]
	));

	##############################################################	ИНФОРМАЦИЯ О ЗАКАЗЕ
  	$order	=	$orders->GetOrderByID($data->GET["oid"]);

  	if (($order[status] != 1) && ($order[status] != 2) ) {
		$blocks->HideBlock("index", "status-done");
		$blocks->HideBlock("index", "status-work");
	} else {
		if ($order[status] == 1) $blocks->HideBlock("index", "status-done");
		if ($order[status] == 2) $blocks->HideBlock("index", "status-work");
	}

    $t->set_var(array(
    	"ORDER_ID"					=>	$order[id],
    	"ORDER_TIME"				=>	strftime("%e %B %Y (%A), %H:%M", $order[time]),
    	"ORDER_INVOICE"				=>	$order[invoice],
    	"ORDER_STATUS_COLOR"		=>	$_default_orders_colors[$order[status]],
    	"ORDER_MANAGER_NAME"		=>	$users->GetNameById($order[manager_id]),
    	"ORDER_MANAGER_TIME"		=>	strftime("%e %B %Y (%A), %H:%M", $order[manager_time]),
    	"ORDER_PAYMENT"				=>	$orders->GetPaymentName($order[payment_id]),
    	"ORDER_DELIVERY"			=>	$orders->GetDeliveryName($order[delivery_id]),
    	"ORDER_ADDITIONAL"			=>	nl2br($order[additional])
    ));

	$orders_statuses 	= 	$orders->GetOrdersStatuses();
    $t->set_block("index", "status-list", "_status-list");
    for ($i0=0; $i0<count($orders_statuses); $i0++) {
		if ($orders_statuses[$i0][id] == $order[status]) $tmp_selected = "selected"; else $tmp_selected = "";
		$t->set_var(array(
			"STATUS_ID"			=>	$orders_statuses[$i0][id],
			"STATUS_NAME"		=>	$orders_statuses[$i0][name],
			"STATUS_SELECTED"	=>	$tmp_selected
		));
		$t->parse("_status-list", "status-list", true);
	}

	##############################################################	ИНФОРМАЦИЯ О ПОКУПАТЕЛЕ
 	$user_info		=		$customer->GetCustomerByID($order[user_id]);
 	$t->set_var(array(
 		"USER_COMPANY"		=>	$user_info[company],
 		"USER_CITY"			=>	$user_info[city],
 		"USER_PHONE"		=>	$user_info[phone],
 		"USER_EMAIL"		=>	$user_info[email],
 		"USER_USER_"		=>	$user_info[user_],
 		"USER_ORGANIZATION"	=>	$user_info[organization],
 		"USER_ORDERS_COUNT"	=>	$orders->GetOrdersQuantityByCustomer($order[user_id])
 	));


	##############################################################	СПИСОК ТОВАРОВ В ЗАКАЗЕ
	$t->set_block("index", "items_list", "_items_list");
	$items	=	$orders->GetOrderItems($data->GET["oid"]);
	if (!count($items)) $t->set_var("_items_list", "");
	$sum	=	0;
	for ($i=0; $i<count($items); $i++) {
		if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";
		$sum += ($items[$i][count] * $items[$i][price]);

		$pr = $cp->GetProductByID($items[$i][product_id]);

		$t->set_var(array(
			"I"						=>	$i,
     		"BGCOLOR"				=>	$bg_color,
     		"ITEMS_ID"				=>	$pr[id],
       		"ITEMS_PRCODE"			=>	$pr[prcode],
       		"ITEMS_CODE"			=>	$pr[code],
       		"ITEMS_NAME"			=>	$pr[name],
       		"ITEMS_WAREHOUSE"		=>	$pr[warehouse],
     		"ITEMS_COUNT"			=>	$items[$i][count],
     		"ITEMS_PRICE"			=>	number_format($items[$i][price], 2, ',', ' '),
     		"ITEMS_SUM"				=>	number_format(($items[$i][price] * $items[$i][count]), 2, ',', ' '),
     	));
		$t->parse("_items_list", "items_list", true);
	}
	$t->set_var(array(
     		"ORDER_TOTAL"				=>	number_format($sum, 2, ',', ' ')
     ));
}

######################################################################################
if ($what == "print") {
	$t->set_var(array(
		"PAGE"				=>	$data->GET["page"],
		"FIND"				=>	$data->GET["find"],
		"CUSTOMER_ID"		=>	$data->GET["customer"]
	));

	##############################################################	ИНФОРМАЦИЯ О ЗАКАЗЕ
  	$order	=	$orders->GetOrderByID($data->GET["oid"]);

  	if (($order[status] != 1) && ($order[status] != 2) ) {
		$blocks->HideBlock("index", "status-done");
		$blocks->HideBlock("index", "status-work");
	} else {
		if ($order[status] == 1) $blocks->HideBlock("index", "status-done");
		if ($order[status] == 2) $blocks->HideBlock("index", "status-work");
	}

    $t->set_var(array(
    	"ORDER_ID"					=>	$order[id],
    	"ORDER_TIME"				=>	strftime("%e %B %Y (%A), %H:%M", $order[time]),
    	"ORDER_INVOICE"				=>	$order[invoice],
    	"ORDER_STATUS_COLOR"		=>	$_default_orders_colors[$order[status]],
    	"ORDER_MANAGER_NAME"		=>	$users->GetNameById($order[manager_id]),
    	"ORDER_MANAGER_TIME"		=>	strftime("%e %B %Y (%A), %H:%M", $order[manager_time]),
    	"ORDER_PAYMENT"				=>	$orders->GetPaymentName($order[payment_id]),
    	"ORDER_DELIVERY"			=>	$orders->GetDeliveryName($order[delivery_id]),
    	"ORDER_ADDITIONAL"			=>	nl2br($order[additional])
    ));

	$orders_statuses 	= 	$orders->GetOrdersStatuses();
    $t->set_block("index", "status-list", "_status-list");
    for ($i0=0; $i0<count($orders_statuses); $i0++) {
		if ($orders_statuses[$i0][id] == $order[status]) $tmp_selected = "selected"; else $tmp_selected = "";
		$t->set_var(array(
			"STATUS_ID"			=>	$orders_statuses[$i0][id],
			"STATUS_NAME"		=>	$orders_statuses[$i0][name],
			"STATUS_SELECTED"	=>	$tmp_selected
		));
		$t->parse("_status-list", "status-list", true);
	}

	##############################################################	ИНФОРМАЦИЯ О ПОКУПАТЕЛЕ
 	$user_info		=		$customer->GetCustomerByID($order[user_id]);
 	$t->set_var(array(
 		"USER_COMPANY"		=>	$user_info[company],
 		"USER_CITY"			=>	$user_info[city],
 		"USER_PHONE"		=>	$user_info[phone],
 		"USER_EMAIL"		=>	$user_info[email],
 		"USER_USER_"		=>	$user_info[user_],
 		"USER_ORGANIZATION"	=>	$user_info[organization],
 		"USER_ORDERS_COUNT"	=>	$orders->GetOrdersQuantityByCustomer($order[user_id])
 	));


	##############################################################	СПИСОК ТОВАРОВ В ЗАКАЗЕ
	$t->set_block("index", "items_list", "_items_list");
	$items	=	$orders->GetOrderItems($data->GET["oid"]);
	if (!count($items)) $t->set_var("_items_list", "");
	$sum	=	0;
	for ($i=0; $i<count($items); $i++) {
		if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";
		$sum += ($items[$i][count] * $items[$i][price]);

		$pr = $cp->GetProductByID($items[$i][product_id]);

		$t->set_var(array(
			"I"						=>	$i,
     		"BGCOLOR"				=>	$bg_color,
     		"ITEMS_ID"				=>	$pr[id],
       		"ITEMS_PRCODE"			=>	$pr[prcode],
       		"ITEMS_CODE"			=>	$pr[code],
       		"ITEMS_NAME"			=>	$pr[name],
       		"ITEMS_WAREHOUSE"		=>	$pr[warehouse],
     		"ITEMS_COUNT"			=>	$items[$i][count],
     		"ITEMS_PRICE"			=>	number_format($items[$i][price], 2, ',', ' '),
     		"ITEMS_SUM"				=>	number_format(($items[$i][price] * $items[$i][count]), 2, ',', ' '),
     	));
		$t->parse("_items_list", "items_list", true);
	}
	$t->set_var(array(
     		"ORDER_TOTAL"				=>	number_format($sum, 2, ',', ' ')
     ));
}


$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
