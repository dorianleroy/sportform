<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"pr_pricelist.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

if (!$data->GET["cid"]) $data->GET["cid"]	=	0;

$cp	=	new CATS_AND_PRODUCTSClass();

#############################################################################################################
if ($what == "upload") {
	if ($_FILES['new_price'][size])	$cp->UploadPricelist($_default_images_dir_pricelist, $_FILES['new_price']);
	header("Location: ".$links[pr_pricelist]."?tmp=".time());
	die();
}


$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");

###########################################
if ($what == "general") {
	$t->set_var(array(
		"PRICELIST_DATE"		=>	date("d-m-Y H:i:s", filectime($_default_images_dir_pricelist)),
		"PRICELIST_SIZE"		=>	round( (filesize($_default_images_dir_pricelist) / 1024), 2)
	));
}


$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
