<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"bo_changepassword.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

$errors = "";

if ($what == "change") {

	$p	= $data->POST["password"];
	$p1 = $data->POST["password1"];

	if ( strlen($p) && ($p == $p1) ) {
		$users->ChangePassword($p);
		header("Location: ".$links['bo_index']);
		die();
	} else {
		$errors = $MESSAGES["error_password"];
	}
}


$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


###########################################
if ($what == "general") {	if (strlen($errors)) $show_errors = "block"; else $show_errors = "none";

	$t->set_var(array(
		"SHOW_ERRORS"		=>	$show_errors,
		"ERROR_MESSAGE"		=>	$errors
	));
}





$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
