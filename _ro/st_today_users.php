<?
#################################################################
require ("libs/fo_prepare.php");
$users->CheckAccess();

$t->set_file(array(
	    "index"		=>	"st_today_users.tpl.htm"
	    ));

$what = "general";

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];


$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");


########################################### ФОРМА С ВХОДОМ В СИСТЕМУ
if ($what == "general") {
	if (!$data->GET["start"]) $data->GET["start"]	=	date("d-m-Y");
	if (!$data->GET["finish"]) $data->GET["finish"]	=	date("d-m-Y", time()-(60*60*24*30));

	$t->set_var(array(
		"START"		=>	$data->GET["start"],
		"FINISH"	=>	$data->GET["finish"]
	));

 	$start	=	strtotime($data->GET["start"]);
 	$finish	=	strtotime($data->GET["finish"]);
 	$t->set_block("index", "today_users", "_today_users");
 	$sql = new SQLClass();
	$res = $sql->query("SELECT id, time, value FROM fo_stat WHERE stat_name='today_users' AND name='today_users' AND time <='".$start."' AND time >='".$finish."' ORDER BY time DESC");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

   		if (($i/2) == round($i/2)) $bg_color = "#d7d6d0"; else $bg_color = "#eae8dd";

   		$t->set_var(array(
				"I"					=>	$i,
				"BGCOLOR"			=>	$bg_color,
				"TU_DATE"			=>	date("d-m-Y, l", $z['time']),
				"TU_COUNT"			=>	$z['value'],
				"TU_WIDTH"			=>	round($z['value'] * 1.5)
		));

		$t->parse("_today_users", "today_users", true);
	}
	$sql->close();
}





$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

$t->p("OUT");
?>
