<?
################################################################# инклюдим то, что надо
require ("libs/library.php");

setlocale(LC_ALL, "ru_RU.utf8");
$sql = new SQLClass();
$res = $sql->query("SET NAMES utf8");
$sql->close();

if (!function_exists("htmlspecialchars_decode")) {
	function htmlspecialchars_decode($string,$style=ENT_COMPAT)
     {
         $translation = array_flip(get_html_translation_table(HTML_SPECIALCHARS,$style));
         if($style === ENT_QUOTES){ $translation['&#039;'] = '\''; }
         return strtr($string,$translation);
     }
}


function html2specialchars($str){
   $trans_table = array_flip(get_html_translation_table(HTML_ENTITIES));
   return strtr($str, $trans_table);
}

function GetVar($data, $var) {
	if (strlen($data->POST[$var])) $value = $data->POST[$var];
	if (strlen($data->GET[$var])) $value = $data->GET[$var];
	return $value;
}


################################################################# создаем шаблон
$t = new Template("templates/".$_curSchema."/", "keep");

################################################################# указываем список файлов с шаблонами

$t->set_file(array(
   		"overall_header" 		=> "overall_header.tpl.htm",
		"overall_footer" 		=> "overall_footer.tpl.htm"
		));

##################################################################	устанавливаем ссылки
$t->set_var(array(
	"CSS_FILE_LINK"		=>	"templates/".$_curSchema."/style.css",
	"META_ADDITIONAL"	=>	"",
	"IMAGE_DIR"			=>	"templates/".$_curSchema."/"
	));

##################################################################	генерим верхнее меню
$t->set_var(array(
		"BASE_URL"					=>	$links['base_url'],
		"THIS_URL"					=>	str_replace("http:/", "http://", str_replace("//", "/", $links['base_url'].$_SERVER["REQUEST_URI"]) ),
		"CURRENT_YEAR"				=>	date("Y")
    ));


$blocks 	= new BlocksClass();
$cp 		= new CATS_AND_PRODUCTSClass();

$my_url	=	"http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];

################################################################################	Запоминаем откуда пришел клиент
if (!isset($_COOKIE["f_referer"])) {
	if (isset($_SERVER['HTTP_REFERER'])) {
	    $url	=	parse_url($_SERVER['HTTP_REFERER']);
	    if ("http://".$url['host']."/" != $links['base_url']) {
	    	setcookie("f_referer", $_SERVER['HTTP_REFERER'], time()+31536000, "/");
	 	}
	}
}

################################################################################	//	Запоминаем откуда пришел клиент

############################################################################# Что раньше смотрел клиент
//if($from == 'p'){

    //$ids_product = $_SERVER['REQUEST_URI'];

    //setcookie("see_early[$ids_product]", $ids_product, time() + 3153600);

    //echo "<pre>", var_dump($_COOKIE), "</pre//>";
//}
########################################################################### // Что раньше смотрел клиент

?>