<?
class NewsClass {

function NewsClass() {
}

################################################################################################
function BlogList($cat) {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, publishdate, url, headline, annot, enabled
						FROM ".$tableCollab["blog"]."
						WHERE cat_id IN (".$cat.") AND enabled='1'
						ORDER BY publishdate DESC");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

####################################################################################
function GetBlogByURL($url) {
	global $tableCollab;
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, cat_id, publishdate, headline, annot, details, album_id, album_auth,enabled FROM ".$tableCollab["blog"]." WHERE url='".$url."' AND enabled='1'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z;
}

################################################################################################
function GetNewsCats() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["news_cats"]." WHERE enabled='1' ORDER BY seq");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################################################
function AddNews($title, $enabled) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["news"]." (publishdate, headline, enabled) VALUES('".time()."', '".$title."', '".$enabled."')");
	$sql->close();

	return mysql_insert_id();
}

################################################################################################
function GetCatName($cat_id) {
	global $tableCollab;
	$sql = new SQLClass();
	$res = $sql->query("SELECT name FROM ".$tableCollab["news_cats"]." WHERE id='".$cat_id."'AND enabled='1'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z[name];
}

################################################################################################
function NewsList() {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, cat_id, publishdate, headline, enabled FROM ".$tableCollab["news"]." WHERE enabled='1' ORDER BY cat_id, publishdate DESC");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$z[cat_name]	=	$this->GetCatName($z[cat_id]);
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}


################################################################################################
function UpdateNews($id, $name, $enabled) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("UPDATE ".$tableCollab["news"]." SET headline='".$name."', enabled='".$enabled."' WHERE id='".$id."'");
	$sql->close();
}

####################################################################################
function GetNewsByID($id) {
	global $tableCollab;
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, cat_id, publishdate, url, headline, annot, details, enabled FROM ".$tableCollab["news"]." WHERE id='".$id."' AND enabled='1'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z;
}

####################################################################################
function GetNewsByURL($url) {
	global $tableCollab;
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, cat_id, publishdate, url, headline, annot, details, enabled FROM ".$tableCollab["news"]." WHERE url='".$url."' AND enabled='1'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z;
}

####################################################################################
function GetNewsByCat($cat_id) {
	global $tableCollab;
	$sql = new SQLClass();
	$res = $sql->query("SELECT id, publishdate, url, headline, annot, details, enabled FROM ".$tableCollab["news"]." WHERE cat_id='".$cat_id."' AND enabled='1' ORDER BY publishdate DESC");
	$q	=	array();
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

####################################################################################
function GetImagesForNews($text_id, $img_dir) {
	$q = array();

	$d = dir($img_dir);
	while (false !== ($entry = $d->read())) {
		$s = explode("_", $entry);
		if ($s[0] == $text_id) $q[count($q)] = $entry;
	}
	$d->close();

	return $q;
}

######################################################	������� ������ ��������� ��������
function SaveNews($id, $cat_id, $name, $time, $short, $full, $forum_id, $enabled, $url) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["news"]." SET cat_id='".$cat_id."', headline='".$name."', publishdate='".$time."', url='".$url."', annot='".$short."', details='".$full."', enabled='".$enabled."' WHERE id='".$id."'");
	$sql->close();
}

####################################################################################
function AddImage($text_id, $img_dir, $img, $name) {
	$fileinfo = pathinfo($img[name]);
	$extension = $fileinfo[extension];
	if (strlen($name)) $m_name = $name; else $m_name = substr(md5(uniqid(rand(), true)), 0, 5);
	$new_name = $text_id."_".$m_name."_.".$extension;
	copy($img[tmp_name], $img_dir.$new_name);
	chmod($img_dir.$new_name, 0666);
}

####################################################################################
function DelImage($img, $img_dir) {
	unlink($img_dir.$img);
}


################################################################################################
function TipsList() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, time, name, enabled FROM ".$tableCollab["tips"]." ORDER BY time DESC, id DESC");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################################################
function UpdateTips($id, $name, $enabled) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("UPDATE ".$tableCollab["tips"]." SET name='".$name."', enabled='".$enabled."' WHERE id='".$id."'");
	$sql->close();
}

################################################################################################
function AddTips($title, $enabled) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["tips"]." (time, name, enabled) VALUES('".time()."', '".$title."', '".$enabled."')");
	$sql->close();
	return mysql_insert_id();
}

####################################################################################
function GetTipsByID($id) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, time, name, text, enabled FROM ".$tableCollab["tips"]." WHERE id='".$id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z;
}

######################################################	������� ������ ��������� ��������
function SaveTips($id, $name, $time, $full, $enabled) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["tips"]." SET name='".$name."', time='".$time."', text='".$full."', enabled='".$enabled."' WHERE id='".$id."'");
	$sql->close();
}

}
?>