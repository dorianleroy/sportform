<?
class CATS_AND_PRODUCTSClass {

function CATS_AND_PRODUCTSClass() {
}


################################################################# добавляем
################################################################# категорию
function AddCat($cid, $name, $short_name, $url) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["categoryes"]." (pid, seq, name, short_name, url) VALUES ('".$cid."', '".time()."', '".$name."', '".$short_name."', '".$url."')");
	$sql->close();

	return $id;
}

################################################################# Сохранить информацию
################################################################# о категории
function UpdateCat($id, $short_name, $name, $enabled, $url, $desc, $keywords) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["categoryes"]." SET short_name='".$short_name."', name='".$name."', enabled='".$enabled."', url='".$url."', _desc='".$desc."', _keywords='".$keywords."' WHERE id='".$id."'");
	$sql->close();

	return $id;
}

################################################################# Изменить сортировку
################################################################# категории
function UpdateCatSeq($pid, $id, $what) {
	global $tableCollab;

	$_this = $this->GetCatByID($id);

	if ($what == "up") $tmp_seq = "AND seq<'".$_this[seq]."' ORDER BY seq DESC"; else $tmp_seq = "AND seq>'".$_this[seq]."' ORDER BY seq";

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, seq FROM ".$tableCollab["categoryes"]." WHERE pid='".$pid."' ".$tmp_seq." LIMIT 1");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	if ($z[id]) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["categoryes"]." SET seq='".$z[seq]."' WHERE pid='".$pid."' AND id='".$id."'");
		$sql->query("UPDATE ".$tableCollab["categoryes"]." SET seq='".$_this[seq]."' WHERE pid='".$pid."' AND id='".$z[id]."'");
		$sql->close();
	}
}

################################################################# транслитерация
################################################################# русского текста
function translitIt($str) {
    $tr = array(
        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "_", "."=> "", "/"=> "_", "," => "", "ё"=>"e",
        ";"=>":", "\""=>"", "'"=>"", "№"=>"N", "«"=>"", "»"=>"",
        "+"=>"-", "&"=>"", "("=>"", ")"=>"", "«"=>"", "»"=>"", "$"=>""
    );
    return strtr($str,$tr);
}

################################################################# добавляем товар
################################################################# в категорию
function AddProduct($cid, $name) {
	global $tableCollab;

	if (strpos($name, "'")) {		$name	=	addslashes($name);	}

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["products"]." (name, url) VALUES ('".$name."', '".$this->translitIt($name)."')");
	$id = mysql_insert_id();
	$sql->query("INSERT INTO ".$tableCollab["cats2prods"]." (cat_id, prod_id) VALUES ('".$cid."', '".$id."')");
	$sql->close();

	return $id;
}

################################################################# функиця, возвращающая
################################################################# текущий курс
function GetCurrency($name) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("SELECT value FROM ".$tableCollab["currency"]." WHERE currency='".$name."'");
	$sql->fetch();
  	$z = $sql->Record;
	$sql->close();
	return $z[value];
}

################################################################# взять ID продукта, который
################################################################# можно купить вместе с этим продуктом, но дешевле
function GetBuyTogetherID($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT pid2 FROM ".$tableCollab["buytogether"]." WHERE pid1='".$pid."'");
	$sql->fetch();
  	$z = $sql->Record;
	$sql->close();
	return $z[pid2];
}

################################################################# обновить товар, который
################################################################# можно купить вместе с этим продуктом, но дешевле
function UpdateBuyTogetherID($pid, $item) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["buytogether"]." WHERE pid1='".$pid."'");
	$sql->query("INSERT INTO ".$tableCollab["buytogether"]." (pid1, pid2) VALUES('".$pid."', '".$item."')");
	$sql->close();
}

################################################################# обновить товар, который
################################################################# можно купить вместе с этим продуктом, но дешевле
function DelBuyTogetherID($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["buytogether"]." WHERE pid1='".$pid."'");
	$sql->close();
}

################################################################# взять все товары,
################################################################# которые просмотрели за указанный период
function GetProductsViewsByPeriod($from_date, $to_date) {
	global $tableCollab;

	$q = array();

	if (($to_date-$from_date)>(60*60*24)) $limit = "LIMIT 100"; else $limit = "";

	$sql = new SQLClass();
	$res = $sql->query("SELECT DISTINCT(pid), SUM(views) s
				 FROM ".$tableCollab["stat_products_views"]."
				 WHERE time>='".$from_date."' AND time<='".$to_date."'
				 GROUP BY pid
				 ORDER BY s DESC
				 ".$limit);
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

################################################################# взять список ID статей, которые
################################################################# могут быть интересны для данного продукта
function GetP2ARTICLE($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT article FROM ".$tableCollab["products2articles"]." WHERE product_id='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z[article];
	}
	$sql->close();
	return $q;
}

################################################################# Удалить товары из P2ARTICLE
#################################################################
function DelFromP2ARTICLE($pid, $article) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["products2articles"]." WHERE product_id='".$pid."' AND article='".$article."'");
	$sql->close();
}

################################################################# Добавить товар
################################################################# для P2ARTICLE
function AddP2ARTICLE($pid, $article) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["products2articles"]." (product_id, article) VALUES ('".$pid."', '".$article."')");
	$sql->close();
}

################################################################# взять список ID продуктов, которые
################################################################# надо не забыть купить с товарами этой категории
function GetP2CID($cid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT product_id FROM ".$tableCollab["products2cats"]." WHERE cat_id='".$cid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z[product_id];
	}
	$sql->close();
	return $q;
}

################################################################# Удалить товары из P2C
################################################################# товар
function DelFromP2C($cid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["products2cats"]." WHERE cat_id='".$cid."' AND product_id='".$p2p."'");
	$sql->close();
}

################################################################# Добавить товар
################################################################# для P2C
function AddP2C($cid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["products2cats"]." (cat_id, product_id) VALUES ('".$cid."', '".$p2p."')");
	$sql->close();
}


################################################################# взять список ID продуктов, которые
################################################################# "часто" покупают вместе с этим продуктом
function GetP2PID($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT product_id2 FROM ".$tableCollab["products2products"]." WHERE product_id1='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z[product_id2];
	}
	$sql->close();
	return $q;
}

################################################################# Удалить товары из P2P
################################################################# товар
function DelFromP2P($pid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["products2products"]." WHERE product_id1='".$pid."' AND product_id2='".$p2p."'");
	$sql->close();
}

################################################################# Добавить товар
################################################################# для P2P
function AddP2P($pid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["products2products"]." (product_id1, product_id2) VALUES ('".$pid."', '".$p2p."')");
	$sql->close();
}

################################################################# взять список ID продуктов, которые
################################################################# похожи (альтернатива) с этим продуктом
function GetP2ALT($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT product_id2 FROM ".$tableCollab["products2alternative"]." WHERE product_id1='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z[product_id2];
	}
	$sql->close();
	return $q;
}

################################################################# Удалить товары из P2ALT
################################################################# товар
function DelFromP2ALT($pid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("DELETE FROM ".$tableCollab["products2alternative"]." WHERE product_id1='".$pid."' AND product_id2='".$p2p."'");
	$sql->close();
}

################################################################# Добавить товар
################################################################# для P2ALT
function AddP2ALT($pid, $p2p) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("INSERT INTO ".$tableCollab["products2alternative"]." (product_id1, product_id2) VALUES ('".$pid."', '".$p2p."')");
	$sql->close();
}

################################################################# обновить цены и статус
################################################################# у продукта
function UpdateProductGeneralInfo($pid, $price, $price2, $enabled, $warehouse, $seq) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET price='".$price."', price2='".$price2."', enabled='".$enabled."', warehouse='".$warehouse."', seq='".$seq."' WHERE id='".$pid."'");
	$sql->close();
}

################################################################# обновить цены и статус
################################################################# у продукта
function UpdateProductUrl($pid, $url, $enabled) {
	global $tableCollab;
	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET url='".$url."', enabled='".$enabled."' WHERE id='".$pid."'");
	$sql->close();
}

################################################################# обновить цены и статус
################################################################# у продукта
function UpdateProductName($pid, $name, $enabled) {
	global $tableCollab;
	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET name='".$name."', enabled='".$enabled."' WHERE id='".$pid."'");
	$sql->close();
}

################################################################# обновить цены и статус
################################################################# у продукта
function UpdateProductFullInfo($pid, $name, $vendor, $url, $descr, $full_descr, $price, $old_price, $enabled, $page_descr, $page_keywords, $code, $prcode, $youtube, $for_unit, $units, $f_new, $f_hot, $f_popular, $price2, $for_unit2, $size_chart, $color) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET
				 name='".$name."', vendor='".$vendor."', descr='".$descr."', full_descr='".$full_descr."', price='".$price."',
				 old_price='".$old_price."', enabled='".$enabled."', page_descr='".$page_descr."', page_keywords='".$page_keywords."',
				 code='".$code."', prcode='".$prcode."', youtube='".$youtube."', url='".$url."', for_unit='".$for_unit."', units='".$units."',
				 price2='".$price2."', for_unit2='".$for_unit2."', size_chart='".$size_chart."', color='".$color."'
				 WHERE id='".$pid."'");
	$sql->close();
}

################################################################# создаем массив
################################################################# категорий
function GetProductsTree($pid, $tree, $notproducts="", $enabled="") {
	global $tableCollab;

	$cat = $this->GetSimpleCatsList($pid);
	for ($i=0; $i<count($cat); $i++) {
		$tmp = count($tree);
		$tree[$tmp][cat_name] 	= $cat[$i][name];
		$tree[$tmp][cat_id] 	= $cat[$i][id];
		if (!$notproducts) $tree[$tmp][products]	= $this->GetProductsByCID($cat[$i][id], null, null, null, null, 'old_price DESC, price', $enabled);

		$tree = $this->GetProductsTree($cat[$i][id], $tree, $notproducts, $enabled);
	}

	return $tree;
}

################################################################# создаем массив
################################################################# категорий    //new
function GetSimpleCatsList_2($pid) {
	global $tableCollab;
	$q = array();


	if ($pid == "-1") $_and_pid = ""; else $_and_pid = "AND pid='".$pid."'";

	$sql = new SQLClass();
	$sql->query("SELECT id, short_name, name, url, pid, enabled
    	         FROM ".$tableCollab["categoryes"]."
				 WHERE enabled='1' $_and_pid
				 ORDER BY seq
			 	 ");
	while($sql->fetch()) {
    	$z = $sql->Record;
		$q[count($q)] = $z;
	}

	$sql->close();
	return $q;
}


################################################################# создаем массив
################################################################# категорий       //old
function GetSimpleCatsList($pid) {
	global $tableCollab;
	$q = array();
	if ($pid == "-1") $_and_pid = ""; else $_and_pid = "AND pid='".$pid."'";
	$sql = new SQLClass();
	$sql->query("SELECT id, short_name, name, url, pid, enabled
    	         FROM ".$tableCollab["categoryes"]."
				 WHERE enabled='1' $_and_pid
				 ORDER BY seq
			 	 ");
	while($sql->fetch()) {
    	$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

################################################################# вернуть список
################################################################# статей для категории
function GetArticlesIDforCat($pid) {
	global $tableCollab;
	$sql = new SQLClass();
	$sql->query("SELECT articles FROM ".$tableCollab["categoryes"]." WHERE 1 AND id='".$pid."'");
	$sql->fetch();
    $z = $sql->Record;
	$sql->close();
	return $z[articles];
}

################################################################# создаем массив
################################################################# всех производителей
function GetVendorsList() {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT id, name FROM ".$tableCollab["vendors"]." ORDER BY name");
	while($sql->fetch()) {
    	$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

################################################################# создаем массив
################################################################# всех едениц измерения
function GetUnitsList() {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT id, name FROM ".$tableCollab["units"]." ORDER BY name");
	while($sql->fetch()) {
    	$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();
	return $q;
}

################################################################# создаем массив
################################################################# всех едениц измерения
function GetUnitByID($id) {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT name FROM ".$tableCollab["units"]." WHERE id='".$id."'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();
	return $z[name];
}

################################################################# создаем массив
################################################################# всех едениц измерения
function GetUnitShortNameByID($id) {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT short_name FROM ".$tableCollab["units"]." WHERE id='".$id."'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();
	return $z[short_name];
}

################################################################# взять информацию
################################################################# о конкретном производителе
function GetVendorById($vendor_id, $img_dir) {
	global $tableCollab;
	$q = array();
	$sql = new SQLClass();
	$sql->query("SELECT id, name FROM ".$tableCollab["vendors"]." WHERE id='".$vendor_id."'");
	$sql->fetch();
    $z = $sql->Record;
	$q[title] = $z[title];
	$sql->close();

	$sql = new SQLClass();
	$sql->query("SELECT text FROM ".$tableCollab["vendors_texts"]." WHERE vendor_id='".$vendor_id."'");
	$sql->fetch();
    $z = $sql->Record;
	$q[text] = $z[text];
	$sql->close();

	##### img logo
	if (file_exists($img_dir.$vendor_id."_small_logo.gif")) $q[logo] = $img_dir.$vendor_id."_small_logo.gif";
	if (file_exists($img_dir.$vendor_id."_small_logo.jpg")) $q[logo] = $img_dir.$vendor_id."_small_logo.jpg";
	if (file_exists($img_dir.$vendor_id."_small_logo.png")) $q[logo] = $img_dir.$vendor_id."_small_logo.png";

	return $q;
}

################################################################# категория по ID
#################################################################
function GetCatByID($id) {
	global $tableCollab;
	$sql = new SQLClass();
	$sql->query("SELECT id, pid, short_name, name, url, _desc, _keywords, enabled, seq FROM ".$tableCollab["categoryes"]." WHERE id='".$id."' AND enabled='1'");
	$sql->fetch();
   	$z = $sql->Record;
	$sql->close();
	return $z;
}

################################################################# массив всех подкатегорий
#################################################################
function GetSubCatsInCat($pid, $list) {
	global $tableCollab;

	$list[count($list)] = $pid;

	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["categoryes"]." WHERE pid='".$pid."' AND enabled='1'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$list = $this->GetSubCatsInCat($z[id], $list);
	}
	$sql->close();
	return array_unique($list);
}

################################################################# проверка на то
################################################################# дочерняя ли это категория
function IsThisChildCat($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("SELECT pid FROM ".$tableCollab["categoryes"]." WHERE id='".$pid."'  AND enabled='1'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[pid];
}

################################################################# проверка на то
################################################################# родительская ли это категория
function IsThisParentCat($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("SELECT COUNT(id) FROM ".$tableCollab["categoryes"]." WHERE pid='".$pid."' AND enabled='1'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z[0];
}

################################################################# формируем список производителей
################################################################# в этой категории
function GetVendorsInCat($cid) {
	global $tableCollab;

$q = array();

$vendors = $this->GetAllVendorsInCat($cid, "");

$v = explode(", ", $vendors);
$v = array_unique($v);
$vendors = implode(", ", $v);

if (strlen($vendors)) {

$sql = new SQLClass();
$a = $sql->query("SELECT id, name FROM ".$tableCollab["vendors"]." WHERE id IN(".$vendors.") ORDER BY name");
for ($i=0; $i<mysql_num_rows($a); $i++) {
	$sql->fetch();
	$z = $sql->Record;
	$q[count($q)] = $z;
}
$sql->close();

}

return $q;

}


################################################################# формируем список серий
################################################################# в этой категории
function GetSeriesInCat($cid) {
	global $tableCollab;

$q = array();

$sql = new SQLClass();
$a = $sql->query("SELECT ".$tableCollab["series"].".id, ".$tableCollab["series"].".name, ".$tableCollab["series"].".descr
				  FROM ".$tableCollab["series"]."
				  WHERE cat_id='".$cid."' ORDER BY seq");
for ($i=0; $i<mysql_num_rows($a); $i++) {
	$sql->fetch();
	$z = $sql->Record;
	$q[count($q)] = $z;
}
$sql->close();

return $q;

}

################################################################# формируем список производителей
################################################################# (в качестве их ID; не уникальные)
function GetAllVendorsInCat($cid, $vendors) {
	global $tableCollab;

	$sql = new SQLClass();
	$a = $sql->query("SELECT id, name FROM ".$tableCollab["categoryes"]." WHERE pid='".$cid."'");
	for ($i=0; $i<mysql_num_rows($a); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$vendors = $this->GetAllVendorsInCat($z[id], $vendors);
	}
	$sql->close();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$a = $sql->query("
			SELECT ".$tp.".vendor
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id
	");
	for ($i=0; $i<mysql_num_rows($a); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$vendors .= $z[vendor];
		if ($i < (mysql_num_rows($a)-1)) $vendors .= ", ";
	}
	$sql->close();

	return $vendors;
}


################################################################# считаем сколько
################################################################# товаров в этой категории
function GetProductsCountInCat($cid, $count) {
	global $tableCollab;

	$sql = new SQLClass();
	$a = $sql->query("SELECT id, name FROM ".$tableCollab["categoryes"]." WHERE pid='".$cid."'");
	for ($i=0; $i<mysql_num_rows($a); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$count = $this->GetProductsCountInCat($z[id], $count);
	}
	$sql->close();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$a = $sql->query("
			SELECT COUNT(".$tp.".id)
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id
	");
	$sql->fetch();
	$z = $sql->Record;
	$count += $z[0];
	$sql->close();

	return $count;
}

################################################################# взять картинку для
################################################################# товара
function Increaseorders($pid) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["products"]." SET orders=orders+1 WHERE id='".$pid."'");
	$sql->close();
}

################################################################# взять картинку для
################################################################# товара
function GetImageForPID($img_dir, $type, $pid, $color=0) {
	global $_curSchema;
// var_dump($img_dir."  pid-**".$pid."** _type-**".$type."**color-**".$color."**_.jpg"); exit;
	if (file_exists("uploads/images/products/".$pid."_".$type."-".$color."_.gif")) return "/uploads/images/products/".$pid."_".$type."-".$color."_.gif";
	if (file_exists("uploads/images/products/".$pid."_".$type."-".$color."_.jpg")) return "/uploads/images/products/".$pid."_".$type."-".$color."_.jpg";
	if (file_exists("uploads/images/products/".$pid."_".$type."-".$color."_.png")) return "/uploads/images/products/".$pid."_".$type."-".$color."_.png";
	return $img_dir."nophoto.jpg";
}

################################################################# взять все картинки для товара
#################################################################
function GetImagesForPID($img_dir, $pid, $color="") {
	global $_curSchema;
	$q	=	array();
    $array_img = glob(".".$img_dir."/".$pid."_*-".$color."_.jpg");
    if(is_array($array_img)){
        foreach ($array_img as $filename) {
            if (!strpos($filename, "_main")) $q[count($q)] = $filename;
        }
    }
	sort($q, SORT_STRING);
	return $array_img;
}

####################################################################################
function AddImage($pid, $img_dir, $img, $name) {
	$fileinfo = pathinfo($img[name]);
	$extension = $fileinfo[extension];
	if (strlen($name)) $m_name = $name; else $m_name = substr(md5(uniqid(rand(), true)), 0, 5);
	$new_name = $pid."_".$m_name."_.".$extension;
	copy($img[tmp_name], $img_dir.$new_name);
	chmod($img_dir.$new_name, 0666);
}

####################################################################################
function DelImage($img, $img_dir) {
	unlink($img_dir.$img);
}

################################################################# берем весь путь с
################################################################# самой главной и вниз
function GetPathFromCID($cid) {
	global $tableCollab;

	$q = array();
	$tmp = $cid;

	while ($tmp) {
		$sql = new SQLClass();
		$sql->query("SELECT id, pid, name, short_name, url FROM ".$tableCollab["categoryes"]." WHERE id='".$tmp."'");
		$sql->fetch();
   		$z = $sql->Record;
		$tmp = $z[pid];
		$q[count($q)] = $z;
		$sql->close();
	}

	return $q;
}

################################################################# взять самую верхнюю
################################################################# главную категорию
function GetMainCatByPID($pid) {
	global $tableCollab;

	$q = $pid;

	while ($q) {
		$sql = new SQLClass();
		$sql->query("SELECT id, pid FROM ".$tableCollab["categoryes"]." WHERE id='".$q."'");
		$sql->fetch();
   		$z = $sql->Record;
		$q = $z[pid];
		$id = $z[id];
		$sql->close();
	}

	return $id;
}

################################################################# рекурсивное составление
################################################################# массива всех подкатегорий
function RecursiveSubCats($cid, $cats) {
	global $tableCollab;

	if ($this->IsThisParentCat($cid)) {
		$subcats = $this->GetSimpleCatsList($cid);
		for ($i=0; $i<count($subcats); $i++) {
			$cats = $this->RecursiveSubCats($subcats[$i][id], $cats);
		}
	} else {
		$cats[count($cats)] = $cid;
	}
	return $cats;
}

################################################################# список хитов продаж по номеру категории
################################################################# при этом категория может быть родительская
function GetBestSellers($cid, $limit) {
	global $tableCollab;

	######	для начала возьмем список всех подкатегорий
	$cats = array($cid);
	$cats = $this->RecursiveSubCats($cid, $cats);

	######	составляем из массива список через запятую
	$in_cats = implode(",", $cats);

	######	ну а теперь сами продукты
	$q = array();
	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id IN (".$in_cats.") AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series."
			ORDER BY ".$tp.".orders DESC
			LIMIT ".$limit."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}


################################################################# список рекомендованных по номеру категории
################################################################# при этом категория может быть родительская
function GetRecomended($cid, $limit) {
	global $tableCollab;

	######	для начала возьмем список всех подкатегорий
	$cats = array($cid);
	$cats = $this->RecursiveSubCats($cid, $cats);

	######	составляем из массива список через запятую
	$in_cats = implode(",", $cats);

	######	ну а теперь сами продукты
	$q = array();
	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id IN (".$in_cats.") AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series."
			ORDER BY ".$tp.".views DESC
			LIMIT ".$limit."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# список товаров
################################################################# по номеру категории
function GetProductsByCID($cid, $vendor, $series, $from, $limit, $sort = "", $enabled="") {
	global $tableCollab, $MESSAGES;

	if (!$this->GetCatByID($cid)) return 0;
	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];
	$ts = $tableCollab["series"];

	if (!strlen($sort)) $order_by	=	$tp.".price"; else $order_by	=	$tp.".".$sort;
	if (!$enabled) $where_enabled = ""; else $where_enabled = "AND ".$tp.".enabled='1'";
	if (!$vendor) $where_vendor = ""; else $where_vendor = "AND ".$tp.".vendor='".$vendor."'";
	if (strlen($from)&&strlen($limit)) $_limit = "LIMIT $from, $limit"; else $_limit = "";
	if (strlen($series)) $where_series = "AND series='".$series."'"; else $where_series = "";

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tp.".vendor, ".$tp.".url, ".$tp.".name, ".$tp.".code, ".$tp.".prcode, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".url,
			       ".$tp.".old_price, ".$tp.".views, ".$tp.".orders, ".$tp.".enabled, ".$tp.".warehouse, ".$tp.".units, ".$tp.".for_unit, ".$tp.".descr, ".$tp.".seq,
			       ".$tp.".price2, ".$tp.".for_unit2
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series." ".$where_enabled."
			ORDER BY ".$order_by.", ".$tp.".name
			$_limit
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[name]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}


################################################################# список товаров
################################################################# по брэнду
function GetProductByVendor($vendor) {
    global $tableCollab;

    $q = array();

    $sql = new SQLClass();

    $tp = $tableCollab['products'];
    $res0 = $sql->query("SELECT id FROM ".$tableCollab['vendors']." WHERE name LIKE '".$vendor."'");
    $sql->fetch();
    $z=$sql->Record;

    $res = $sql->query("
     SELECT ".$tp.".id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders
        FROM ".$tp."
        WHERE vendor='$z[id]' AND $tp.`enabled`=1 AND $tp.`id`>6
    ");


     for ($i=0; $i<mysql_num_rows($res); $i++) {
        $sql->fetch();
        $prod = $sql->Record;
        $q[count($q)] = $prod;
    }

    return $q;

}

###########################################################################################
function GetProdByIdCatNew($cid){
    global $tableCollab;

    $q = array();
    $qTemp = array();

    $sql = new SQLClass();

    $list_txt = $cid;

    $tp = $tableCollab['products'];
    $tc2p = $tableCollab['cats2prods'];

    if (!$cid) die("Error. No values for function");

    $res3 = $sql->query("
        SELECT ".$tp.".id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders
        FROM ".$tp.", ".$tc2p."
        WHERE ".$tc2p.".cat_id IN(".$list_txt.") AND ".$tc2p.".prod_id=".$tp.".id   AND enabled='1'
        ORDER BY ".$tp.".`price` DESC
        ");

    for ($i=0; $i<mysql_num_rows($res3); $i++) {
        $sql->fetch();
        $z = $sql->Record;

        $qT[count($qT)] = $z;
    }


    $sql->close();

    return $qT;
}

###########################################################################################################
function GetProdByIdCat($cid){

    global $tableCollab;

    $q = array();
    $qTemp = array();

    $sql = new SQLClass();

    $res1 = $sql->query("
        SELECT url FROM ".$tableCollab["categoryes"]." where id='$cid'
    ");

    $sql->fetch();

    $z=$sql->Record;

    if (strpos($z[url],'obuv')){
        $wha = '%ob%v%';
        $z[url] = str_replace('obuv','',$z[url]);
        $wh = "and url LIKE '$wha'";
    }
    elseif(strpos($z[url],'form')){
        $wha = '%ob%v%';
        $z[url] = str_replace('form','',$z[url]);
        $wh = "and url NOT LIKE '$wha'";
    }
        else

       $wh = '';

//   var_dump($wh);exit;

    $res2 = $sql->query("
        SELECT id FROM ".$tableCollab["categoryes"]." where url LIKE '%$z[url]%' ".$wh."
    ");

    for ($i=0; $i<mysql_num_rows($res2); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[$z[id]] = $z;
	}

    foreach($q as $k=>$v){
       if($k != $cid)
        $qTemp[] = $k;
    }

    if(count($qTemp)>0){

        $list_txt = implode(",", $qTemp);

        $tp = $tableCollab['products'];
        $tc2p = $tableCollab['cats2prods'];
    //  $ts = $tableCollab['series'];

        if (!$cid) die("Error. No values for function");


        $res3 = $sql->query("
            SELECT ".$tp.".id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders
            FROM ".$tp.", ".$tc2p."
            WHERE ".$tc2p.".cat_id IN(".$list_txt.") AND ".$tc2p.".prod_id=".$tp.".id
            ");

        for ($i=0; $i<mysql_num_rows($res3); $i++) {
            $sql->fetch();
            $z = $sql->Record;

            $qT[count($qT)] = $z;
        }
    }
    else {
        $qT =array();
    }

    //    echo "<pre>", var_dump($q), "</pre>";
        $sql->close();

        return $qT;

}

################################################################# список товаров
################################################################# по номеру категории
function GetProductsByCatsList($list, $from, $limit) {
	global $tableCollab, $MESSAGES;

	if (!count($list)) return 0;
	$list_txt = implode(", ", $list);
	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	if (strlen($from)&&strlen($limit)) $_limit = "LIMIT $from, $limit"; else $_limit = "";

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id IN(".$list_txt.") AND ".$tc2p.".prod_id=".$tp.".id AND enabled='1'
			ORDER BY ".$tp.".stock DESC, ".$tp.".views DESC, ".$tp.".name
			$_limit
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}


################################################################# берем какое-то
################################################################# поле из продуктов по категории
function GetProductsFieldByCID($cid, $field) {
	global $tableCollab;

	if (!$this->GetCatByID($cid)) return 0;
	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".".$field."
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor."
			");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z[$field];
	}
	$sql->close();

	return $q;
}

################################################################# список товаров
################################################################# по номеру категории
function GetProductsCountInCID($cid, $vendor, $series) {
	global $tableCollab;

	if (!$this->GetCatByID($cid)) return 0;

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	if (!$vendor) $where_vendor = ""; else $where_vendor = "AND ".$tp.".vendor='".$vendor."'";
	if (strlen($series)) $where_series = "AND series='".$series."'"; else $where_series = "";

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT COUNT(".$tp.".id)
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series."
		");
	$sql->fetch();
	$z = $sql->Record;

	return $z[0];
}

################################################################# список товаров
################################################################# по списку категории
function GetProductsCountInList($list, $vendor, $series) {
	global $tableCollab;

	if (!count($list)) return 0;
	$list_txt = implode(", ", $list);

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	if (!$vendor) $where_vendor = ""; else $where_vendor = "AND ".$tp.".vendor='".$vendor."'";
	if (strlen($series)) $where_series = "AND series='".$series."'"; else $where_series = "";

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT COUNT(".$tp.".id)
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id IN(".$list_txt.") AND ".$tc2p.".prod_id=".$tp.".id ".$where_vendor." ".$where_series."
		");
	$sql->fetch();
	$z = $sql->Record;

	return $z[0];
}

################################################################# взять ID родительской категории по номеру текущей сабдиректории
function GetParentCat($cid) {
   global $tableCollab;
    $sql = new SQLClass();
    $res = $sql->query("SELECT pid FROM ".$tableCollab["categoryes"]." WHERE id='".$cid."'");
    $sql->fetch();
    $z = $sql->Record;
    return $z;
}

################################################################# взять ID категории
################################################################# по его URL
function GetCategoryIDByURL($url) {
	global $tableCollab, $MESSAGES;
	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["categoryes"]." WHERE url='".$url."' AND enabled='1'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	return $z[id];
}

################################################################# взять ID товара
################################################################# по его URL
function GetProductIDByURL($url) {
	global $tableCollab, $MESSAGES;
    $url=urlencode($url);
	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["products"]." WHERE url='".$url."' AND enabled='1'");
	$query = "SELECT id FROM ".$tableCollab["products"]." WHERE url='".$url."' AND enabled='1'";
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
	//var_dump($z); exit;
	return $z[id];
}

################################################################# взять один товар
################################################################# по номеру товара
function GetProductByID($pid) {
	global $tableCollab, $MESSAGES;

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
    $test = "SELECT ".$tp.".id, ".$tc2p.".cat_id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price,
			".$tp.".orders, ".$tp.".enabled, ".$tp.".page_descr, ".$tp.".url, ".$tp.".size_chart,
			".$tp.".page_keywords, ".$tp.".code, ".$tp.".prcode, ".$tp.".warehouse, ".$tp.".youtube, ".$tp.".units, ".$tp.".for_unit, ".$tp.".price2, ".$tp.".for_unit2,
			".$tp.".color, ".$tp.".rating_sum, ".$tp.".rating_count
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tp.".id='".$pid."' AND ".$tc2p.".prod_id='".$pid."' AND ".$tp.".enabled='1'";

	$res = $sql->query("
			SELECT ".$tp.".id, ".$tc2p.".cat_id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price,
			".$tp.".orders, ".$tp.".enabled, ".$tp.".page_descr, ".$tp.".url, ".$tp.".size_chart,
			".$tp.".page_keywords, ".$tp.".code, ".$tp.".prcode, ".$tp.".warehouse, ".$tp.".youtube, ".$tp.".units, ".$tp.".for_unit, ".$tp.".price2, ".$tp.".for_unit2,
			".$tp.".color, ".$tp.".rating_sum, ".$tp.".rating_count
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tp.".id='".$pid."' AND ".$tc2p.".prod_id='".$pid."' AND ".$tp.".enabled='1'
		");
   /* $query = "SELECT ".$tp.".id FROM ".$tp." WHERE ".$tp.".id='4'";
	$ress = $sql->query($query);
	$sql->fetch();
	$za = $sql->Record;
	$sql->close();
	var_dump($test);
	exit;*/
	if (!mysql_num_rows($res)) return 0;

	$sql->fetch();
	$z = $sql->Record;
	$sql->close();
    //var_dump($z[cat_id]); exit;
	if (!$cat = $this->GetCatByID($z[cat_id])) return 0;
	$z[cat_name] = $cat[name];
	$z[cat_short_name] = $cat[short_name];

	$sql0 = new SQLClass();
	$sql0->query("SELECT name, country FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
	$sql0->fetch();
	$z0 = $sql0->Record;
	$sql0->close();
	if (!$z0[name]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[name];
	$z[vendor_name_country] = $z0[country];

	return $z;
}

################################################################# список товаров
################################################################# по случайному выбору
function GetRandomProducts($count) {
	global $tableCollab, $MESSAGES;

	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tc2p.".cat_id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders, MD5( RAND()*NOW() ) AS myRandom
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".prod_id=".$tp.".id
			ORDER BY myRandom
			LIMIT ".$count."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[title]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[title];

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["series"]." WHERE id='".$z[series]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		$z[series_name] = $z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# список популярных товаров
################################################################# в случайном порядке
function GetPopularProducts($count) {
	global $tableCollab, $MESSAGES;

	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT id, vendor, name, code, prcode, descr, full_descr, price, url,
			       old_price, views, orders, enabled, warehouse, units, for_unit, descr, MD5( RAND()*NOW() ) AS myRandom
			FROM ".$tp."
			WHERE enabled='1' AND f_popular='1'
			ORDER BY myRandom
			LIMIT ".$count."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

   		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[name]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# список новых товаров
################################################################# в случайном порядке
function GetNewProducts($count) {
	global $tableCollab, $MESSAGES;

	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT id, vendor, name, code, prcode, descr, full_descr, price, url,
			       old_price, views, orders, enabled, warehouse, units, for_unit, descr, MD5( RAND()*NOW() ) AS myRandom
			FROM ".$tp."
			WHERE enabled='1' AND f_new='1'
			ORDER BY myRandom
			LIMIT ".$count."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

   		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[name]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# список мнений о товаре
################################################################# по ид товара
function GetProductReviews($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT time, user_id, name, email, text, rating FROM ".$tableCollab["products_reviews"]." WHERE pid='".$pid."' AND enabled='1' ORDER BY time");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# добавить отзыв о товаре
################################################################# по ид товара
function AddProductReview($pid, $rating, $user_id, $name, $email, $text) {
	global $tableCollab;

	if ( (strlen($name)) && (strlen($email)) && (strlen($text)) ) {
		$sql = new SQLClass();
		$sql->query("INSERT INTO ".$tableCollab["products_reviews"]."(pid, time, user_id, name, email, text, rating)
					 VALUES('".$pid."', '".time()."', '".$user_id."', '".$name."', '".$email."', '".$text."', '".$rating."')");
		$sql->close();

		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["products"]." SET rating_sum = rating_sum + ".$rating.", rating_count = rating_count + 1
					 WHERE id='".$pid."'");
		$sql->close();
	}
}


################################################################# Проверить был ли отзыв
################################################################  для товара
function CheckReview($pid, $user_id, $name){
    global $tableCollab;


        $sql = new SQLClass();

        $res = $sql->query("SELECT * FROM ".$tableCollab["products_reviews"]." WHERE pid='$pid' AND name='$name'");
        $z=$sql->fetch();
        $sql->close();

        if($z)
        {
            return false;
        }
        else {
            return true;
        }



}

################################################################# берем последние
################################################################# новинки
function GetNoveltyProducts($count) {
	global $tableCollab;

	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["cats2prods"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tc2p.".cat_id, ".$tp.".vendor, ".$tp.".name, ".$tp.".prcode, ".$tp.".code, ".$tp.".url, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tp.".enabled='1' AND ".$tc2p.".prod_id=".$tp.".id
			ORDER BY ".$tp.".time_create DESC
			LIMIT ".$count."
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[title]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[title];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# берем товары
################################################################# которые надо купить
################################################################# для этой категории
function GetDontForgetProductsByCID($cid) {
	global $tableCollab, $MESSAGES;

	if (!$this->GetCatByID($cid)) return 0;
	$q = array();

	$tp = $tableCollab["products"];
	$tc2p = $tableCollab["products2cats"];
	$tc2p_ = $tableCollab["cats2prods"];
	$ts = $tableCollab["series"];

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT ".$tp.".id, ".$tp.".vendor, ".$tp.".name, ".$tp.".descr, ".$tp.".full_descr, ".$tp.".price, ".$tp.".old_price, ".$tp.".views, ".$tp.".orders
			FROM ".$tp.", ".$tc2p."
			WHERE ".$tc2p.".cat_id='".$cid."' AND ".$tc2p.".product_id=".$tp.".id
			ORDER BY ".$tp.".views, ".$tp.".price
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["vendors"]." WHERE id='".$z[vendor]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		if (!$z0[title]) $z[vendor_name] = $MESSAGES["vendor_noname"]; else $z[vendor_name] = $z0[title];

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["series"]." WHERE id='".$z[series]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		$z[series_name] = $z0[name];

		$sql0 = new SQLClass();
		$sql0->query("SELECT cat_id FROM ".$tc2p_." WHERE prod_id='".$z[id]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		$z[cat_id] = $z0[cat_id];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# поиск товаров
################################################################# по ключевым словам
################################################################# и возможно выбранной категории
function SearchInProducts($find0) {
	global $tableCollab, $MESSAGES;

	######	ну а теперь сами продукты
	$q = array();
	$tp = $tableCollab["products"];
	$tc = $tableCollab["cats2prods"];

	$find	=	explode(" ", $find0);

	$where_find = "";
	for ($i=0; $i<count($find); $i++) {
		$where_find .= "AND (name LIKE '%".addslashes($find[$i])."%' OR descr LIKE '%".addslashes($find[$i])."%' OR full_descr LIKE '%".addslashes($find[$i])."%' OR page_keywords LIKE '%".addslashes($find[$i])."%' OR code LIKE '%".addslashes($find[$i])."%')";
	}

	$sql = new SQLClass();
	$res = $sql->query("
			SELECT DISTINCT($tp.id)
			FROM $tp, $tc
			WHERE $tp.enabled='1' AND $tc.prod_id=$tp.id ".$where_find."
			ORDER BY $tp.stock DESC, $tp.views DESC, $tp.name
		");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# взять список всех ID категорий, в которых находится товар
function GetCatsOfProduct($pid) {
	global $tableCollab, $MESSAGES;

	$q	=	array();
	$sql = new SQLClass();
	$res = $sql->query("SELECT cat_id FROM ".$tableCollab["cats2prods"]." WHERE prod_id='".$pid."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$q[count($q)]	=	$z['cat_id'];
	}
	$sql->close();

	return $q;
}

################################################################# Добавить новую категорию, в которой присутствует товар
function AddCatOfProduct($pid, $cat) {
	global $tableCollab, $MESSAGES;

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["cats2prods"]." (cat_id, prod_id) VALUES ('".$cat."', '".$pid."')");
	$sql->close();
}

################################################################# Удалить категорию в которой присутствует товар
function DelCatOfProduct($pid, $cat) {
	global $tableCollab, $MESSAGES;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["cats2prods"]." WHERE cat_id='".$cat."' AND prod_id='".$pid."'");
	$sql->close();
}

################################################################# список свойств
#################################################################
function GetProperties() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["properties"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}


################################################################# Добавить новое свойство
#################################################################
function AddProperty($name) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["properties"]." (name) VALUES ('".$name."')");
	$sql->close();
}

################################################################# Добавить новое свойство
################################################################# со значением для товара
function AddPropertyValue($pid, $property, $value) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' AND property_id='".$property."'");
	$sql->close();

	$sql = new SQLClass();
	$sql->query("INSERT INTO ".$tableCollab["prop2prod"]." (product_id, property_id, seq, value, enabled) VALUES ('".$pid."', '".$property."', '".time()."', '".$value."', '1')");
	$sql->close();
}

################################################################# Выбрать свойства
################################################################# и значения для товара
function GetPropertiesValues($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT property_id, value, seq FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' ORDER BY seq");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$sql0 = new SQLClass();
		$sql0->query("SELECT name FROM ".$tableCollab["properties"]." WHERE id='".$z[property_id]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();
		$z[property_name]	=	$z0[name];

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

################################################################# Выбрать свойства
################################################################# и значения для товара
function GetProductPropertyValue($pid, $property) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT value, seq FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' AND property_id='".$property."'");
	$sql->fetch();
	$z = $sql->Record;

	$sql0 = new SQLClass();
	$sql0->query("SELECT name FROM ".$tableCollab["properties"]." WHERE id='".$property."'");
	$sql0->fetch();
	$z0 = $sql0->Record;
	$sql0->close();
	$z[property_name]	=	$z0[name];
	$sql->close();

	return $z;
}

################################################################# Выбрать свойства
################################################################# и значения для товара
function SavePropertyValue($pid, $property, $value) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("UPDATE ".$tableCollab["prop2prod"]." SET value='".$value."' WHERE product_id='".$pid."' AND property_id='".$property."'");
	$sql->close();
}

################################################################# Выбрать свойства
################################################################# и значения для товара
function DelProductProperty($pid, $property) {
	global $tableCollab;

	$sql = new SQLClass();
	$sql->query("DELETE FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' AND property_id='".$property."'");
	$sql->close();
}

####################################################################################
function UpdatePropertySeq($pid, $id, $what) {
	global $tableCollab;

	$_this = $this->GetProductPropertyValue($pid, $id);

	if ($what == "up") $tmp_seq = "AND seq<'".$_this[seq]."' ORDER BY seq DESC"; else $tmp_seq = "AND seq>'".$_this[seq]."' ORDER BY seq";

	$sql = new SQLClass();
	$res = $sql->query("SELECT property_id, seq FROM ".$tableCollab["prop2prod"]." WHERE product_id='".$pid."' ".$tmp_seq." LIMIT 1");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	if ($z[property_id]) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["prop2prod"]." SET seq='".$z[seq]."' WHERE property_id='".$id."' AND product_id='".$pid."'");
		$sql->query("UPDATE ".$tableCollab["prop2prod"]." SET seq='".$_this[seq]."' WHERE property_id='".$z[property_id]."' AND product_id='".$pid."'");
		$sql->close();
	}
}

####################################################################################
function UploadPricelist($img_dir, $img) {
	copy($img[tmp_name], $img_dir);
	chmod($img_dir, 0666);
}

####################################################################################
function SaveProductColor($id, $name, $color) {
	global $tableCollab;

	if ($id > 0) {
		$sql = new SQLClass();
		$sql->query("UPDATE ".$tableCollab["products_colors"]." SET name='".$name."', color='".$color."' WHERE id='".$id."'");
		$sql->close();
	} else {
		$sql = new SQLClass();
		$sql->query("INSERT INTO ".$tableCollab["products_colors"]."(name, color) VALUES('".$name."', '".$color."')");
		$sql->close();
	}
}

####################################################################################
function GetProductColors() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name, color FROM ".$tableCollab["products_colors"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetProductColorsArray() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id FROM ".$tableCollab["products_colors"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z["id"];
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetProductColor($color) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name, color FROM ".$tableCollab["products_colors"]." WHERE id='".$color."'");
	$sql->fetch();
	$z = $sql->Record;
	return $z;
}

####################################################################################
function GetProductSize($size) {
	global $tableCollab;

	$sql = new SQLClass();
	$res = $sql->query("SELECT size_group, name FROM ".$tableCollab["size_line"]." WHERE id='".$size."'");
	$sql->fetch();
	$z = $sql->Record;
	return $z;
}

###################################################################################
function UpdateLastEnter($thisEmail){
    global $tableCollab;

    $q = array();

    $sql = new SQLClass();
    $res = $sql->query("UPDATE ".$tableCollab['customers']."  SET lastlogin = '".date("Y-m-d H:i:s")."' WHERE `email` LIKE ('".$thisEmail."') ");
}

####################################################################################
function GetProductColorList($pid) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT color FROM ".$tableCollab["products2colors"]." WHERE pid='".$pid."' ORDER BY seq");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z["color"];
	}
	$sql->close();

	return $q;
}

#####################################################################################
##################################  получить цвета товара из таблички склада -
##################################  warehouse (перепроверка products2colors)
#######################################################################################
function GetColorsWare($pid){
    global $tableCollab;

    $q = array();
    $sql = new SQLClass();

    $res = $sql->query("SELECT ".$tableCollab["products2colors"].".`color` FROM ".$tableCollab["products2colors"].",".$tableCollab['products_colors']." WHERE pid='".$pid."' and id=".$tableCollab["products2colors"].".color and ".$tableCollab['products_colors'].".color<>'' ORDER BY seq");
    if ($res){
        for ($i=0; $i<mysql_num_rows($res); $i++) {
            $sql->fetch();
            $z = $sql->Record;
            $q[count($q)] = $z["color"];
        }

    }

    return $q;
}

###############################################################################################
##################################  получить ДОПОЛНИТЕЛЬНЫЕ цвета товара из таблички склада -
##################################  warehouse (перепроверка products2colors)
################################################################################################
function GetAdtnlColors($pid){
    global $tableCollab;

    $q = array();
    $sql = new SQLClass();

    $res = $sql->query("SELECT ".$tableCollab["products2colors"].".`color` FROM ".$tableCollab["products2colors"].",".$tableCollab['products_colors']." WHERE pid='".$pid."' and id=".$tableCollab["products2colors"].".color and ".$tableCollab['products_colors'].".color='' ");
    if ($res){
        for ($i=0; $i<mysql_num_rows($res); $i++) {
            $sql->fetch();
            $z = $sql->Record;
            $q[count($q)] = $z["color"];
        }

    }

    return $q;
}

####################################################################################
function GetSizeGroups() {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["size_group"]." ORDER BY name");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetSizeGroupByID($group_id) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["size_group"]." WHERE id='".$group_id."'");
	$sql->fetch();
	$z = $sql->Record;
	$sql->close();

	return $z;
}

####################################################################################
function GetSizeLine($group) {
	global $tableCollab;

	$q = array();

	$sql = new SQLClass();
	$res = $sql->query("SELECT id, name FROM ".$tableCollab["size_line"]." WHERE size_group='".$group."' ORDER BY seq");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

		$q[count($q)] = $z;
	}
	$sql->close();

	return $q;
}

####################################################################################
function GetProductWarehouse($pid, $color, $size) {
	global $tableCollab;
	$sql = new SQLClass();
	$res = $sql->query("SELECT pid FROM ".$tableCollab["products_warehouse"]." WHERE pid='".$pid."' AND color='".$color."' AND size='".$size."'");
	$c	=	mysql_num_rows($res);
	$sql->close();
	return $c;
}


######################################################## получить размеры для товара
######################################################## в виде id=>name
########################################################
function GetSizesArray($pid, $colors){
    global $tableCollab;
    $q = array();
    if(is_array($colors)){
    $color_in_str = implode(',', $colors);
    }
    else
    {
        $color_in_str = $colors;
    }

    if ($color_in_str)
        $wh = "AND color IN ($color_in_str)";
    else
        $wh = '';

    $pr_w = $tableCollab["products_warehouse"];

    $sql = new SQLClass();
    $res = $sql->query("SELECT `color` as id, `size` AS name FROM ".$pr_w." WHERE pid='".$pid."' $wh");
     while($row = mysql_fetch_assoc(($res))){
        $q[count($q)] = $row;
        $arl[] = $row['name'];
    }

    if (count($q)) {

    		$in	=	implode(',', $arl);
    		unset($q);
    		$q	=	array();

    		$res = $sql->query("SELECT id, name FROM ".$tableCollab["size_line"]." WHERE id IN (".$in.") ORDER BY seq");
    		for ($i=0; $i<mysql_num_rows($res); $i++) {
    			$sql->fetch();
    	   		$z0 = $sql->Record;;
    	   		$q[count($q)] = $z0;
    	   	}
    		$sql->close();
    	}

    return $q;

}

function GetSizesArray_back($pid, $colors, $size_group){
    global $tableCollab;
    $q = array();
    if(is_array($colors)){
    $color_in_str = implode(',', $colors);
    }
    else
    {
        $color_in_str = $colors;
    }

    if ($color_in_str)
        $wh = "AND ".$tableCollab["products_warehouse"].".color IN ($color_in_str)";
    else
        $wh = '';

    $pr_w    = $tableCollab["products_warehouse"];
//    $sg      = $tableCollab["sizes_group"];
    $sl      = $tableCollab["size_line"];


    $sql = new SQLClass();

    $res = $sql->query("
        SELECT ".$pr_w.".`color` as id, ".$sl.".`name` AS name
        FROM ". $pr_w .", ". $sl ."
        WHERE pid='".$pid."' AND ". $sl .".`size_group`='".$size_group."'  AND ". $sl .".`id` = ". $pr_w .".`size` $wh
        ");

     while($row = mysql_fetch_assoc(($res))){
        $q[count($q)] = $row;
    }
    $sql->close();

    return $q;

}

######################################################## добавить заказ в базу данных
######################################################## перед окончательным подтверждением
########################################################
function InsertCustomerInfo($out_array){
    global $tableCollab;

    $sql = new SQLClass();

    // check for user on email
    $res = $sql->query("SELECT * FROM ".$tableCollab['customers']." WHERE `email` LIKE ('".$out_array['RegisterForm_email']."')");
    $sql->fetch();
    $z = $sql->Record;
    if(!$z){ //if client with email not exist - ok

        //generator of pass
            $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
            $max=10;
            // Определяем количество символов в $chars
            $size=StrLen($chars)-1;
            // Определяем пустую переменную, в которую и будем записывать символы.        $password=null;

            // Создаём пароль.
                while($max--)
                $password.=$chars[rand(0,$size)];

        $res = $sql->query("
        INSERT INTO ".$tableCollab['customers']."
        (id,
        login,
        name,
        family,
        password,
        company,
        city,
        street,
        house,
        apartment,
        building,
        section,
        entrance,
        level,
        code,
        metro,
        post_address,
        phone,
        fax,
        email,
        icq,
        skype,
        site,
        user_,
        user_boss,
        user_fin,
        user_marketing,
        time,
        status,
        newsletter,
        lastlogin,
        uniqid,
        whois,
        cash_payment,
        referer,
        organization,
        discount
        )
        VALUES
        (
        NULL,
        '".$out_array['BillingAddressForm_last_name']." ".$out_array['BillingAddressForm_first_name']."',
        '".$out_array['BillingAddressForm_first_name']."',
        '".$out_array['BillingAddressForm_last_name']."',
         '".$password."',
        NULL,
        '".$out_array['BillingAddressForm_city']."',
        '".$out_array['RegisterForm_street']."',
        '".$out_array['Billing_home']."',
        '".$out_array['Billing_flat']."',
        '".$out_array['Billing_stroenie']."',
        '".$out_array['Billing_korpus']."',
        '".$out_array['Billing_podezd']."',
        '".$out_array['Billing_floor']."',
        '".$out_array['Billing_code']."',
        '".$out_array['metro']."',

        ".$out_array['BillingAddressForm_postcode'].",
        '".$out_array['phone']."',
        NULL,
        '".$out_array['RegisterForm_email']."',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        '".date("Y-m-d H:i:s")."',
        1,
        NULL,
        NULL,
        '".mktime()."',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
        )
        ");

        if (!$res) return false;
        $id = mysql_insert_id();
    }
    else {
        $id = $z['id'];
    }
    
    $res = $sql->query("INSERT INTO ".$tableCollab['customers_delivery']."
     (id,
      customer_id,
      enabled,
      also)
      VALUES
      (
      NULL,
      '".$id."',
      '1',
      '".$out_array['note']."'
      )
     ");
    $id = mysql_insert_id();
    $sql->close();
    return $id;

}

######################################################## получить id
######################################################## последнего заказа из
######################################################## таблицы orders
function LastOrder(){
    global $tableCollab;

    $sql = new SQLClass();

    $res = $sql->query("SELECT `id`,`date` FROM ".$tableCollab['customers_delivery']." ORDER BY `id` DESC LIMIT 1");

    $sql->fetch();

    $z = $sql->Record;

    $sql->close();

    if($res)
        return $z;
    else
        return false;
}

######################################################## после нажатия заказать
######################################################## сделать enable в таблице
######################################################## sf_fo_customers_delivery
function LastDeliveryEnable(){
    global $tableCollab;

    $sql = new SQLClass();

    $res = $sql->query("UPDATE ".$tableCollab['customers_delivery']." SET `enabled`=0 ORDER BY `id` DESC LIMIT 1");

    $sql->close();

    if($res)
        return true;
    else
        return false;
}

######################################################## вставить содержимое заказа
######################################################## в таблице orders
########################################################
function InsertOrder($basket_arr, $order_number){
    global $tableCollab;

    $sql = new SQLClass();
    $multi_insert = '';

    if(is_array($basket_arr)){

        foreach($basket_arr as $key=>$value){
            if($value != ''){

                $pid = explode(".",$value);
                $res = $sql->query("SELECT `price` FROM ".$tableCollab['products']." WHERE `id`='".$pid[0]."'");
                $sql->fetch();
                $z = $sql->Record;
                $multi_insert .= '(';
                $multi_insert .= $order_number.',';
                $multi_insert .= str_replace('.', ',', $value);
                $multi_insert .= ", ".$z['price'].')';

                if((count($basket_arr)-2) != $key){
                    $multi_insert .= ',';
                }
            }
        }
    }

    if($multi_insert != ''){
        $res = $sql->query("INSERT INTO ".$tableCollab['orders']." (id_deliv, pid, color, size, count_pid, price) VALUES ".$multi_insert."");
    }

//    $count_affected = $sql->affected_rows();
//    $res2 = $sql->query("UPDATE ".$tableCollab['orders']." as `orders`, ".$tableCollab['products']." as `products`
//    SET `orders`.`price` = `products`.`price`
//    WHERE `orders`.`pid` = `products`.`id`
//    ORDER BY `orders`.`id_deliv`
//    LIMIT ".$count_affected." DESC
//    ");

    $sql->close();

    if($res) return true;
        else return false;
}
######################################################## получить размеры из интерфейса (ajax)
######################################################## в виде id=>name
########################################################
function GetSizes_one($pid, $colors){
    global $tableCollab;

    $q = array();

    $sql = new SQLClass();
//    $q = array();
    $res = $sql->query("SELECT `size` FROM ".$tableCollab['products_warehouse']." WHERE pid='".$pid."' AND color='".$colors."'");
    for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
		$z = $sql->Record;
		$q[count($q)] = $z['size'];
	}

    if (count($q)) {

        $in	=	implode(',', $q);
        unset($q);
        $qa	=	array();

        $res = $sql->query("SELECT  name,id FROM ".$tableCollab["size_line"]." WHERE id IN (".$in.") ORDER BY seq");

        for ($i=0; $i<mysql_num_rows($res); $i++) {
            $sql->fetch();
            $z0 = $sql->Record;;
            $qa[count($qa)] = $z0;
        }

    }
	$sql->close();

    return $qa;

}



####################################################################################
function GetProductWarehouseSizes($pid, $color) {
	global $tableCollab;
	$sql = new SQLClass();
	$res = $sql->query("SELECT size FROM ".$tableCollab["products_warehouse"]." WHERE pid='".$pid."' AND color='".$color."'");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;

   		$sql0 = new SQLClass();
		$sql0->query("SELECT id, name FROM ".$tableCollab["size_line"]." WHERE id='".$z["size"]."'");
		$sql0->fetch();
   		$z0 = $sql0->Record;
		$sql0->close();

		$q[count($q)] = $z0;
	}
	$sql->close();
	return $q;
}

#################################################
function GetBlogTopItems(){
    global $tableCollab;
    // var_dump($tableCollab);
    $sql = new SQLClass();
    $res = $sql->query("SELECT * FROM ".$tableCollab["blog"]." ORDER BY id DESC LIMIT 5");
    while($row = mysql_fetch_assoc(($res))){
        $blogTable[$row['id']] = $row;
    }
    return $blogTable;

}

##############################################################
function GetMostPopular(){
    global $tableCollab;

    $mp = array();
    $sql = new SQLClass();
    //"SELECT * FROM ".$tableCollab["products"].", ".$tableCollab["vendors"]." WHERE 'sf_fo_vendors.id' = 'sf_fo_vendors.vendor' ORDER BY `views` DESC LIMIT 3"
    $res = $sql->query("SELECT DISTINCT a.id, a.seq, a.rating_sum, a.price, a.old_price, a.rating_count, a.code, a.vendor, a.color, a.name, a.url
            FROM ".$tableCollab["products"]." AS a, ".$tableCollab["cats2prods"]." AS b, ".$tableCollab["categoryes"]." AS c
            WHERE a.id = b.prod_id
            AND b.cat_id = c.id
            AND c.enabled = '1'
            ORDER BY `a`.`price` desc
            LIMIT 0 , 6");


    //SELECT a.id, c.url FROM sf_fo_products as a, sf_fo_cats2prods as b, sf_fo_categories as c WHERE a.id=b.prod_id AND b.cat_id=c.id AND a.id='8';

    //SELECT c.url FROM sf_fo_cats2prods as b, sf_fo_categories as c WHERE c.enabled='1' AND b.prod_id='8' AND b.cat_id=c.id LIMIT 1;
    while($row = mysql_fetch_assoc($res)){
        //var_dump($row['id']);
        $mp[] = $row;

    }
    return $mp;

}

####################################################################################
function GetProductWarehouseSizes_2($pid) {
	global $tableCollab;
	$q	=	array();


	$sql = new SQLClass();
	$res = $sql->query("SELECT DISTINCT(size) FROM ".$tableCollab["products_warehouse"]." WHERE pid='".$pid."' ");
	for ($i=0; $i<mysql_num_rows($res); $i++) {
		$sql->fetch();
   		$z = $sql->Record;
   		$q[count($q)] = $z[0];
	}
	$sql->close();


	if (count($q)) {

		$in	=	implode(',', $q);
		unset($q);
		$q	=	array();

		$res = $sql->query("SELECT id, name FROM ".$tableCollab["size_line"]." WHERE id IN (".$in.") ORDER BY seq");
		for ($i=0; $i<mysql_num_rows($res); $i++) {
			$sql->fetch();
	   		$z0 = $sql->Record;;
	   		$q[count($q)] = $z0;
	   	}
		$sql->close();
	}

	return $q;
}

####################################################################################
function GetProductsWarehouseSizes($pids) {
	global $tableCollab;
	$q	=	array();
	if (count($pids)) {
		$in = array();
		for ($i=0; $i<count($pids); $i++) {
  			$in[count($in)]	=	$pids[$i]["id"];
		}
		$in	=	implode(',', $in);

		$sql = new SQLClass();
		$res = $sql->query("SELECT DISTINCT(size) FROM ".$tableCollab["products_warehouse"]." WHERE pid IN (".$in.")");
		for ($i=0; $i<mysql_num_rows($res); $i++) {
			$sql->fetch();
   			$z = $sql->Record;
	   		$q[count($q)] = $z[0];
		}
		$sql->close();

		if (count($q)) {
			$in	=	implode(',', $q);
			unset($q);
			$q	=	array();

			$sql0 = new SQLClass();
			$res = $sql0->query("SELECT id, size_group, name FROM ".$tableCollab["size_line"]." WHERE id IN (".$in.") ORDER BY seq");
			for ($i=0; $i<mysql_num_rows($res); $i++) {
				$sql0->fetch();
		   		$z0 = $sql0->Record;
		   		$q[$z0[1]][count($q[$z0[1]])] = $z0;
		   	}
			$sql0->close();
		}
	}
	return $q;
}

####################################################################################
function GetProductsVendors($pids) {
	global $tableCollab;
	$q	=	array();
	if (count($pids)) {
		$in = array();
		for ($i=0; $i<count($pids); $i++) {
  			$in[count($in)]	=	$pids[$i]["id"];
		}
		$in	=	implode(',', $in);

		$sql = new SQLClass();
		$res = $sql->query("SELECT DISTINCT(vendor) FROM ".$tableCollab["products"]." WHERE id IN (".$in.")");
		for ($i=0; $i<mysql_num_rows($res); $i++) {
			$sql->fetch();
   			$z = $sql->Record;
	   		if ($z[0] != 0) $q[count($q)] = $z[0];
		}
		$sql->close();
//        var_dump($q); exit;
		$in	=	implode(',', $q);
		$q	=	array();

		$sql = new SQLClass();
		$res = $sql->query("SELECT id, name FROM ".$tableCollab["vendors"]." WHERE id IN (".$in.") ORDER BY name");
//        echo "SELECT id, name FROM ".$tableCollab["vendors"]." WHERE id IN (".$in.") ORDER BY name"; exit;
		for ($i=0; $i<mysql_num_rows($res); $i++) {
			$sql->fetch();
   			$z = $sql->Record;
	   		$q[count($q)] = $z;
		}
		$sql->close();
	}

	return $q;
}

####################################################################################
function GetProductsPriceRange($pids) {
	global $tableCollab;
	$q	=	array();
	if (count($pids)) {
		$in = array();
		for ($i=0; $i<count($pids); $i++) {
  			$in[count($in)]	=	$pids[$i]["id"];
		}
		$in	=	implode(',', $in);
		$sql = new SQLClass();
		$res = $sql->query("SELECT MIN(price), MAX(price) FROM ".$tableCollab["products"]." WHERE id IN (".$in.")");
		$sql->fetch();
		$z = $sql->Record;
		$sql->close();
		$q["min"]	=	$z[0];
		$q["max"]	=	$z[1];
	}
	return $q;
}

####################################################################################
function GetProductsColors($pids) {
	global $tableCollab;
	$q	=	array();

	if (count($pids)) {

		$in = array();
		for ($i=0; $i<count($pids); $i++) {
  			$in[count($in)]	=	$pids[$i]["id"];
		}
		$in	=	implode(',', $in);

		$sql = new SQLClass();
//        echo "SELECT ".$tableCollab["products2colors"].".`color` FROM ".$tableCollab["products2colors"].",".$tableCollab['products_colors']." WHERE pid  IN (".$in.") and id=".$tableCollab["products2colors"].".color and ".$tableCollab['products_colors'].".color<>''"; exit;
        $res = $sql->query("SELECT ".$tableCollab["products2colors"].".`color` FROM ".$tableCollab["products2colors"].",".$tableCollab['products_colors']." WHERE pid  IN (".$in.") and id=".$tableCollab["products2colors"].".color and ".$tableCollab['products_colors'].".color<>''");
		for ($i=0; $i<mysql_num_rows($res); $i++) {
			$sql->fetch();
   			$z = $sql->Record;
	   		$q[count($q)] = $z[0];
		}
		$sql->close();


		$in	=	implode(',', $q);
		$q	=	array();
        if($in != ''){
            $sql = new SQLClass();
            $res = $sql->query("SELECT id, name, color FROM ".$tableCollab["products_colors"]." WHERE id IN (".$in.") ORDER BY name");
            for ($i=0; $i<mysql_num_rows($res); $i++) {
                $sql->fetch();
                $z = $sql->Record;
                $q[count($q)] = $z;
            }
        }
		$sql->close();

	}

	return $q;
}

####################################################################################
function ProductsDoFilters($pids, $price=0, $size=0, $color=0, $vendor=0) {
	global $tableCollab;
	$q	=	$pids;

	if (count($pids)) {
		$in	=	array();
		$or	=	array();
		for ($i=0; $i<count($pids); $i++) {
  			$in[count($in)]	=	$pids[$i]["id"];
  			$or[count($or)]	=	$pids[$i]["id"];
		}
		$in	=	implode(',', $in);

		///////////////////////////////////	есть фильтр по цене
		$pq	=	$or;
		if (strlen($price["0"]) && strlen($price["1"])) {
			$pq	=	array();
			$sql = new SQLClass();
			$res = $sql->query("SELECT DISTINCT(id) FROM ".$tableCollab["products"]." WHERE id IN (".$in.") AND (price >= ".$price["0"]." AND price <= ".$price["1"].")");
			for ($i=0; $i<mysql_num_rows($res); $i++) {
				$sql->fetch();
   				$z = $sql->Record;
	   			$pq[count($pq)] = $z[0];
			}
			$sql->close();
		}

		///////////////////////////////////	есть фильтр по размеру
		$sq	=	$or;
		if ($size) {
			$sq	=	array();
			$sql = new SQLClass();
			$res = $sql->query("SELECT DISTINCT(pid) FROM ".$tableCollab["products_warehouse"]." WHERE pid IN (".$in.") AND size = '".$size."'");
			for ($i=0; $i<mysql_num_rows($res); $i++) {
				$sql->fetch();
   				$z = $sql->Record;
	   			$sq[count($sq)] = $z[0];
			}
			$sql->close();
		}


		///////////////////////////////////	есть фильтр по цвету
		$cq	=	$or;
		if ($color) {
			$cq	=	array();
			$sql = new SQLClass();
            $res = $sql->query("SELECT DISTINCT(".$tableCollab["products2colors"].".`pid`) FROM ".$tableCollab["products2colors"].",".$tableCollab['products_colors']." WHERE pid  IN (".$in.") and id=".$tableCollab["products2colors"].".color and ".$tableCollab['products_colors'].".color<>'' and ".$tableCollab["products2colors"].".color='".$color."' ");
//			$res = $sql->query("SELECT DISTINCT(id) FROM ".$tableCollab["products"]." WHERE id IN (".$in.") AND color = '".$color."'");
			for ($i=0; $i<mysql_num_rows($res); $i++) {
				$sql->fetch();
   				$z = $sql->Record;
	   			$cq[count($cq)] = $z[0];
			}
			$sql->close();
		}

		///////////////////////////////////	есть фильтр по производителю
		$vq	=	$or;
		if ($vendor) {
			$vq	=	array();
			$sql = new SQLClass();
			$res = $sql->query("SELECT DISTINCT(id) FROM ".$tableCollab["products"]." WHERE id IN (".$in.") AND vendor = '".$vendor."'");
			for ($i=0; $i<mysql_num_rows($res); $i++) {
				$sql->fetch();
   				$z = $sql->Record;
	   			$vq[count($vq)] = $z[0];
			}
			$sql->close();
		}

		//print_r($or); print_r($pq); print_r($sq); print_r($cq); print_r($vq);

		$fil	=	array_intersect($or, $pq, $sq, $cq, $vq);
		//////////////////////////////////////////	приводим массив ID в нужный формат
		$q		=	array();
		$i	=	0;
		foreach ($fil as $key => $val) {
			$q[$i][0]		=	$val;
			$q[$i]["id"]	=	$val;
			$i++;
		}
	}

	return $q;
}

}
?>