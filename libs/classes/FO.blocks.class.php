<?
class BlocksClass {

function BlocksClass() {
}

################################################################# �������� (���������)
################################################################# �����-���� ����
function HideBlock($parent_block, $block_name) {
	global $t;

	@$t->set_block($parent_block, $block_name, "_".$block_name);
	@$t->set_var($block_name, "");
	@$t->parse("_".$block_name, $block_name);
//     if($block_name == "is_tags"){
//        die(var_dump($t));
//    }
}

################################################################# �������� ����� � ���������
################################################################# ������� �� ����� ������
function HideBlocks($what, $blocks, $parent) {
	global $t;

	$bl = new BlocksClass();
	for ($i=0; $i<count($blocks); $i++) {
		if ($what!=$blocks[$i]) $bl->HideBlock($parent, $parent."_".$blocks[$i]);
	}
}

################################################################# ������� ���� �� �������
function BlogOnMain(){
    global $t;

    $t->set_file(array("header_blog" => "header_blog.tpl.htm"
                        ));


    $cp = new CATS_AND_PRODUCTSClass();

//    $blog_items = $cp->GetBlogTopItems();

//    $blog_out = "<div id='twitter' class='blogbody'>";

//    foreach ($blog_items as $value){
//        $blog_out .=
//        "<div class='blogitem'>
//            <div class='dateblog'>".$value['date']."</div>";
//            $blog_out .= "<div class='prevblog'>".$value['preview']."</div>
//        </div>";
//
//    }

//    $blog_out .= '</div>';

//    $t->set_var(array(
//        "BLOG_BODY" => $blog_out
//                ));
     $t->parse("HEADER_BLOG", "header_blog");
}

#################################################################   ���������� �������� ���������� ������ �� �������
function MostPopular(){
    global $t;
    $cp = new CATS_AND_PRODUCTSClass();

    $most_popular = $cp->GetMostPopular();
//    echo "<pre>", var_dump($most_popular), "</pre>";
    foreach ($most_popular as $key =>$value){

        if($value['rating_count'] != 0){
        $rating_cur = round(($value['rating_sum']/$value['rating_count']) * 20);}
        else
        {
            $rating_cur = 0.001;
        }
        $sizes	=	$cp->GetProductWarehouseSizes_2($value["id"]);
###
        $t->set_block("index", "av_sizes_".$key , "_av_sizes_".$key );
        if (!count($sizes)) {
            $hide_buy_button	=	"display: none;";
            $t->set_var("_av_sizes_".$key , "��� � �������");
        } else {

            $hide_buy_button	=	"";
            for ($is=0; $is<count($sizes); $is++) {
                if ($is < (count($sizes)-1)) $av_sizes_point = " - "; else $av_sizes_point = "";
                $t->set_var(array(
                    "AV_SIZE_NAME_".$key 		=>	$sizes[$is]["name"],
                    "AV_SIZE_POINT_".$key 		=>	$av_sizes_point
                ));
                $t->parse("_av_sizes_".$key , "av_sizes_".$key , true);
            }
        }
###
        $url = urldecode($value['url']);
        $available_colors = $cp->GetColorsWare($value['id']);
        if(!in_array($value['color'],$available_colors)){
            $color_item = $available_colors[0];
        }
        else $color_item = $value['color'];

        if ($value['old_price'] == 0.00)
            $style = "nonOldPrice";
        else
            $style = "oldPrice";

        $t->set_var(array(
                          "PRODUCT_URL_".$key           => $value['url'],
                          "PRODUCT_ID_".$key            => $value['id'],
                          "PRODUCT_COLOR_".$key         => $color_item,
                          "PRODUCT_VENDOR_".$key        => $value['brand'],
                          "PRODUCT_NAME_".$key          => $value['name'],
                          "PRODUCT_RATING_".$key        => $rating_cur,
                          "PRODUCT_CODE_".$key          => $value['code'],
                          "PRODUCT_SIZE_".$key          => count($sizes),
                          "PRODUCT_PRICE_".$key         => number_format($value['price'], 0, ',', ' '),
                          "PRODUCT_OLD_PRICE_".$key     => number_format($value['old_price'], 0, ',', ' '),
                          "PRICE_OLD_STYLE_".$key       => $style
                          )
                     );

    }


}
    
#################################################################	���������� � ����� ���������� � �������
function MainCatsDrop() {
	global $t;

	$t->set_file(array(
    	"block_main_cats_drop"		=>	"block_main_cats_drop.tpl.htm"
	));

	$cp = new CATS_AND_PRODUCTSClass();

	$t->set_block("block_main_cats_drop", "main_cats_main_title", "_main_cats_main_title");
	$t->set_block("main_cats_main_title", "main_cats_sub_cats", "_main_cats_sub_cats");
	$main_cid	=	$cp->GetSimpleCatsList_2(177);
	for ($i=0; $i<count($main_cid); $i++) {
		if ($i < (count($main_cid)-1)) $main_cat_no_border = ""; else $main_cat_no_border = "border: 0px;";
		$t->set_var(array(
			"MAIN_CAT_NAME"			=>	$main_cid[$i]["short_name"],
			"MAIN_CAT_NO_BORDER"	=>	$main_cat_no_border,
            "MAIN_CAT_ID"           => $main_cid[$i]["id"]
		));

		$subcat_cid	=	$cp->GetSimpleCatsList($main_cid[$i]["id"]);
		for ($i0=0; $i0<count($subcat_cid); $i0++) {
			if ($i0 < (count($subcat_cid)-1)) $sub_cat_pointer = ","; else $sub_cat_pointer = "";
			$t->set_var(array(
				"SUB_CAT_POINT"			=>	$sub_cat_pointer,
				"SUB_CAT_URL"			=>	$subcat_cid[$i0]["url"],
				"SUB_CAT_NAME"			=>	$subcat_cid[$i0]["short_name"]
			));
	 		$t->parse("_main_cats_sub_cats", "main_cats_sub_cats", true);
		}

		$t->parse("_main_cats_main_title", "main_cats_main_title", true);
		$t->set_var("_main_cats_sub_cats", "");
	}

	$t->parse("BLOCK_MAIN_CATS_DROP", "block_main_cats_drop");
}


#################################################################	���������� ������� � ������
//function ShowFiltersRow() {
//	global $t, $_COOKIE, $found_all, $data;
//}

#################################################################	���������� ������� � ������
function ShowFiltersRow() {
	global $t, $_COOKIE, $found_all, $data;

	$cp = new CATS_AND_PRODUCTSClass();

	$t->set_file(array(
    	"block_filters_row"		=>	"block_filters_row.tpl.htm"
	));

	#############################################################################	������ �� ����
	$filter_price	=	$cp->GetProductsPriceRange($found_all);
    if(count($filter_price)>0){
        if ($data->GET["min"]) $_min	=	$data->GET["min"]; else $_min	=	$filter_price["min"];
        if ($data->GET["max"]) $_max	=	$data->GET["max"]; else $_max	=	$filter_price["max"];
        $t->set_var(array(
            "FILTER_PRICE_MIN"			=>	number_format($_min, 0, ',', ' '),
            "FILTER_PRICE_MAX"			=>	number_format($_max, 0, ',', ' '),
            "FILTER_PRICE_MIN_VALUE"	=>	$filter_price["min"],
            "FILTER_PRICE_MAX_VALUE"	=>	$filter_price["max"],
            "FILTER_PRICE_MIN_VALUE0"	=>	$_min,
            "FILTER_PRICE_MAX_VALUE0"	=>	$_max
        ));
    }
    else{
        $t->set_block("block_filters_row","price_global", "_hzd");
        $t->parse("_hzd", "hzd", true);
    }

	#############################################################################	������ �� �������
	$filter_sizes	=	$cp->GetProductsWarehouseSizes($found_all);
    if (count($filter_sizes)>0){
        $t->set_block("block_filters_row", "filter-sizes-groups", "_filter-sizes-groups");
        $t->set_block("filter-sizes-groups", "filter-sizes", "_filter-sizes");
        $ii = 0;
        foreach ($filter_sizes as $key => $val) {
            $gr	=	$cp->GetSizeGroupByID($key);
            $t->set_var(array(
                "FILTER_SIZE_GROUP_NAME"	=>		$gr["name"]
            ));

            for ($i=0; $i<count($filter_sizes[$key]); $i++) {
                if ($filter_sizes[$key][$i]["id"] == $data->GET["size"]) $filter_sizes_selected = "filters_selected_size"; else $filter_sizes_selected = "";
                $t->set_var(array(
                    "FILTER_SIZE_ITEM_NUM"			=>	$ii++,
                    "FILTER_SIZE_ITEM_ID"			=>	$filter_sizes[$key][$i]["id"],
                    "FILTER_SIZE_ITEM_NAME"			=>	$filter_sizes[$key][$i]["name"],
                    "FILTER_SIZE_ITEM_SELECTED"		=>	$filter_sizes_selected
                ));
                $t->parse("_filter-sizes", "filter-sizes", true);
            }

            $t->parse("_filter-sizes-groups", "filter-sizes-groups", true);
            $t->set_var("_filter-sizes", "");
	    }
    }
    else{

        $t->set_block("block_filters_row","size_global", "_hzd");
        $t->parse("_hzd", "hzd", true);
    }

	#############################################################################	������ �� �����
 	$t->set_block("block_filters_row", "filter-colors", "_filter-colors");
 	$filter_colors	=	$cp->GetProductsColors($found_all);

    if(count($filter_colors)>0){
        for ($i=0; $i<count($filter_colors); $i++) {
            if ($filter_colors[$i]["id"] == $data->GET["color"]) $filter_colors_selected = "filter_selected_color"; else $filter_colors_selected = "";
            $t->set_var(array(
                "FILTER_COLOR_ITEM_NUM"				=>	$i,
                "FILTER_COLOR_ITEM_ID"				=>	$filter_colors[$i]["id"],
                "FILTER_COLOR_ITEM_NAME"			=>	$filter_colors[$i]["name"],
                "FILTER_COLOR_ITEM_COLOR"			=>	$filter_colors[$i]["color"],
                "FILTER_COLOR_ITEM_SELECTED"		=>	$filter_colors_selected
            ));
            $t->parse("_filter-colors", "filter-colors", true);
        }
    }
    else{

            $t->set_block("block_filters_row","color_global", "_hzad");
            $t->parse("_hzad", "hzad", true);
        }

	#############################################################################	������ �� ������������
	$t->set_block("block_filters_row", "filter-vendors", "_filter-vendors");
	$filter_vendors	=	$cp->GetProductsVendors($found_all);
    if(count($filter_vendors)>0){
        for ($i=0; $i<count($filter_vendors); $i++) {
            if ($filter_vendors[$i]["id"] == $data->GET["vendor"]) $filter_vendors_selected = "background-position: 0% 100%;"; else $filter_vendors_selected = "";
            $t->set_var(array(
                "FILTER_VENDOR_ITEM_NUM"			=>	$i,
                "FILTER_VENDOR_ITEM_ID"				=>	$filter_vendors[$i]["id"],
                "FILTER_VENDOR_ITEM_NAME"			=>	$filter_vendors[$i]["name"],
                "FILTER_VENDOR_ITEM_SELECTED"		=>	$filter_vendors_selected
            ));
            $t->parse("_filter-vendors", "filter-vendors", true);
        }
    }
    else{
        $t->set_block("block_filters_row","vendor_global", "_hzad");
        $t->parse("_hzad", "hzad", true);
    }

	$t->parse("BLOCK_FILTERS_ROW", "block_filters_row");
}

#################################################################	���������� � ����� ���������� � �������
function ShowHeaderBasketInfo() {
	global $t, $_COOKIE;

	$t->set_file(array(
    	"ajax_header_basket"		=>	"ajax_header_basket.tpl.htm"
	));

	if (!$_COOKIE["basket"]) {
		$t->parse("BLOCK_HEADER_BASKET_INFO", "");
		return array();
	}

	$cp = new CATS_AND_PRODUCTSClass();
	$sum = 0;
 	$pr	=	explode("-", $_COOKIE["basket"]);
 	for ($i=0; $i<(count($pr)-1); $i++) {
		$z			=	explode(".", $pr[$i]);
		$product	=	$cp->GetProductByID($z[0]);
		$sum		+=	($product[price] * $z[3]);
		unset($product);
	}

	$t->set_var(array(
		"BASKET_ITEMS"		=>  (count($pr)-1),
		"BASKET_SUM"		=>	number_format($sum, 0, ',', ' ')
	));

	$t->parse("BLOCK_HEADER_BASKET_INFO", "ajax_header_basket");
	return array((count($pr)-1), $sum);
}

#################################################################	������� ������ ���������
function CatsRowBlock() {
	global $t, $_CAT_ID, $_CURRENT_CAT, $_default_images_dir_pricelist;

$t->set_file(array(
		"catsrow"	=>	"block_catsrow.tpl.htm"
	    ));
$t->set_block("catsrow", "catalog_cats_main", "_catalog_cats_main");
$t->set_block("catalog_cats_main", "catalog_cats_sub", "_catalog_cats_sub");
$t->set_block("catalog_cats_sub", "not_parent", "_not_parent");
$t->set_block("catalog_cats_sub", "yes_parent", "_yes_parent");
$t->set_block("yes_parent", "cats_level3", "_cats_level3");

$not_parent = $t->get_var("not_parent");
$yes_parent = $t->get_var("yes_parent");

$c_p = new CATS_AND_PRODUCTSClass();
$cats = $c_p->GetSimpleCatsList(0);
for ($i=0; $i<count($cats); $i++) {
	if ($_CAT_ID) {
		if ($_CAT_ID == $cats[$i][id]) $subcat_display	=	"block"; else $subcat_display	=	"none";
	} else $subcat_display	=	"block";

	$t->set_var(array(
		"CAT_ID"			=>	@$cats[$i][id],
		"CAT_NAME"			=>	@$cats[$i][short_name],
		"CAT_FULLNAME"		=>	@$cats[$i][name],
		"SUBCAT_DISPLAY"	=>	$subcat_display
    ));

	$cp0 = new CATS_AND_PRODUCTSClass();
	$cats0 = $cp0->GetSimpleCatsList(@$cats[$i][id]);
	if (!count($cats0)) $t->set_var("_catalog_cats_sub", "");
	for ($i0=0; $i0<count($cats0); $i0++) {
		$t->set_var(array(
			"SUBCAT_ID"			=>	@$cats0[$i0][id],
			"SUBCAT_URL"		=>	@$cats0[$i0][url],
			"SUBCAT_NAME"		=>	@$cats0[$i0][short_name],
			"SUBCAT_FULLNAME"	=>	@$cats0[$i0][name]
	    ));

      	if ($cp0->IsThisParentCat($cats0[$i0][id])) {
      		$t->set_var("yes_parent", $yes_parent);

      		$cats3 = $cp0->GetSimpleCatsList(@$cats0[$i0][id]);
      		if (!count($cats3)) $t->set_var("_cats_level3", "");
			for ($i3=0; $i3<count($cats3); $i3++) {
				$t->set_var(array(
					"SUBCAT3_ID"		=>	@$cats3[$i3][id],
					"SUBCAT3_URL"		=>	@$cats3[$i3][url],
					"SUBCAT3_NAME"		=>	@$cats3[$i3][short_name],
					"SUBCAT3_FULLNAME"	=>	@$cats3[$i3][name]
			    ));
			    $t->parse("_cats_level3", "cats_level3", true);
			}

      		$t->set_var("not_parent", "");
      	} else {
			$t->set_var("yes_parent", "");
			$t->set_var("not_parent", $not_parent);
      	}
      	$t->parse("_yes_parent", "yes_parent", true);
  		$t->parse("_not_parent", "not_parent", true);
  		$t->set_var("_cats_level3", "");


		$t->parse("_catalog_cats_sub", "catalog_cats_sub", true);
		$t->set_var("_yes_parent", "");
		$t->set_var("_not_parent", "");
	}
	$cp0 = null;

	$t->parse("_catalog_cats_main", "catalog_cats_main", true);
	$t->set_var("_catalog_cats_sub", "");
}

$t->set_var(array(
	"SELECT_CAT_ID"		=>	$_CURRENT_CAT
));

	///////////////////////////////////////////////////////////	���������
	$users 		= new UsersClass();
	$_userid	=	$users->UserInSystem();
	if (!$_userid) {
		$this->HideBlock("catsrow", "siderow_pricelist");
		$this->HideBlock("catsrow", "income-menu");
	} else {
		$t->set_var(array(
			"PRICE_DATE"		=>	date("d-m-Y", filectime($_default_images_dir_pricelist)),
			"PRICE_SIZE"		=>	round( (filesize($_default_images_dir_pricelist) / 1024), 2)
		));
	}
	///////////////////////////////////////////////////////////	//	���������

$t->parse("BLOCK_CATSROW", "catsrow");
}

}
?>