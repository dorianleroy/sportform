<?php
class XSS {

   var $GET = array();
   var $POST = array();

   function XSS(){   		/*   		foreach ($_GET as $key => &$val) {
			//$val = filter_input(INPUT_GET, $key);
			$_GET[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_MAGIC_QUOTES);
		}
		foreach ($_POST as $key => &$val) {
			$_POST[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_MAGIC_QUOTES);
		}
		print_r($_GET);
		*/
      $this->check_all_input();
   }

   function check_all_input(){
      if(isset($_GET) AND $_GET){
        $this->GET = $this->check_data_array($_GET);
      } else {
        global $HTTP_GET_VARS;
        if(isset($HTTP_GET_VARS) AND $HTTP_GET_VARS) {
          $this->GET = $this->check_data_array($HTTP_GET_VARS);
        }
      }

      if(isset($_POST) AND $_POST){
        $this->POST = $this->check_data_array($_POST);
      } else {
        global $HTTP_POST_VARS;
        if(isset($HTTP_POST_VARS) AND $HTTP_POST_VARS) {
          $this->POST = $this->check_data_array($HTTP_POST_VARS);
        }
      }
   }

   function xss_check($string){
      return htmlspecialchars($string);
   }

   function check_data_array($array){
      $keys = array_keys($array);
      foreach($keys as $key){
	 if (is_array($array[$key])) {
		$this->check_data_array($array[$key]);
	 }else{
         	$array[$key] = $this->xss_check($array[$key]);
 	 }
      }
      return $array;
   }

}
global $data;
$data=new XSS();
?>