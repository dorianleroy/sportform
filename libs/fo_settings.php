<?
## URL's
$links['base_url']					=	$_SERVER['HTTP_HOST'];

## SITE
$_default_images_dir				=	"/uploads/";
$_default_images_dir_texts			=	"images/texts/";
$_default_images_dir_photoalbum		=	"images/photoalbum/";
$_default_images_dir_clubs			=	"images/clubs/";
$_default_images_dir_schools		=	"images/schools/";
$_default_images_dir_news			=	"images/news/";
$_default_images_dir_tips			=	"images/tips/";
$_default_images_dir_watermarks		=	"images/watermarks/";
$_default_images_dir_categories		=	"categories/";
$_default_images_dir_products		=	$_default_images_dir."images/products/";
$_default_images_dir_pricelist		=	$_default_images_dir."pricelist/fortuna_pricelist.xls";

$_default_min_year					=	2000;


#########################################
$_default_products_per_page			=	120;

## cookies
$cookies_uniqid 	=	"sportform_fos";	//	имя куки, которая будет запоминать уникального посетителя
$cookies_uniqid_exp	=	3600*24*90;		//	время жизни куки уникального юзера

## month
$_months						=	array("", "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
?>