<?
Header("Content-Type: text/html; charset=utf-8");

## database parameters
define("MYDBSERVERTYPE","mysql");  ## valid values are: msql, mssql, mysql, oci8, odbc, oracle, pgsql, sybase, usql
define("MYSERVER","localhost");
define("MYLOGIN","root");
define("MYPASSWORD","");
define("MYDATABASE","sportform_db");

$_DB_tables_prefix						= "sf_";										///	������� ��� ������


## Database tables collaboration

$tableCollab["categoryes"]				= $_DB_tables_prefix."fo_categories";			///	��������� �������
$tableCollab["products"]				= $_DB_tables_prefix."fo_products";				///	������
$tableCollab["cats2prods"]				= $_DB_tables_prefix."fo_cats2prods";			///	� ����� ���������� ��������� ������
$tableCollab["vendors"]					= $_DB_tables_prefix."fo_vendors";				///	������ ��������������
$tableCollab["prop2prod"] 				= $_DB_tables_prefix."fo_prop2prod";			///	�������� ��� ������� ������
$tableCollab["properties"] 				= $_DB_tables_prefix."fo_properties";			///	������� �������
$tableCollab["products2alternative"]	= $_DB_tables_prefix."fo_products2alternative";	///	������� "�������������� �������"
$tableCollab["products2products"]		= $_DB_tables_prefix."fo_products2products";	///	������� "������������� ������"
$tableCollab["products_colors"]			= $_DB_tables_prefix."fo_products_colors";		///	����� �������
$tableCollab["products2colors"]			= $_DB_tables_prefix."fo_products2colors";		///	�������� ����� �������
$tableCollab["products_warehouse"]		= $_DB_tables_prefix."fo_products_warehouse";	///	������� ������ ������������� ����� � �������
$tableCollab["products_reviews"]		= $_DB_tables_prefix."fo_products_reviews";		///	������ � �������
$tableCollab["size_group"]				= $_DB_tables_prefix."fo_size_group";			///	������� ��������
$tableCollab["size_line"]				= $_DB_tables_prefix."fo_size_line";			///	������� ��������
$tableCollab["units"]					= $_DB_tables_prefix."fo_units";				///	������� ������ ���������

$tableCollab["customers"]				= $_DB_tables_prefix."fo_users";				///	������� ��������
 
$tableCollab["news"] 					= $_DB_tables_prefix."fo_news";					///	������� � �����-������ ��������
$tableCollab["news_cats"] 				= $_DB_tables_prefix."fo_news_cats";			///	��������� ��������

$tableCollab["orders"]					= $_DB_tables_prefix."fo_orders";				///	������� �������
$tableCollab["orders_items"]			= $_DB_tables_prefix."fo_orders_items";			///	������� ������� � ������
$tableCollab["orders_status"]			= $_DB_tables_prefix."fo_orders_status";		///	������� �������� �������
$tableCollab["payments"]				= $_DB_tables_prefix."fo_payments";				///	������� ������� ������
$tableCollab["deliverys"]				= $_DB_tables_prefix."fo_deliverys";			///	������� ������� ��������
$tableCollab["blog"]                    = $_DB_tables_prefix."fo_blog";                 /// ������� �����
$tableCollab["customers_delivery"]      = $_DB_tables_prefix."fo_customers_delivery";   /// ������� ���������� � �������
$tableCollab["orders"]                  = $_DB_tables_prefix."fo_orders";              /// ������� �������



## Classes collaboration
$classCollab["SQL"] 		= "SQL.class.php";
$classCollab["BLOCKS"] 		= "FO.blocks.class.php";
$classCollab["C_P"] 		= "FO.c_p.class.php";
$classCollab["NEWS"] 		= "FO.news.class.php";
//$classCollab["SITE"] 		= "FO.site.class.php";
//$classCollab["USERS"] 		= "FO.users.class.php";
//$classCollab["ORDERS"] 		= "FO.orders.class.php";
//$classCollab["CUSTOMERS"] 	= "FO.customers.class.php";
//$classCollab["FORUM"] 		= "FO.forum.class.php";
//$classCollab["TEXTS"] 		= "FO.texts.class.php";
//$classCollab["PHOTO"] 		= "FO.photo.class.php";
//$classCollab["CLUBS"] 		= "FO.clubs.class.php";
//$classCollab["SCHOOLS"] 	= "FO.schools.class.php";
//$classCollab["GURU"] 		= "FO.guru.class.php";


## Languages collaboration
$langCollab[1] = "russian";	## default language
$langCollab[2] = "english";


## Your current Schema
$_curSchema = "defaultSchema";

## Custom variables
?>
